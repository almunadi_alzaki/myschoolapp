import { ADD_GUEST_BOOK, GET_GUEST_BOOKS } from "../actions/guest-book.action";

const initialState = {
  visitors: [],
}

export default function guestBooksReducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_GUEST_BOOKS: {
      return {...state, visitors: action.visitors};
    }
    case ADD_GUEST_BOOK: {
      return {...state, ...action.result}
    }
    default:
      return state
  }
}

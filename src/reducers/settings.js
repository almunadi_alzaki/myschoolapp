import { SET_NOTIFICATION_POPUP, SET_NOTIFICATION_SOUND } from "../actions/settings.action";

const initialState = {
  sound_code: 'default',
  visibility: 1,
  popup_settings: 'always'
}

export default function settingsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_NOTIFICATION_SOUND: {
      return {...state, sound_code: action.sound_code}
    }
    case SET_NOTIFICATION_POPUP: {
      return {...state, popup_settings: action.popup_settings, visibility: action.visibility}
    }
    default:
      return state
  }
}

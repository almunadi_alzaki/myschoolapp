import { ADD_STUDENT, DELETE_STUDENT, GET_STUDENT_LOCAL } from '../actions/actions'
import { GET_MESSAGES } from '../actions/messageActions'

const initialState = {
    schoolId: '',
    password: '',
    email: '',
    students: []
}

export default function reducer(state = initialState, action = {}) {
    const {payload} = action;
    switch (action.type) {
        case GET_STUDENT_LOCAL:
            return {
                ...state,
                students: payload //JSON.parse(payload)
            }

        case ADD_STUDENT:
            let students = [...state.students, ...payload];
            return {
                ...state,
                students: students,
            }
        case GET_MESSAGES:
            return {
                ...state,
                students: payload,
            }
        case DELETE_STUDENT:
            return {
                ...state,
                students: payload,
            }
        default:
            return state
    }
}

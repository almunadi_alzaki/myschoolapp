import {
	GET_ACADEMIC_CALENDAR,
	GET_FULL_SCHOOL_TIMETABLE,
	GET_LOGGED_IN_USER,
	GET_RIGHT_MENU,
	GET_TEACHERS,
	GET_TIME_TABLE,
	REMOVE_LOGGED_IN_USER,
	UPDATE_APP
} from "../actions/general.action";
import {SET_PAGE_SCREEN} from "../actions/staffactions";

const initialState = {
	pageScreen: 'student',
	rightMenus: [],
	teachers: [],
	timeTable: [],
	calendar: null,
	updateStatus: {
		class: null,
		divs: null,
		students: null,
		subjects: null,
		teachers: null
	},
	schoolTimeTable: null,
	appPermission: [],
}

export default function generalReducer(state = initialState, action = {}) {
	switch (action.type) {
		case GET_RIGHT_MENU: {
			return {
				...state,
				rightMenus: action.rightMenus || []
			};
		}
		case GET_LOGGED_IN_USER: {
			const permission = action.permission;
			const existingPermission = state.appPermission.find(p => permission.teacherId ? p.teacherId === permission.teacherId : p.studentId === permission.studentId);
			return {
				...state,
				appPermission: state.appPermission.filter(p => p !== existingPermission).concat(permission)
			};
		}
		case REMOVE_LOGGED_IN_USER: {
			const {teacherId, studentId} = action;
			const appPermission = state.appPermission.filter(p => teacherId ? p.teacherId !== teacherId : p.studentId !== studentId);
			return {...state, appPermission};
		}
		case SET_PAGE_SCREEN: {
			return {...state, pageScreen: action.pageScreen};
		}
		case GET_FULL_SCHOOL_TIMETABLE: {
			return {...state, schoolTimeTable: action.timeTable};
		}
		case GET_TIME_TABLE: {
			return {...state, timeTable: action.timeTable}
		}
		case GET_TEACHERS: {
			return {...state, teachers: [...action.teachers]}
		}
		case GET_ACADEMIC_CALENDAR: {
			return {...state, calendar: action.calendar}
		}
		case UPDATE_APP: {
			return {...state, updateStatus: {...state.updateStatus, ...action.status}}
		}
		default:
			return state
	}
}

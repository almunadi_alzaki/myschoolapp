import {
    ADD_STAFF,
    GET_CLASS_LOCAL,
    GET_DIV_LOCAL,
    GET_MENUPERMISSION_LOCAL,
    GET_ROOMS,
    GET_STAFF_LOCAL,
    GET_STUDENTS_LOCAL,
    GET_SUBJECT_LOCAL,
    GET_TEACHERMENUPERMISSION_LOCAL,
    GET_VIEW_BEHAVIOURS,
    GET_VIEW_EXECUSES,
    UPDATE_EXCUSE_STATUS,
    ADD_SCHOOL,
    SET_LOADING_BEHAVIOR,
    SEND_BEHAVIOR_SUCCESS
} from '../actions/staffactions'
import { GET_STAFF_MESSAGES } from '../actions/messageActions'

const initialState = {
    schoolId: '',
    password: '',
    mobileNo: '',
    staffs: [],
    classes: [],
    divs: [],
    students: [],
    searchArray: [],
    subjects: [],
    viewexecuses: [],
    viewbehaviours: [],
    staffMsgs: [],
    menupermissions: [],
    rooms: [],
    teachermenupermissions: [],
    isLoading: false,
}


export default function reducer(state = initialState, action = {}) {
    const { payload } = action

    switch (action.type) {
        case GET_STAFF_LOCAL: {
            let schoolId = state.schoolId;
            if (payload.length) {
                const staff = payload.find(s => s.schoolId === schoolId);
                if (!staff) {
                    schoolId = payload[0].schoolId;
                }
            } else {
                schoolId = '';
            }
            return {
                ...state,
                schoolId,
                staffs: payload //JSON.parse(payload)
            }
        }
        case GET_CLASS_LOCAL:
            return {
                ...state,
                classes: payload //JSON.parse(payload)
            }

        case GET_DIV_LOCAL:
            return {
                ...state,
                divs: payload //JSON.parse(payload)

            }
        case GET_STUDENTS_LOCAL:
            return {
                ...state,
                students: payload //JSON.parse(payload)

            }

        case GET_MENUPERMISSION_LOCAL:
            return {
                ...state,
                menupermissions: payload //JSON.parse(payload)

            }

        case GET_TEACHERMENUPERMISSION_LOCAL:
            return {
                ...state,
                teachermenupermissions: payload //JSON.parse(payload)

            }

        case GET_SUBJECT_LOCAL:
            return {
                ...state,
                subjects: payload //JSON.parse(payload)

            }
        case GET_ROOMS:
            return {
                ...state,
                rooms: action.rooms //JSON.parse(payload)
            }

        case GET_VIEW_EXECUSES:
            return {
                ...state,
                viewexecuses: payload //JSON.parse(payload)

            }
        case UPDATE_EXCUSE_STATUS: {
            const excuseIndex = state.viewexecuses.findIndex(p => p.behaviourId === action.behaviourId);
            const excuse = state.viewexecuses[excuseIndex];
            if (excuse) {
                excuse.accept_or_reject = action.statusval;
                return {
                    ...state,
                    viewexecuses: Object.assign(state.viewexecuses, { [excuseIndex]: excuse }) //JSON.parse(payload)

                }
                return state;
            }
            return {
                ...state,
                viewexecuses: payload //JSON.parse(payload)

            }
        }
        case GET_VIEW_BEHAVIOURS:
            return {
                ...state,
                viewbehaviours: payload //JSON.parse(payload)

            }

        case ADD_STAFF:
            let staffs = [...state.staffs, ...payload];
            return {
                ...state,
                staffs: staffs,
            }
        case GET_STAFF_MESSAGES:
            return {
                ...state,
                staffs: payload,
                staffMsgs: payload
            }
        case ADD_SCHOOL:
            return {
                ...state,
                schoolId: action.schoolId
            }
        case SET_LOADING_BEHAVIOR:
            return {
                ...state,
                isLoading: true,
            }
        case SEND_BEHAVIOR_SUCCESS:
            return {
                ...state,
                isLoading: false,
            }
        default:
            return state
    }
}

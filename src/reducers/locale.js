import I18n from 'react-native-i18n'
import { GET_LOCALE, GET_SETTINGS, SET_LOCALE, SET_TOKEN, SET_TRANSLATIONS } from '../actions/actions'

import { GET_PAGE_SCREEN, } from '../actions/staffactions'
import ReactNative from "react-native";

const currentLocale = I18n.currentLocale();
export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;
const initialState = {
    locale: isRTL ? 'ar' : 'en',
    isRTL,
    pageScreen: 'parent',
    version: null,
    toggle: false,
    deviceToken: "",
}

export default function reducer(state = initialState, action = {}) {
    const { payload } = action

    switch (action.type) {

        case SET_LOCALE: {
            I18n.locale = payload.locale;
            const isRTL = payload.locale === 'ar';
            ReactNative.I18nManager.allowRTL(isRTL);
            ReactNative.I18nManager.forceRTL(isRTL);
            return {
                ...state,
                locale: payload.locale,
                isRTL
            }
        }
        case SET_TOKEN:
            return {
                ...state,
                deviceToken: payload.token
            }
        case GET_PAGE_SCREEN:
            return {
                ...state,

            }

        case GET_LOCALE:
            return {
                ...state,
            }

        case SET_TRANSLATIONS:
            const { translations } = payload
            I18n.translations = translations

            return {
                ...state,
                version: translations._version || state.version,
                toggle: !state.toggle,
            }

        case GET_SETTINGS: {
            I18n.locale = payload.locale;
            const isRTL = payload.locale === 'ar';
            ReactNative.I18nManager.forceRTL(isRTL);
            ReactNative.I18nManager.allowRTL(isRTL);
            return {
                ...state,
                locale: payload.locale,
                isRTL,
                notificationSound: payload.notificationSound,
                notificationPopup: payload.notificationPopup
            }
        }
        default:
            return state
    }
}

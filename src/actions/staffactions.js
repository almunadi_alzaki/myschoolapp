import axios from 'axios'
import {Alert, Platform} from 'react-native'
import * as firebase from "react-native-firebase";
import realm from '../realm/schema';
import {updateAppService} from '../realm/updateAppService'
import {setNotificationPopupSuccess, setNotificationSoundSuccess} from "./settings.action";
import {getRightMenu, getTeachers, getTimeTable, setUpdateStatus} from "./general.action";
import {groupBy} from "../utils";
import {strings} from "../i18n";
import {getUnreadStaffMessages} from './messageActions';

export const SET_LOCALE = 'SET_LOCALE';
export const GET_LOCALE = 'GET_LOCALE';
export const SET_TRANSLATIONS = 'SET_TRANSLATIONS';
export const ADD_STAFF = 'ADD_STAFF';
export const ADD_SCHOOL = 'ADD_SCHOOL';
export const DELETE_STUDENT = 'DELETE_STUDENT';
export const SET_PAGE_SCREEN = 'SET_PAGE_SCREEN';
export const GET_PAGE_SCREEN = 'GET_PAGE_SCREEN';
export const GET_STAFF_LOCAL = 'GET_STAFF_LOCAL';
export const GET_CLASS_LOCAL = 'GET_CLASS_LOCAL';
export const GET_ROOMS = 'GET_ROOMS';
export const UPDATE_EXCUSE_STATUS = 'UPDATE_EXCUSE_STATUS';
export const GET_MENUPERMISSION_LOCAL = 'GET_MENUPERMISSION_LOCAL';
export const GET_TEACHERMENUPERMISSION_LOCAL = 'GET_TEACHERMENUPERMISSION_LOCAL';
export const GET_STUDENTS_LOCAL = 'GET_STUDENTS_LOCAL';
export const GET_SUBJECT_LOCAL = 'GET_SUBJECT_LOCAL';
export const GET_DIV_LOCAL = 'GET_DIV_LOCAL';
export const GET_VIEW_EXECUSES = 'GET_VIEW_EXECUSES';
export const GET_VIEW_BEHAVIOURS = 'GET_VIEW_BEHAVIOURS';
export const SET_LOADING_BEHAVIOR = 'SET_LOADING_BEHAVIOR';
export const SEND_BEHAVIOR_SUCCESS = 'SEND_BEHAVIOR_SUCCESS';
export const SET_TOKEN = 'SET_TOKEN';
export const GET_SETTINGS = 'GET_SETTINGS';
//import realm from '../realm/staffschema';
global.realm = realm;
// $2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ
const SECURE_TOKEN = "$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ";
const baseUrl = 'https://www.nenewe.com/';
const fetchStaffDetails = {
	url: baseUrl + 'school/neneweapis/register-teacher/',
};

const addNewToken = {
	url: baseUrl + 'add-tokens/',
};

const removeToken = {
	url: baseUrl + 'remove-token/',
};

const fetchClasses = {
	url: baseUrl + 'school/neneweapis/class-lists/',
};

const fetchDivs = {
	url: baseUrl + 'school/neneweapis/div-lists/',
};
const fetchStudents = {
	url: baseUrl + 'school/neneweapis/filter-students/',
};
const fetchSubjects = {
	url: baseUrl + 'school/neneweapis/subject-lists/',
};

const viewExecuses = {
	url: baseUrl + 'school/neneweapis/filter-excuse-to-go-home/',
};

const viewBehaviours = {
	url: baseUrl + 'school/neneweapis/view-student-behaviour/',
};

const UpdateExecuses = {
	url: baseUrl + 'school/neneweapis/update-excuse-to-go-home/',
};

const sendBehaviours = {
	url: baseUrl + 'school/neneweapis/add-student-behaviour/',
};

const staffMenus = {
	url: baseUrl + 'school/neneweapis/fetch-staff-menu-permission/',
};


export const setToken = token => ({
	type: SET_TOKEN,
	payload: {
		token
	}
});

export const setLocaleSuccess = locale => ({
	type: SET_LOCALE,
	payload: {
		locale
	},
});

export function setLocale(locale) {
	//console.log('REALM PATH', realm.path);
	return async function (dispatch, getState) {
		console.log("action set locale " + locale)
		try {
			realm.write(() => {
				realm.create('Settings', {
					id: "settings",
					locale: locale
				}, true);
			});
			dispatch(setLocaleSuccess(locale));
			Alert.alert('Language Preference Changed', "");
		} catch (err) {
			console.log(err)
		}
	}
}


export function updateApp() {
	return async function (dispatch, getState) {
		try {
			console.log('test')
			const isLoading = Object.values(getState().general.updateStatus).some(p => !!p);
			if (isLoading) return;
			dispatch(setUpdateStatus({
				class: true,
				divs: true,
				students: true,
				subjects: true,
				teachers: true
			}));
			let students = realm.objects('Student');
			const school_ids = Object.keys(groupBy(students, 'school_id'));
			school_ids.forEach(school_id => dispatch(getRightMenu({
				school_id,
				page_type: 'parent'
			})));
			let staffs = realm.objects('Staff');
			staffs.forEach(staff => dispatch(getRightMenu({
				teacherId: staff.teacherId,
				schoolId: staff.schoolId,
				page_type: 'staff'
			})));
			const schools = groupBy(staffs, 'schoolId');
			Object.keys(schools)
				.forEach(s => {
					dispatch(setUpdateStatus({
						class: true,
						divs: true,
						students: true,
						subjects: true,
						teachers: true
					}));
					const levelId = schools[s][0].levelid;
					let form = new FormData();
					form.append("token_secure", SECURE_TOKEN);
					form.append("schoolId", s);
					if (levelId != 17) {
						dispatch(updateClasses(form, s));
						dispatch(updateDivs(form, s));
					} else
						dispatch(setUpdateStatus({class: false, divs: false,}));
					dispatch(fetchFilterStudentsValues(s));
					dispatch(fetchFilterSubjectsValues(s));
					dispatch(getTeachers(s));
				});
		} catch (err) {
			dispatch(setUpdateStatus({
				class: false,
				divs: false,
				students: false,
				subjects: false,
				teachers: false
			}));
			console.log(err)
		}
	}
}

export function updateAcadamicStudents(form) {
	return async function (dispatch, getState) {
		let students = await axios.post(fetchStudents.url, form);
		if (students.data.studentresults.length > 0) {
			students = students.data.studentresults;
			if (realm.objects('AcadamicStudents').length > 0) {
				try {
					let msgDelte = realm.objects('AcadamicStudents');
					realm.write(() => {
						realm.delete(msgDelte)
					})
					updateAppService.saveAcadamicStudents(students);
					dispatch(getAllStudentsLocal())
				} catch (err) {
					console.log(err)
				}

			} else {
				updateAppService.saveAcadamicStudents(students);
				// let Classes = realm.objects('AcadamicStudents');
				//dispatch(NavigationActions.navigate({ routeName: 'Staff' }));
				dispatch(getAllStudentsLocal())
			}
		} else {
			Alert.alert('No students Found!', "");
		}
	}
}

export function updateStaffMenuPermission(form) {
	return async function (dispatch, getState) {
		if (realm.objects('Staff').length > 0) {
			staffDetails = realm.objects('Staff');
			let teacherId = staffDetails[0].teacherId;
			form.append("teacherId", teacherId);
			console.log("Update permission", form);
			let allstaffmenus = await axios.post(staffMenus.url, form);
			if (allstaffmenus.data.staffpermissions.length > 0) {
				let menupermissions = allstaffmenus.data.staffpermissions;
				if (realm.objects('StaffMenuPermission').length > 0) {
					//console.log("already have class");
					try {
						let msgDelte = realm.objects('StaffMenuPermission');
						realm.write(() => {
							realm.delete(msgDelte)
						})
						updateAppService.saveMenuPermission(menupermissions);
					} catch (err) {
						console.log(err)
					}
				} else {
					updateAppService.saveMenuPermission(menupermissions);
					// let Classes = realm.objects('AcadamicStudents');
					//dispatch(NavigationActions.navigate({ routeName: 'Staff' }));
					dispatch(getAllStaffMenuPermissionLocal())
					let MenuPermissions = realm.objects('StaffMenuPermission');
					//dispatch(NavigationActions.navigate({ routeName: 'Staff' }));
					dispatch(getAllStaffMenuPermissionLocal(menupermissions))
					Alert.alert('Update Finished!');
				}
			} else {
				Alert.alert('No permission Found!', "");
			}
		}
	}
}

export function updateClasses(form, schoolId) {
	return async function (dispatch, getState) {
		let allclasses = await axios.post(fetchClasses.url, form);
		if (allclasses.data.classes.length > 0) {
			let classes = allclasses.data.classes;
			if (realm.objects('Classes').length > 0) {
				try {
					// let msgDelte = realm.objects('Classes');
					// realm.write(() => {
					// 	realm.delete(msgDelte)
					// })
					updateAppService.saveClass(classes, schoolId);
				} catch (err) {
					console.log(err)
				}
			} else {
				updateAppService.saveClass(classes, schoolId);
				// let Classes = realm.objects('Classes');
				//dispatch(NavigationActions.navigate({ routeName: 'Staff' }));
				dispatch(getAllClassesLocal());
			}
		} else {
			Alert.alert('No Classes Found!', "");
		}
		dispatch(setUpdateStatus({
			class: false,
		}));
	}
}

export function updateDivs(form, schoolId) {
	return async function (dispatch, getState) {
		// console.log("Form....",form);
		let alldivs = await axios.post(fetchDivs.url, form);
		if (alldivs.data.alldivs.length > 0) {
			let divs = alldivs.data.alldivs;
			if (realm.objects('Divs').length > 0) {
				try {
					const msgDelte = realm.objects('Divs').filtered(`schoolId == "${schoolId}"`);
					// let msgDelte = realm.objects('Divs');
					realm.write(() => {
						realm.delete(msgDelte)
					})
					updateAppService.saveDiv(divs, schoolId);
				} catch (err) {
					console.log(err)
				}

			} else {
				updateAppService.saveDiv(divs, schoolId);
				let Divs = realm.objects('Divs');
				//dispatch(NavigationActions.navigate({ routeName: 'Staff' }));
				dispatch(getAllDivsLocal())
			}
		} else {
			Alert.alert('No Divs Found!', "");
		}
		dispatch(setUpdateStatus({
			divs: false,
		}));
	}
};

export const changeStaffSuccess = (schoolId) => ({
	type: ADD_SCHOOL,
	schoolId,
});

export function changeStaff(schoolId, levelid) {
	return async function (dispatch) {
		try {
			realm.write(() => {
				realm.create('Settings', {
					id: "settings",
					schoolId,
				}, true);
			});
			dispatch(getStaffLocal());
			dispatch(getUnreadStaffMessages())
			dispatch(getTimeTable());
			dispatch(changeStaffSuccess(schoolId));
			dispatch(getAllStaffMenuPermissionLocal());
			// dispatch(fetchFilterStudentsValues(schoolId))
			// dispatch(getAllStudentsLocal())
			// dispatch(getAllSubjectsLocal());
			// if (levelid != 17) {
			//     dispatch(getAllDivsLocal());
			//     dispatch(getAllClassesLocal());
			//     dispatch(fetchFilterClassesValues(schoolId));
			//     dispatch(fetchFilterDivsValues(schoolId));
			// }
		} catch (error) {
			console.log('error', error);
		}
	}
}


export function addStaffToApp(schoolId, password, deviceToken) {
	console.log("Action  Login Staff token " + deviceToken);
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("password", password);
	return async function (dispatch, getState) {
		try {
			let staffDetails = await axios.post(fetchStaffDetails.url, form);
			if (staffDetails.data.length > 0) {
				let staff = staffDetails.data[0];
				if (realm.objects('Staff').filtered(`schoolId == "${staff.schoolId}"`).length > 0) {
					dispatch(getStaffLocal(staffDetails.data));
					Alert.alert(strings('staffAlreadyExist'), "");
				} else {
					console.log()
					realm.write(() => {
						realm.create('Staff', {
							teacherId: staff.teacherId,
							teacherName: staff.shortName,
							schoolName: staff.schoolname,
							mobileNo: staff.mobileNo,
							schoolId: staff.schoolId,
							t_password: staff.t_password,
							levelid: staff.levelid,
							logged_to: staff.logged_to,
							teacherType: staff.teacherType,
							permission: staff.permission
						});
					});
					let stud = realm.objects('Staff');
					// console.log("from realm  " + JSON.stringify(stud))
					let form = new FormData();
					form.append("token_secure", SECURE_TOKEN);
					form.append("schoolId", staff.schoolId);
					form.append("teacherId", staff.teacherId);
					form.append("device_type", Platform.OS);
					// form.append("token","ExponentPushToken[ywsrqLFtJIo7yB7j8OWFD7]");
					form.append("token", getState().locale.deviceToken);
					form.append("visibility", 1);
					form.append("sound_code", getState().locale.notificationSound);
					form.append("popup_settings", getState().locale.notificationPopup);
					let tokenSet = await axios.post(addNewToken.url, form);
					console.log("token set result  " + JSON.stringify(tokenSet.data));
					firebase.messaging().subscribeToTopic('staffs-' + staff.schoolId);
					dispatch(changeStaff(staff.schoolId));
					dispatch(getTeachers(staff.schoolId));
					dispatch(fetchFilterStudentsValues(staff.schoolId));
					dispatch(fetchFilterSubjectsValues(staff.schoolId));
					// dispatch(getAllSubjectsLocal());
					if (staff.levelid != 17) {
						dispatch(fetchFilterClassesValues(staff.schoolId));
						dispatch(fetchFilterDivsValues(staff.schoolId));
						// dispatch(getAllClassesLocal());
						// dispatch(getAllDivsLocal());
					}
					Alert.alert(strings('staffAdded'), "");
				}
			} else {
				Alert.alert(strings('noStaffFound'), "");
			}

		} catch (err) {
			console.log(err);
			// Alert.alert('Addng Staff Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export const sendBehaviourSuccess = (payload) => ({
	type: SEND_BEHAVIOR_SUCCESS,
	payload,
});

export function sendBehaviour(paramsArray, navigation) {
	let schoolId = paramsArray[0];
	let behaviourMenuId = paramsArray[1];
	let selstudents = paramsArray[2];
	selstudents = selstudents.toString();
	let period = paramsArray[3];
	let subId = paramsArray[4];
	let message = paramsArray[5];
	let teacherId = paramsArray[6];
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("studentId", selstudents);
	form.append("period", period);
	form.append("subId", subId);
	form.append("teacherId", teacherId);
	form.append("message", message);
	form.append("behaviour", behaviourMenuId);
	return async function (dispatch, getState) {
		dispatch({
			type: SET_LOADING_BEHAVIOR
		});
		try {
			let behaviourResponse = await axios.post(sendBehaviours.url, form);
			dispatch(sendBehaviourSuccess(behaviourResponse.data))
			console.log("behaviour result  " + JSON.stringify(behaviourResponse.data));
			Alert.alert(strings('Behaviour send'), "");
			if (navigation) {
				navigation.pop();
				navigation.pop();
			}
		} catch (err) {
			console.log(err);
		}
	}
}


export function getStaffMenus(schoolId, teacherId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("teacherId", teacherId);
	//form.append("menu_for", menu_for);

	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let allstaffmenus = await axios.post(staffMenus.url, form);
			if (allstaffmenus.data.staffpermissions.length > 0) {
				let menupermissions = allstaffmenus.data.staffpermissions;
				const ex = realm.objects('StaffMenuPermission').filtered(`teacherId == '${teacherId}'`);
				realm.write(() => realm.delete(ex));
				menupermissions.map((permission, key) => {
					realm.write(() => {
						realm.create('StaffMenuPermission', {
							teacherId: permission.teacherId,
							mobileNo: permission.mobileNo,
							menu_id: permission.menu_id,
							full_or_own: permission.full_or_own,
							menustatus: permission.status,
							schoolId: schoolId,
							menu_name_en: permission.menu_name_en,
							parent_id: permission.parent_id,
							menu_name_ar: permission.menu_name_ar,
							menu_link: permission.menu_link,
							menu_for: permission.menu_for,
							icon_label: permission.icon_label

						});
					});
				});
				dispatch(getAllStaffMenuPermissionLocal());
			} else {
				Alert.alert('No permission Found!', "");
			}

		} catch (err) {
			console.log(err);
			// Alert.alert('Inserting Menu Permission Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}


export function getTeacherMenus(schoolId, teacherId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("teacherId", teacherId);
	//form.append("menu_for", menu_for);
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let allstaffmenus = await axios.post(staffMenus.url, form);
			if (allstaffmenus.data.staffpermissions.length > 0) {
				let menupermissions = allstaffmenus.data.staffpermissions;
				const ex = realm.objects('StaffMenuPermission').filtered(`teacherId == '${teacherId}'`);
				realm.write(() => realm.delete(ex));
				menupermissions.map((permission, key) => {
					realm.write(() => {
						realm.create('StaffMenuPermission', {
							teacherId: permission.teacherId,
							mobileNo: permission.mobileNo,
							menu_id: permission.menu_id,
							full_or_own: permission.full_or_own,
							menustatus: permission.status,
							schoolId: schoolId,
							menu_name_en: permission.menu_name_en,
							parent_id: permission.parent_id,
							menu_name_ar: permission.menu_name_ar,
							menu_link: permission.menu_link,
							menu_for: permission.menu_for,
							icon_label: permission.icon_label
						});
					});
				});
				dispatch(getAllTeacherMenuPermissionLocal());
			} else {
				Alert.alert('No permission Found!', "");
			}
		} catch (err) {
			console.log(err.message);
			// Alert.alert('Inserting Menu Permission Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function fetchExecuseValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let viewexecuses = await axios.post(viewExecuses.url, form);
			dispatch(receiveViewExecuses(viewexecuses.data.execusetogohomes))

		} catch (err) {
			console.log(err);
			// Alert.alert('Error confirm execuse API!', "");
			// dispatch(loadQuotesError())
		}
	}
}


export function fetchTodayBehaviourValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let viewbehaviours = await axios.post(viewBehaviours.url, form);
			dispatch(receiveViewBehaviours(viewbehaviours.data.behaviours))

		} catch (err) {
			console.log(err);
			// Alert.alert('Error confirm execuse API!', "");
			// dispatch(loadQuotesError())
		}
	}
}


export function changeExecuseStatus(schoolId, teacherId, behaviourId, statusval) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("behaviourId", behaviourId);
	form.append("confirmteacherId", teacherId);
	form.append("sentstatus", statusval);
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let updateExecuseResponse = await axios.post(UpdateExecuses.url, form);
			dispatch({
				type: UPDATE_EXCUSE_STATUS,
				schoolId,
				teacherId,
				behaviourId,
				statusval
			});
			if (statusval === 1) {
				Alert.alert(strings('Excuse Accepted'), "");
			} else {
				Alert.alert(strings('Excuse Rejected'), "");
			}

		} catch (err) {
			console.log(err);
			// Alert.alert('Error update confirm execuse API!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function fetchFilterSubjectsValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let allsubjects = await axios.post(fetchSubjects.url, form);
			if (allsubjects.data.subjects.length > 0) {
				allsubjects.data.subjects.map((item, key) => {
					realm.write(() => {
						realm.create('Subjects', {
							subId: item.subId,
							sub_name_en: item.sub_name_en,
							sub_name_ar: item.sub_name_ar,
							sub_shortcut: item.sub_shortcut,
							levelid: item.levelid,
							ordering: item.ordering,
							hifl_or_not: item.hifl_or_not,
							schoolId: schoolId
						}, true);
					});
				});
				dispatch(getAllSubjectsLocal(allsubjects.data.subjects))
			} else {
				Alert.alert('No Subjects Found!', "");
			}

		} catch (err) {
			console.log(err.message);
			// Alert.alert('Inserting Subjects Failed!', "");
			// dispatch(loadQuotesError())
		}
		dispatch(setUpdateStatus({
			subjects: false,
		}));
	}
}

export function fetchFilterClassesValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		try {
			let allclasses = await axios.post(fetchClasses.url, form);
			if (allclasses.data.classes.length > 0) {
				let classes = allclasses.data.classes;
				classes.map((item, key) => {
					realm.write(() => {
						realm.create('Classes', {
							classId: item.classId,
							class_en: item.class_en,
							schoolId: schoolId,
							class_ar: item.class_ar,
							levelid: item.levelId,
							class_shortcut: item.class_shortcut,
							class_order: item.class_order,
							hifl_or_not: item.hifl_or_not
						}, true);
					});
				});
				// let Classes = realm.objects('Classes');
				dispatch(getAllClassesLocal())
			} else {
				Alert.alert('No classes Found!', "");
			}

		} catch (err) {
			console.log(err.message, 'test');
			// Alert.alert('Inserting classes Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function fetchFilterDivsValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		try {
			// const divs = realm.objects('Divs').filtered(`schoolId == "${schoolId}"`);
			// realm.write(() => {
			// 	realm.delete(divs);
			// });
			dispatch(getAllDivsLocal());
			let div = await axios.post(fetchDivs.url, form);
			if (div.data.alldivs.length > 0) {
				let divs = div.data.alldivs;
				divs.map((item, key) => {
					realm.write(() => {
						realm.create('Divs', {
							classId: item.class,
							schoolId: schoolId,
							div: item.div
						}, true);
					});
				});
				dispatch(getAllDivsLocal())
			} else {
				Alert.alert('No Divs Found!', "");
			}

		} catch (err) {
			console.log(err.message, 'test');
			// Alert.alert('Inserting Divs Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function fetchFilterStudentsValues(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		try {
			let students = await axios.post(fetchStudents.url, form);
			// const existing = realm.objects('AcadamicStudents');
			if (students.data.studentresults.length > 0) {
				students = students.data.studentresults;
				// realm.write(() => {
				//     realm.delete(existing);
				// });
				students.map((item, key) => {
					realm.write(() => {
						realm.create('AcadamicStudents', {
							id: item.id,
							schoolId: schoolId,
							st_name: item.st_name,
							classid: item.class,
							st_acadamy_no: item.st_acadamy_no,
							parent_phone: item.parent_phone,
							id_no: item.id_no,
							st_password: item.st_password,
							app_usage: item.app_usage,
							div: item.div
						}, true);
					});
				});
				dispatch(getAllStudentsLocal());
			} else {
				Alert.alert('No Students Found!', "");
			}

		} catch (err) {
			console.log(err.message, 'test');
			// Alert.alert('Inserting Students Failed!', "");
			// dispatch(loadQuotesError())
		}
		dispatch(setUpdateStatus({
			students: false,
		}));
	}
}


export const deleteStudentSuccess = (students) => ({
	type: DELETE_STUDENT,
	payload: students
});

export const logoutSuccess = (staff) => ({
	type: GET_STAFF_LOCAL,
	payload: staff
});

export function deleteStudent(student, students, deviceToken) {
	console.log("Action In delete student New");
	return async function (dispatch, getState) {
		try {
			console.log("student details...................", student);
			const studentDelteId = student.id;
			let studentDelte = realm.objects('Student').filtered(`id == "${studentDelteId}"`);
			console.log("student to delete");
			let studentsAfterDelete = students.filter(function (obj) {
				return obj.id !== student.id;
			});
			dispatch(deleteStudentSuccess((studentsAfterDelete)));
			realm.write(() => {
				realm.delete(studentDelte);
			});
			Alert.alert('Deleted Student !', "");
			let form = new FormData();
			form.append("token_secure", SECURE_TOKEN);
			form.append("studentId", student.id);
			form.append("token", deviceToken);
			let tokenSet = await axios.post(removeToken.url, form);
			console.log("token set result  " + JSON.stringify(tokenSet.data))
		} catch (err) {
			console.log(err);
			// Alert.alert('Deleting Student Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function logoutStaff(teacherId, deviceToken) {
	return async function (dispatch, getState) {
		try {
			let staffDelte = realm.objects('Staff').filtered(`teacherId == "${teacherId}"`);
			const schoolId = getState().staff.schoolId;
			const allstudentsbySchool = realm.objects('AcadamicStudents').filtered(`schoolId == "${schoolId}"`);
			const divs = realm.objects('Divs');
			const Classes = realm.objects('Classes');
			const Subjects = realm.objects('Subjects');
			const StaffMessages = realm.objects('StaffMessages').filtered(`teacherId == "${teacherId}"`);
			const StaffMenuPermission = realm.objects('StaffMenuPermission').filtered(`teacherId == "${teacherId}"`);
			const TimeTable = realm.objects('TimeTable').filtered(`teacherId == "${teacherId}"`);
			const RightMenus = realm.objects('RightMenus').filtered(`teacherId == "${teacherId}"`);
			realm.write(() => {
				realm.delete(staffDelte);
				realm.delete(divs);
				realm.delete(Classes);
				realm.delete(Subjects);
				realm.delete(StaffMessages);
				realm.delete(StaffMenuPermission);
				realm.delete(TimeTable);
				realm.delete(RightMenus);
				realm.delete(allstudentsbySchool);
			});
			dispatch(getStaffLocal());
			dispatch(getAllStudentsLocal())
			dispatch(getAllDivsLocal());
			dispatch(getAllClassesLocal());
			dispatch(getAllSubjectsLocal());
			// dispatch(getUnreadStaffMessages());
			dispatch(getAllStaffMenuPermissionLocal());
			dispatch(getTimeTable());
			dispatch(getAllStudentsLocal());

			//  Alert.alert('Deleted Student !', "");
			let form = new FormData();
			form.append("token_secure", SECURE_TOKEN);
			form.append("teacherId", teacherId);
			form.append("token", deviceToken);
			let tokenSet = await axios.post(removeToken.url, form);
			if (realm.objects('Staff').filtered(`schoolId == "${schoolId}"`).length === 0) {
				firebase.messaging().unsubscribeFromTopic('staffs-' + schoolId);
			}
		} catch (err) {
			console.log(err.message);
			// Alert.alert('Logout Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export const getLocale = () => ({

	type: GET_LOCALE

});
export const getSettingsSuccess = (settings) => ({

	type: GET_SETTINGS,
	payload: settings

});

export function getSettings() {
	return async function (dispatch, getState) {
		try {
			const schoolId = getState().staff.schoolId;
			let settings = realm.objects('Settings').filtered(`schoolId == '${schoolId}'`);
			if (settings.length < 1) {
				realm.write(() => {
					// realm.deleteAll()
					realm.create('Settings', {
						id: "settings",
						notificationSound: "true",
						locale: getState().locale.locale,
						notificationPopup: getState().locale.notificationPopup
					})
				})
				// settings = [{locale:"ar"}]
			}
			settings = realm.objects('Settings');
			dispatch(setLocaleSuccess(settings[0].locale));
			dispatch(setNotificationSoundSuccess(settings[0].notificationSound));
			dispatch(setNotificationPopupSuccess(settings[0].notificationPopup))
			// I18n.locale = settings[0].locale;
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getStaffLocal() {
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');
			// const schoolId = getState().staff.schoolId;
			// let staff = realm.objects('Staff').filtered(`schoolId == '${schoolId}'`);
			let staff = realm.objects('Staff');
			let staffs = staff.map(p => p);
			// let staffs = (staff.map(p =>
			// 	({
			// 		...p
			// 	})
			// ));
			dispatch(receiveStaffLocal(staffs));
			if (!staff) {
				staff = "[]";
				console.log("==== async storage empty for Staffs")
			}
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllSubjectsLocal() {
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');
			const schoolId = getState().staff.schoolId;
			let subject = realm.objects('Subjects').filtered(`schoolId == '${schoolId}'`);
			let subjects = (subject);
			dispatch(receiveSubjectLocal(subjects));
			if (!subject) {
				subject = "[]";
				console.log("==== async storage empty for Staffs")
			}
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllStudentsLocal(searchclass, searchdiv, searchname) {
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');
			let schoolId = getState().staff.schoolId;
			let allstudentsbyclass, allstudentsbydiv, allstudentsbyname;
			if (searchclass && searchclass !== 'all') {
				allstudentsbyclass = realm.objects('AcadamicStudents').filtered(`schoolId == "${schoolId}" AND classid == "${searchclass}"`);
			} else {
				allstudentsbyclass = realm.objects('AcadamicStudents').filtered(`schoolId == "${schoolId}"`);
			}
			if (searchdiv && searchdiv !== 'all') {
				allstudentsbydiv = allstudentsbyclass.filtered(`div == "${searchdiv}"`);
			} else {
				allstudentsbydiv = allstudentsbyclass;
			}

			if (searchname) {
				allstudentsbyname = allstudentsbydiv.filtered(`st_name CONTAINS "${searchname}"`);
			} else {
				allstudentsbyname = allstudentsbydiv;
			}
			let firstfivestudents = allstudentsbyname.slice(0, 50);
			let allstudents = (firstfivestudents);
			dispatch(receiveAcadamicStudentsLocal(allstudents));
			if (!allstudents) {
				allstudents = "[]";
				console.log("==== async storage empty for Staffs")
			}

		} catch (err) {
			console.log(err.message, 'test')
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllAcadamicStudentsLocal(searchAcadamicNo, searchname) {
	return async function (dispatch) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');
			let allstudentsbyacadamicNo, allstudentsbyname;

			if (!searchAcadamicNo) {
				allstudentsbyacadamicNo = realm.objects('AcadamicStudents');
			} else {
				allstudentsbyacadamicNo = realm.objects('AcadamicStudents').filtered(`st_acadamy_no BEGINSWITH "${searchAcadamicNo}"`);
			}
			if (!searchname) {
				allstudentsbyname = allstudentsbyacadamicNo;
			} else {
				allstudentsbyname = allstudentsbyacadamicNo.filtered(`st_name CONTAINS "${searchname}"`);
			}

			let firstfivestudents = allstudentsbyname.slice(0, 50);
			let allstudents = (firstfivestudents);

			dispatch(receiveAcadamicStudentsLocal(allstudents));
			if (!allstudents) {
				allstudents = "[]";
				console.log("==== async storage empty for Staffs")
			}

		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllStaffMenuPermissionLocal() {
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');
			// let menupermissions = realm.objects('StaffMenuPermission');
			const schoolId = getState().staff.schoolId;
			let menupermissions = realm.objects('StaffMenuPermission').filtered(`menu_for == "s" and schoolId == '${schoolId}'`);
			let menupermission = (menupermissions);
			dispatch(receiveMenuPermissionLocal(menupermission));
			if (!menupermission) {
				menupermission = "[]";
				console.log("==== async storage empty for menupermission")
			}
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllTeacherMenuPermissionLocal() {
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			// let student = await AsyncStorage.getItem('students');

			const schoolId = getState().staff.schoolId;
			let menupermissions = realm.objects('StaffMenuPermission').filtered(`menu_for == "t" and schoolId == '${schoolId}'`);

			let menupermission = (menupermissions);
			dispatch(receiveTeacherMenuPermissionLocal(menupermission));
			if (!menupermission) {
				menupermission = "[]";
			}
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getRoomListSuccess(rooms) {
	return {
		type: GET_ROOMS,
		rooms
	}
}

export function getRoomList(schoolId) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);

	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			let allrooms = await axios.post("https://www.nenewe.com/school/neneweapis/roomlists/", form);
			if (allrooms.data.rooms.length > 0) {
				let rooms = allrooms.data.rooms;
				dispatch(getRoomListSuccess(allrooms.data.rooms))
			} else {
				Alert.alert('No rooms Found!', "");
			}
		} catch (err) {
			console.log(err);
			// Alert.alert('Inserting rooms Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllClassesLocal() {
	console.log("==============Action get classes ocal");
	return async function (dispatch, getState) {
		// dispatch(fetchingStudent())
		try {
			const schoolId = getState().staff.schoolId;
			let classes = realm.objects('Classes').filtered(`schoolId == '${schoolId}'`);
			let class1 = (classes);
			dispatch(receiveClassesLocal(class1));
			if (!class1) {
				class1 = "[]";
				console.log("==== async storage empty for classes")
			}
		} catch (err) {
			console.log(err.message)
			// dispatch(loadQuotesError())
		}
	}
}

export function getAllDivsLocal(classId) {
	return async function (dispatch, getState) {
		try {
			const schoolId = getState().staff.schoolId;
			let divs = realm.objects('Divs').filtered(`schoolId == "${schoolId}" and classId == "${classId}"`);
			console.log(schoolId, classId, realm.objects('Divs'), 'act');
			let div = (divs);
			dispatch(receiveDivLocal(div));
			if (!div) {
				div = "[]";
				console.log("==== async storage empty forDiv")
			}
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export const receiveClassesLocal = (classLocal) => ({
	type: GET_CLASS_LOCAL,
	payload: classLocal
});

export const receiveViewExecuses = (viewExecuses) => ({

	type: GET_VIEW_EXECUSES,
	payload: viewExecuses

});

export const receiveViewBehaviours = (viewBehabiours) => ({

	type: GET_VIEW_BEHAVIOURS,
	payload: viewBehabiours

});

export const receiveSubjectLocal = (subjectLocal) => ({

	type: GET_SUBJECT_LOCAL,
	payload: subjectLocal

});

export const receiveAcadamicStudentsLocal = (studentsLocal) => ({
	type: GET_STUDENTS_LOCAL,
	payload: studentsLocal
});


export const receiveDivLocal = (divLocal) => ({

	type: GET_DIV_LOCAL,
	payload: divLocal

});

export const receiveStaffLocal = (staffLocal) => ({

	type: GET_STAFF_LOCAL,
	payload: staffLocal

});


export const receiveMenuPermissionLocal = (menupermissionLocal) => ({

	type: GET_MENUPERMISSION_LOCAL,
	payload: menupermissionLocal

});

export const receiveTeacherMenuPermissionLocal = (menupermissionLocal) => ({

	type: GET_TEACHERMENUPERMISSION_LOCAL,
	payload: menupermissionLocal

});

export const setTranslations = translations => ({
	type: SET_TRANSLATIONS,
	payload: {
		translations
	},
});

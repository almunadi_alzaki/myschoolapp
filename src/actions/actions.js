import axios from 'axios'
import { Alert, Platform } from 'react-native'

import realm from '../realm/schema';
import { BASE_URL_HOME } from "../api/URLS";
import { setNotificationPopupSuccess, setNotificationSoundSuccess } from "./settings.action";
import * as firebase from "react-native-firebase";
import { addSchoolSuccess, SET_PAGE_SCREEN } from "./staffactions";
import { removeLoggedInUser } from "./general.action";
import RNRestart from 'react-native-restart';
import { strings } from "../i18n";
import { getUnreadMessages } from "./messageActions";

export const SET_LOCALE = 'SET_LOCALE';

export const GET_LOCALE = 'GET_LOCALE';
export const SET_TRANSLATIONS = 'SET_TRANSLATIONS';
export const GET_PAGE_SCREEN = 'GET_PAGE_SCREEN';
export const ADD_STUDENT = 'ADD_STUDENT';
export const DELETE_STUDENT = 'DELETE_STUDENT';
export const GET_STUDENT_LOCAL = 'GET_STUDENT_LOCAL';
export const SET_TOKEN = 'SET_TOKEN';
export const GET_SETTINGS = 'GET_SETTINGS';
global.realm = realm;
// $2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ
const SECURE_TOKEN = "$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ";
const baseUrl = BASE_URL_HOME;
const fetchMyChilds = {
	url: baseUrl + 'fetch-my-childs/',
};

const addNewToken = {
	url: baseUrl + 'add-tokens/',
};

const removeToken = {
	url: baseUrl + 'remove-token/',
};

export const setPageScreen = (pageScreen) => ({
	type: SET_PAGE_SCREEN, pageScreen
});
export const setToken = token => ({
	type: SET_TOKEN,
	payload: { token }
});

export const setLocaleSuccess = locale => ({
	type: SET_LOCALE,
	payload: { locale },
});


export function setLocale(locale) {
	return async function (dispatch, getState) {
		console.log("action set locale " + locale)
		try {
			realm.write(() => {
				realm.create('Settings', { id: "settings", locale: locale }, true);
			});
			dispatch(setLocaleSuccess(locale));
			Alert.alert(strings('languageChanged'), "");
			RNRestart.Restart();
		} catch (err) {
			console.log(err)
		}
	}
}

export const receiveStudent = (students) => ({
	type: ADD_STUDENT,
	payload: students
});


export function addStudent(schoolId, password, deviceToken) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("st_password", password);
	return async function (dispatch, getState) {
		try {
			let students = await axios.post(fetchMyChilds.url, form);
			console.log(students, 'stud');
			if (students.data.length > 0) {
				let student = students.data[0];
				if (realm.objects('Student').filtered(`id == "${student.id}"`).length > 0) {
					Alert.alert('Student is already exist !', "");
				} else {
					console.log('student call');
					realm.write(() => {
						realm.create('Student',
							{
								st_name: student.st_name,
								id: student.id,
								class: student.class,
								div: student.div,
								st_acadamy_no: student.st_acadamy_no,
								parent_phone: student.parent_phone,
								id_no: student.id_no,
								st_password: student.st_password,
								school_id: student.school_id,
								app_usage: student.app_usage,
								username: student.username,
								classId: student.classId,
								class_en: student.class_en,
								class_ar: student.class_ar,
								levelId: student.levelId,
								class_shortcut: student.class_shortcut,
								class_order: student.class_order
							});
					});
					let stud = realm.objects('Student');
					console.log(stud, 'student');
					let form = new FormData();
					form.append("token_secure", SECURE_TOKEN);
					form.append("schoolId", schoolId);
					form.append("studentId", student.id);
					form.append("device_type", Platform.OS);
					form.append("token", getState().locale.deviceToken);
					form.append("sound_code", getState().settings.sound_code);
					form.append("popup_settings", getState().settings.popup_settings);
					form.append("visibility", getState().settings.visibility);
					let tokenSet = await axios.post(addNewToken.url, form);
					const levelId = students.data[0].levelId;
					console.log('subscribe', levelId, +levelId === 14);
					if (+levelId === 14) {
						let school_code = students.data[0].school_id;
						let classId = students.data[0].class;
						let div = students.data[0].div;
						console.log('subscribed', levelId, +levelId === 14);
						firebase.messaging().subscribeToTopic('/topics/students-by-class-div' + `${school_code}` + '-' + `${classId}` + '-' + `${div}`);
						firebase.messaging().subscribeToTopic('/topics/students-by-class-div' + `${school_code}` + '-' + `${classId}`);
					} else {
						let school_code = students.data[0].school_id;
						let acadamic = students.data[0].st_acadamy_no;
						let acadamic_no = acadamic.substr(0, 2);
						console.log(acadamic_no);
						firebase.messaging().subscribeToTopic('/topics/students-by-acadamic' + `${school_code}` + '-' + `${acadamic_no}`);
					}
					firebase.messaging().subscribeToTopic('/topics/students-' + schoolId);
					Alert.alert(strings('Added new student'), "");

					dispatch(receiveStudent(students.data));
					dispatch(getUnreadMessages());
				}
			} else {
				Alert.alert(strings('noStudentFound'), "");
			}

		} catch (err) {
			console.log(err);
			Alert.alert('Addng Student Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}


export function studentCall(schoolId, id, secretId, st_name, username) {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	form.append("schoolId", schoolId);
	form.append("id", id);
	form.append("secretId", secretId);
	form.append("st_name", st_name);
	form.append("username", username);
	return async function (dispatch, getState) {
		try {
			let res = await axios.post(baseUrl + 'child-add-to-screen/', form);
		} catch (err) {
			console.log(err);
			// Alert.alert('Request failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}


export const deleteStudentSuccess = (students) => ({
	type: DELETE_STUDENT,
	payload: students
});

export function deleteStudent(student, students, deviceToken) {
	return async function (dispatch, getState) {
		try {
			const studentDelteId = student.id;
			const schoolId = student.school_id;
			let studentDelte = realm.objects('Student').filtered(`id == "${studentDelteId}"`);
			let studentsAfterDelete = students.filter(function (obj) {
				return obj.id !== student.id;
			});
			dispatch(deleteStudentSuccess((studentsAfterDelete)));
			dispatch(removeLoggedInUser({ studentId: studentDelteId }));
			realm.write(() => {
				realm.delete(studentDelte);
			});
			if (realm.objects('Student').filtered(`school_id == "${schoolId}" AND classId == "${student.classId}"`).length === 0) {
				firebase.messaging().subscribeToTopic('/topics/students-by-class-div' + `${schoolId}` + '-' + `${student.classId}`);
			}
			if (realm.objects('Student').filtered(`school_id == "${schoolId}" AND classId == "${student.classId}" AND div == "${student.div}"`).length === 0) {
				firebase.messaging().subscribeToTopic('/topics/students-by-class-div' + `${schoolId}` + '-' + `${student.classId}` + '-' + `${student.div}`);
			}
			if (student.st_acadamy_no && realm.objects('Student').filtered(`school_id == "${schoolId}" AND classId == "${student.classId}" AND div == "${student.div}"`).length === 0) {
				firebase.messaging().subscribeToTopic('/topics/students-by-acadamic' + `${schoolId}` + '-' + `${student.st_acadamy_no}`);
			}
			if (realm.objects('Student').filtered(`school_id == "${schoolId}"`).length === 0) {
				firebase.messaging().unsubscribeFromTopic('students-' + schoolId);
			}
			Alert.alert(strings('Deleted Student'), "");
			let form = new FormData();
			form.append("token_secure", SECURE_TOKEN);
			form.append("studentId", studentDelteId);
			form.append("token", deviceToken);
			let tokenSet = await axios.post(removeToken.url, form)
		} catch (err) {
			console.log(err.message);
			// Alert.alert('Deleting Student Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export const getLocale = () => ({

	type: GET_LOCALE

});


export const getSettingsSuccess = (settings) => ({
	type: GET_SETTINGS,
	payload: settings

});

export function getSettings() {
	return async function (dispatch, getState) {
		try {
			let settings = realm.objects('Settings');
			if (settings.length < 1) {
				realm.write(() => {
					// realm.deleteAll()
					realm.create('Settings',
						{
							id: "settings",
							sound_code: getState().settings.sound_code,
							schoolId: getState().staff.schoolId,
							popup_settings: getState().settings.popup_settings,
							visibility: getState().settings.visibility,
							locale: getState().locale.locale,
						})
				})
			}
			settings = realm.objects('Settings');
			dispatch(setLocaleSuccess(settings[0].locale));
			dispatch(addSchoolSuccess(settings[0].schoolId))
			dispatch(setNotificationSoundSuccess(settings[0].sound_code));
			dispatch(setNotificationPopupSuccess(settings[0].popup_settings, settings[0].visibility));
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export function getStudentLocal(schoolId) {
	return async function (dispatch) {
		// dispatch(fetchingStudent())
		try {
			let student = realm.objects('Student');
			console.log(student, 'call');
			dispatch(receiveStudentLocal(student));
		} catch (err) {
			console.log(err)
			// dispatch(loadQuotesError())
		}
	}
}

export const receiveStudentLocal = (studentLocal) => ({
	type: GET_STUDENT_LOCAL,
	payload: studentLocal

});

export const setTranslations = translations => ({
	type: SET_TRANSLATIONS,
	payload: { translations },
});

import axios from 'axios'
import {Alert} from 'react-native'
import {MessageService} from '../realm/service'
import {StaffMessageService} from '../realm/StaffMessageService'
import realm from '../realm/schema';

export const SET_LOCALE = 'SET_LOCALE';
export const GET_LOCALE = 'GET_LOCALE';
export const SET_TRANSLATIONS = 'SET_TRANSLATIONS';
export const ADD_STUDENT = 'ADD_STUDENT';
export const DELETE_STUDENT = 'DELETE_STUDENT';
export const GET_STUDENT_LOCAL = 'GET_STUDENT_LOCAL';
export const SET_TOKEN = 'SET_TOKEN';
export const GET_SETTINGS = 'GET_SETTINGS';
export const GET_MESSAGES = 'GET_MESSAGES';
export const GET_STAFF_MESSAGES = 'GET_STAFF_MESSAGES';


const SECURE_TOKEN = "$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ";
const baseUrl = 'https://www.nenewe.com/';
const unreadMessages = {
	url: baseUrl + 'school/neneweapis/parent-notifications-unread/',
};
const messageReadConst = {
	url: baseUrl + 'school/neneweapis/upadte-notifications-status/',
};
const unreadStaffMessages = {
	url: baseUrl + 'school/neneweapis/staff-notifications-unread/',
};
const staffMessageReadConst = {
	url: baseUrl + 'school/neneweapis/upadte-staff-notifications-status/',
};

export const getUnreadMessagesSuccess = (mesgCount) => ({
	type: GET_MESSAGES,
	payload: mesgCount
});

export const getUnreadStaffMessagesSuccess = (mesgCount) => ({
	type: GET_STAFF_MESSAGES,
	payload: mesgCount
});

export function getUnreadMessages() {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	return async function (dispatch, getState) {
		try {
			let students = realm.objects('Student');
			let schoolIds = students.map(student => student.school_id).toString();
			let studentIds = students.map(student => student.id);
			form.append("schoolId", schoolIds);
			form.append("sms_or_app", 'app');
			form.append("student_id", studentIds.toString());
			let messages = await axios.post(unreadMessages.url, form);
			messages = messages.data;
			if (messages['generalmessages'].length > 0) {
				messages.generalmessages.forEach((message, key) => {
					let messageRealm = realm.objectForPrimaryKey('Message', message.schoolId + "/" + message.add_time);
					if (!messageRealm) {
						let id = message.schoolId + "/" + message.add_time;
						let obj = {
							id: id,
							message: message.message,
							read: false,
							send_by: message.send_by,
							st_acadamy_no: message.acadamic_no,
							class_id: message.class_id,
							div: message.div,
							messageType: "general",
							date: message.add_time,
							schoolName: message.schoolname,
						};
						MessageService.save(obj);
					} else {
					}
				})
			}
			if (messages.privatemessages.length > 0) {
				messages.privatemessages.map((message, key) => {
					let messageRealm = realm.objectForPrimaryKey('Message', message.behaviourId);
					if (!messageRealm) {
						let obj = {
							id: message.behaviourId,
							message: message.message,
							read: false,
							messageType: "private",
							studentId: message.studentId,
							date: message.behaviour_date,
							schoolName: message.schoolname,
						};
						MessageService.save(obj);
					} else {
					}
				})
			}
			getMessageCount(getState, dispatch)

		} catch (err) {
			console.log(err);
			// Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function getUnreadStaffMessages() {
	let form = new FormData();
	form.append("token_secure", SECURE_TOKEN);
	return async function (dispatch, getState) {
		try {
			let staffs = realm.objects('Staff');
			let schoolIds = staffs.map(staff => staff.schoolId).toString();
			let teacherIds = staffs.map(staff => staff.teacherId);
			form.append("schoolId", schoolIds);
			form.append("teacher_id", teacherIds.toString());
			let messages = await axios.post(unreadStaffMessages.url, form);
			messages = messages.data;
			if (messages['generalmessages'].length > 0) {
				messages.generalmessages.map((message, key) => {
					let messageRealm = realm.objectForPrimaryKey('StaffMessages', message.schoolId + "/" + message.add_time);
					if (!messageRealm) {
						let id = message.schoolId + "/" + message.add_time;
						let obj = {
							id: id,
							message: message.message,
							read: false,
							messageType: "general",
							date: message.add_time,
							schoolName: message.schoolname,
						};
						StaffMessageService.save(obj);
					} else {
					}
				})
			}
			if (messages.privatemessages.length > 0) {
				messages.privatemessages.map((message, key) => {
					let messageRealm = realm.objectForPrimaryKey('StaffMessages', message.school_id + "/" + message.add_time + "/" + message.teacherId);
					if (!messageRealm) {
						let id = message.school_id + "/" + message.add_time + "/" + message.teacherId;
						let obj = {
							id: id,
							message: message.private_message,
							read: false,
							messageType: "private",
							st_acadamy_no: message.st_acadamy_no,
							teacherId: message.teacherId,
							date: message.add_time,
							schoolName: message.schoolname,
						};
						StaffMessageService.save(obj);
					} else {
					}
				})
			}
			dispatch(getStaffMessageCount())

		} catch (err) {
			console.log(err);
			// Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

function getMessageCount(getState, dispatch) {
	let mesgCount = {};
	let reduxStudents = getState().student.students;
	reduxStudents = reduxStudents.map((reduxStudent, key) => {
		try {
			const q = `studentId = "${reduxStudent.id}" OR (id BEGINSWITH "${reduxStudent.school_id}" AND send_by = 'school' ) OR (id BEGINSWITH "${reduxStudent.school_id}" AND send_by = 'acadamic_no' AND st_acadamy_no = "${reduxStudent.st_acadamy_no ? reduxStudent.st_acadamy_no.substring(0, 2) : ''}") OR (id BEGINSWITH "${reduxStudent.school_id}" AND send_by = "class" AND class_id = "${reduxStudent.class}") OR (id BEGINSWITH "${reduxStudent.school_id}" AND send_by = "class_and_div" AND class_id = "${reduxStudent.class}" AND div = "${reduxStudent.div}")`;
			let count = realm.objects('Message').filtered(q)
			.filtered(`read == false`).length;
			reduxStudents = getState().student.students;
			// let index = reduxStudents.findIndex(stud => stud.id == id)
			reduxStudent = {...reduxStudent, messageCount: count};
			return reduxStudent;
		} catch (e) {
			console.log(e.message);
		}
	});
	dispatch(getUnreadMessagesSuccess(reduxStudents))
}

function getStaffMessageCount() {
	return async function (dispatch, getState) {
		let mesgCount = {};
		let reduxStaffs = getState().staff.staffs;
		reduxStaffs = reduxStaffs.map((reduxStaff, key) => {
			let count = realm.objects('StaffMessages').filtered(`teacherId = "${reduxStaff.teacherId}" OR id BEGINSWITH "${reduxStaff.schoolId}"`)
				.filtered(`read == false`).length;
			reduxStaffs = getState().staff.staffs;
			// let index = reduxStudents.findIndex(stud => stud.id == id)
			reduxStaff = {...reduxStaff, messageCount: count};
			return reduxStaff;
		});
		dispatch(getUnreadStaffMessagesSuccess(reduxStaffs))
	}
}

export const messageReadSuccess = (mesgCount) => ({
	type: GET_MESSAGES,
	payload: mesgCount
});

export async function messageReadCountSuccess(msg, dispatch, getState) {
	//return async function (dispatch, getState) {
	let form = new FormData();
	//console.log("Message read..",msg);
	let messageType = msg.id.split('/');
	// console.log("Message Type..", messageType.length);
	if (msg.messageType === "general" || messageType.length > 1) {
		form.append("schoolId_addedtime", msg.id);
	} else {
		form.append("behaviourId", msg.id);

	}
	let presentDate = new Date().getDate();
	form.append("read_time", presentDate);
	let students = realm.objects('Student');
	let studentIds = students.map(student => student.id).join(',');
	form.append("studentId", studentIds);

	form.append("token_secure", SECURE_TOKEN);
	try {
		let obj = {
			id: msg.id,
			read: true,
		};

		MessageService.update(obj);

		let messgeReadApi = await axios.post(messageReadConst.url, form);
		//console.log("Result Response..",  messgeReadApi.data);
		getMessageCount(getState, dispatch)
	} catch (err) {
		console.log(err.message)
		//Alert.alert('Getting Messages Failed!', "");
		// dispatch(loadQuotesError())
	}
	//}
}


export function messageRead(msg, id) {
	return async function (dispatch, getState) {
		let form = new FormData();
		if (msg.messageType === "general") {
			form.append("schoolId_addedtime", msg.id);
			form.append("studentId", id);
		} else {
			form.append("behaviourId", msg.id);

		}
		let presentDate = new Date().getDate();
		form.append("read_time", presentDate);
		form.append("token_secure", SECURE_TOKEN);
		try {
			let obj = {
				id: msg.id,
				read: true,
			};

			MessageService.update(obj);

			let messgeReadApi = await axios.post(messageReadConst.url, form);
			//console.log("Result Response..",  messgeReadApi.data);
			getMessageCount(getState, dispatch)
		} catch (err) {
			console.log(err)
			//Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function staffmessageRead(msg) {
	return async function (dispatch, getState) {
		let form = new FormData();
		if (msg.messageType === "general") {
			form.append("schoolId_addedtime", msg.id);
		} else {
			form.append("schoolId_teacherId", msg.id);
		}
		form.append("teacherId", getState().staff.staffs[0].teacherId);
		let presentDate = new Date().getDate();
		form.append("read_time", presentDate);
		form.append("token_secure", SECURE_TOKEN);

		try {
			let parmsval = msg.id;
			let parmsarray = parmsval.split("/");
			let schoold = parmsarray[0];
			let added_time = parmsarray[1];
			let teacherId = Number(parmsarray[2]);
			let obj = {
				id: msg.id,
				teacherId: teacherId,
				read: true,
			};
			StaffMessageService.update(obj);
			let messgeReadApi = await axios.post(staffMessageReadConst.url, form);
			dispatch(getStaffMessageCount())
		} catch (err) {
			console.log(err);
			// Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function staffmessageReadCountSuccess(msg) {
	return async function (dispatch, getState) {
		let form = new FormData();
		if (msg.messageType === "general") {
			form.append("schoolId_addedtime", msg.id);
		} else {
			form.append("schoolId_teacherId", msg.id);

		}
		form.append("teacherId", getState().staff.staffs[0].teacherId);
		let presentDate = new Date().getDate();
		form.append("read_time", presentDate);
		form.append("token_secure", SECURE_TOKEN);

		try {
			let parmsval = msg.id;
			let parmsarray = parmsval.split("/");
			let schoold = parmsarray[0];
			let added_time = parmsarray[1];
			let teacherId = Number(parmsarray[2]);

			let obj = {
				id: msg.id,
				teacherId: teacherId,
				read: true,

			};


			StaffMessageService.update(obj);

			let messgeReadApi = await axios.post(staffMessageReadConst.url, form);
			dispatch(getStaffMessageCount())
		} catch (err) {
			console.log(err.message)
			//	Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function messageDelete(msg) {
	return async function (dispatch, getState) {
		try {
			let obj = {
				id: msg.id,
			};
			MessageService.delete(obj);
			Alert.alert('Message Deleted', "");
			getMessageCount(getState, dispatch)
		} catch (err) {
			console.log(err);
			// Alert.alert('Getting Messages Failed!', "");
			// dispatch(loadQuotesError())
		}
	}
}

export function messageDeleteByArray(msgArray) {
	let count = 0;
	return async function (dispatch, getState) {
		msgArray.forEach(id => {
			try {
				let obj = {
					id: id,
				};
				MessageService.deleteById(obj);
			} catch (err) {
				console.log(err.message, 'err');
				// Alert.alert('Getting Messages Failed!', "");
			}
			count++;
		});

		if (count === msgArray.length) {
			msgArray.forEach(id => {
				try {
					let obj = {
						id: id,
					};
					console.log("Id...", id);
					messageReadCountSuccess(obj, dispatch, getState);
				} catch (err) {
					console.log(err.message)
					//Alert.alert('Getting Messages staus Failed!', "");
					// dispatch(loadQuotesError())
				}
			});
		}
	}

}


export function staffMessageDeleteByArray(msgArray) {
	// console.log("message delete " + msg.id)
	return async function (dispatch, getState) {
		let count = 0;
		msgArray.forEach(id => {
			let parmsval = id.id;
			let parmsarray = parmsval.split("/");
			let schoold = parmsarray[0];
			let added_time = parmsarray[1];
			let teacherId = Number(parmsarray[2]);
			try {
				let obj = {
					id: id.id,
					teacherId: teacherId
				};
				dispatch(staffmessageReadCountSuccess({...id}));
				StaffMessageService.deleteById(obj);
			} catch (err) {
				console.log(err);
				// Alert.alert('Deleting Messages Failed!', "");
			}
			count++;
		});
	}

}

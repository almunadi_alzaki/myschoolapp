import Axios from "axios";
import {token_secure, URLS} from "../api/URLS";
import {toFormData} from "../api/general";
import {getSavedAcademicCalendar, saveAcademicCalendar} from "../realm/timetable";
import {getSavedStaffContacts, saveStaffContacts} from "../realm/staff-contacts";
import {getSavedRightMenus, saveRightMenus} from "../realm/right-menu";

export const GET_RIGHT_MENU = 'GET_RIGHT_MENU';
export const GET_TIME_TABLE = 'GET_TIME_TABLE';
export const GET_TEACHERS = 'GET_TEACHERS';
export const GET_ACADEMIC_CALENDAR = 'GET_ACADEMIC_CALENDAR';
export const UPDATE_APP = 'UPDATE_APP';
export const GET_LOGGED_IN_USER = 'GET_LOGGED_IN_USER';
export const REMOVE_LOGGED_IN_USER = 'REMOVE_LOGGED_IN_USER';
export const GET_FULL_SCHOOL_TIMETABLE = 'GET_FULL_SCHOOL_TIMETABLE';

export const getRightMenu = (data) =>
	async (dispatch) => {
		dispatch(getRightMenuSuccess(getSavedRightMenus(data.school_id || data.schoolId, data.page_type, data.teacherId, data.page_link)));
		return Axios.post(URLS.GET_RIGHT_MENU, toFormData({...data, token_secure}))
			.then(({data: {rightmenus}}) => {
				saveRightMenus(rightmenus, data.school_id || data.schoolId, data.page_type, data.teacherId, data.page_link)
				return rightmenus;
			})
			.then(p => dispatch(getRightMenuSuccess(p)));
	}

export const getRightMenuSuccess = (rightMenus) => ({
	type: GET_RIGHT_MENU,
	rightMenus
});

export const setUpdateStatus = (status) => ({
	type: UPDATE_APP,
	status
});


export const getTimeTable = (data) =>
	async (dispatch) => {
		// dispatch(getTimeTableSuccess(getSavedTimeTable(data.class, data.div, data.schoolId, data.teacherId)));
		return Axios.post(URLS.GET_TIME_TABLE, toFormData({...data, token_secure}))
			.then(({data: {timetable}}) => {
				// saveTimeTable(timetable, data.class, data.div, data.schoolId, data.teacherId);
				return timetable;
			})
			.then(p => dispatch(getTimeTableSuccess(p)));
	}

export const getTimeTableSuccess = (timeTable) => ({
	type: GET_TIME_TABLE,
	timeTable
});


export const getTimeTableByTeacher = (data) =>
	async (dispatch) => {
		// dispatch(getTimeTableSuccess(getSavedTimeTable(data.schoolId, data.teacherId)));
		return Axios.post(URLS.GET_TIME_TABLE_BY_TEACHER, toFormData({...data, token_secure}))
			.then(({data: {timetable}}) => {
				// saveTimeTable(timetable, data.schoolId, data.teacherId);
				return timetable;
			})
			.then(p => dispatch(getTimeTableByTeacherSuccess(p)));
	}
export const getTimeTableByTeacherSuccess = (timeTable) => ({
	type: GET_TIME_TABLE,
	timeTable
});


export const getTimeTableByRoom = (data) =>
	async (dispatch) => {
		// dispatch(getTimeTableSuccess(getSavedTimeTable(data.schoolId, data.teacherId)));
		return Axios.post(URLS.GET_TIME_TABLE_BY_ROOM, toFormData({...data, token_secure}))
			.then(({data: {timetable}}) => {
				// saveTimeTable(timetable, data.schoolId, data.teacherId);
				return timetable;
			})
			.then(p => dispatch(getTimeTableByTeacherSuccess(p)));
	}
export const getTimeTableByRoomSuccess = (timeTable) => ({
	type: GET_TIME_TABLE,
	timeTable
});


export const getLocalTeachers = (schoolId, noCache) =>
	async (dispatch) => {
		dispatch(getTeachersSuccess(getSavedStaffContacts(schoolId)));
	}
export const getTeachers = (schoolId, noCache) =>
	async (dispatch) => {
		dispatch(getTeachersSuccess(getSavedStaffContacts(schoolId)));
		return Axios.post(URLS.GET_TEACHERS, toFormData({schoolId, token_secure}))
			.then(({data: {teachers}}) => {
				saveStaffContacts(teachers, schoolId);
				return teachers;
			})
			.then(p => dispatch(getTeachersSuccess(p)))
			.then(() => dispatch(setUpdateStatus({
				teachers: false
			})));
	}
export const getTeachersSuccess = (teachers) => ({
	type: GET_TEACHERS,
	teachers
});


export const getAcedemicCalendar = () =>
	async (dispatch) => {
		getSavedAcademicCalendar().then(calendar => dispatch(getAcedemicCalendarSuccess(calendar)));
		return Axios.post(URLS.GET_ACADEMIC_CALENDAR, toFormData({token_secure}))
			.then(async ({data: {calendar}}) => {
				const file = await saveAcademicCalendar(calendar);
				return file;
			})
			.then(p => dispatch(getAcedemicCalendarSuccess(p)))
	};
export const getAcedemicCalendarSuccess = (calendar) => ({
	type: GET_ACADEMIC_CALENDAR,
	calendar
});


export const getLoggedInUser = ({studentId, teacherId, schoolId}) =>
	async (dispatch) => {
		return Axios.post(URLS.LOGGED_IN_USER, toFormData({studentId, teacherId, schoolId, token_secure}))
			.then(({data: {permission}}) => {
				return permission;
			})
			.then(p => dispatch(getLoggedInUserSuccess(p)))
	};

export const getLoggedInUserSuccess = (permission) => ({
	type: GET_LOGGED_IN_USER,
	permission
});

export const removeLoggedInUser = ({teacherId, studentId}) => ({
	type: REMOVE_LOGGED_IN_USER,
	teacherId, studentId
});


export const getFullSchoolTimeTable = ({schoolId}) =>
	async (dispatch) => {
		return Axios.post(URLS.FULL_TIMETABLE, toFormData({schoolId, token_secure}))
			.then(({timetable}) => {
				return timetable;
			})
			.then(p => dispatch(getFullSchoolTimeTableSuccess(p)))
	};

export const getFullSchoolTimeTableSuccess = (timeTable) => ({
	type: GET_FULL_SCHOOL_TIMETABLE,
	timeTable
});



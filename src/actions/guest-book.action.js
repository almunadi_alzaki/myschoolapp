import Axios from "axios";
import {token_secure, URLS} from "../api/URLS";
import {toFormData} from "../api/general";

export const GET_GUEST_BOOKS = 'GET_GUEST_BOOKS';
export const ADD_GUEST_BOOK = 'ADD_GUEST_BOOK';
export const UPDATE_GUEST_BOOK = 'UPDATE_GUEST_BOOK';

export const getGuestBooks = (data) =>
    async (dispatch) => Axios.post(URLS.GUEST_BOOKS.GET, toFormData({...data, token_secure}))
        .then(p => p.data.visitors || [])
        .then(p => dispatch(getGuestBooksSuccess(p)));

export const getGuestBooksSuccess = (visitors) => ({
    type: GET_GUEST_BOOKS,
    visitors
});

export const addGuestBook = (data) => {
    addGuestBookSuccess({result: ""});
    return async (dispatch) => Axios.post(URLS.GUEST_BOOKS.ADD, toFormData({...data, token_secure}))
        .then(p => dispatch(addGuestBookSuccess(p.data)))
        .catch(p => dispatch(addGuestBookSuccess({result: "error"})));
}

export const addGuestBookSuccess = (result) => ({
    type: ADD_GUEST_BOOK,
    result
});


export const updateGuestBook = (data) =>
    async (dispatch) => Axios.post(URLS.GUEST_BOOKS.ADD, toFormData({...data, token_secure}))
        .then(p => p.data.timetable)
        .then(p => dispatch(updateGuestBookSuccess(p)));

export const updateGuestBookSuccess = (timeTable) => ({
    type: UPDATE_GUEST_BOOK,
    timeTable
});

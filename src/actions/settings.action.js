import realm from "../realm/schema";
import { Platform, ToastAndroid } from "react-native";
import axios from "axios";
import { token_secure, URLS } from "../api/URLS";

export const SET_NOTIFICATION_SOUND = 'SET_NOTIFICATION_SOUND';
export const SET_NOTIFICATION_POPUP = 'SET_NOTIFICATION_POPUP';

export function setNotificationSound(notificationSound) {
  return async function (dispatch, getState) {
    try {
      console.log(getState().students);
      if (getState().student.students.length) {
        getState().student.students.forEach(s => {
          let form = new FormData();
          form.append("token_secure", token_secure);
          form.append("token", getState().locale.deviceToken);
          form.append("sound_code", notificationSound);
          // form.append("popup_settings", getState().settings.popup_settings);
          // form.append("visibility", getState().settings.visibility);
          form.append("studentId", s.id);
          axios.post(URLS.SOUND_SETTINGS, form);
        })
      }
      if (getState().staff.staffs.length) {
        getState().staff.staffs.forEach(s => {
          let form = new FormData();
          form.append("token_secure", token_secure);
          form.append("sound_code", notificationSound);
          form.append("token", getState().locale.deviceToken);
          // form.append("popup_settings", getState().settings.popup_settings);
          // form.append("visibility", getState().settings.visibility);
          form.append("teacherId", s.teacherId);
          axios.post(URLS.SOUND_SETTINGS, form);
        })
      }
      realm.write(() => {
        realm.create('Settings', {id: "settings", sound_code: notificationSound}, true);
      });
      dispatch(setNotificationSoundSuccess(notificationSound));
      Alert.alert('Sound settings  Changed');
    } catch (err) {
      console.log(err)
    }
  }
}


export const setNotificationSoundSuccess = sound_code => ({
  type: SET_NOTIFICATION_SOUND,
  sound_code
})


export function setNotificationPopup(popup_settings, visibility) {
  return async function (dispatch, getState) {
    try {
      let form = new FormData();
      form.append("token_secure", token_secure);
      form.append("device_type", Platform.OS);
      form.append("token", getState().locale.deviceToken);
      form.append("sound_code", getState().settings.notificationPopup);
      form.append("popup_settings", popup_settings);
      await axios.post(URLS.ADD_TOKEN, form);

      realm.write(() => {
        realm.create('Settings', {id: "settings", popup_settings: popup_settings}, true);
      });
      dispatch(setNotificationPopupSuccess(popup_settings, visibility));
      ToastAndroid.show('Notification popup settings has  Changed', ToastAndroid.SHORT);
    } catch (err) {
      console.log(err)
    }
  }
}

export const setNotificationPopupSuccess = (popup_settings, visibility) => ({
  type: SET_NOTIFICATION_POPUP,
  popup_settings, visibility
})

import React, { Component } from 'react';
import { Text, TouchableOpacity, ActivityIndicator } from 'react-native';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <TouchableOpacity {...this.props} style={{
                backgroundColor: this.props.disabled ? 'grey' : this.props.color,
                color: '#fff',
                padding: 10,
                width: this.props.width ? this.props.width : null ,
                borderRadius: 5,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 1,
                },
                shadowOpacity: 0.20,
                shadowRadius: 1.41,
                elevation: 2,
            }}>
                {this.props.loading ?
                    <React.Fragment>
                        <ActivityIndicator size="small" color="#fff" />
                        <Text style={{ color: '#fff', fontWeight: 'bold'  }}>{this.props.title}</Text>
                    </React.Fragment>
                    : <Text style={{ color: '#fff',fontWeight: 'bold' }}>{this.props.title}</Text>
                }
            </TouchableOpacity>
        )
    }
}

export default Button;

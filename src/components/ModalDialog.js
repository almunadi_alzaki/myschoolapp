import React, {Component} from 'react';
import {Modal, Text, TouchableOpacity,StyleSheet, View} from 'react-native';

class ModalDialog extends Component {
    state = {
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.show}>
                <View style={styles.modal}>
                    <View style={styles.view}>
                        <Text style={styles.text}>{this.props.text}</Text>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={this.props.onAction}>
                            <Text>{this.props.buttonTitle}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    view: {
        backgroundColor: '#fff',
        height: '50%',
        width: '90%',
        elevation: 1,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#eee',
        justifyContent: 'center',
        alignItems: 'center'
    }, modal: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor:'rgba(0,0,0,0.6)'
    },
    text: {fontSize: 22, textAlign: 'center'},
    button: {marginTop: 40, backgroundColor: '#eee', padding: 10}
})
export default ModalDialog;

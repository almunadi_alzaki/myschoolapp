import React, { Component } from 'react';
import { TouchableOpacity, View } from 'react-native';

class CheckBox extends Component {
  static defaultProps = {
    size: 24
  }

  constructor(props) {
    super(props);
    this.state = {isCheck: false};
  }

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.value !== this.props.value) {
      this.setState({isCheck: nextProps.value});
    }
  }

  checkClicked = async () => {
    await this.setState(prevState => ({
      isCheck: !prevState.isCheck,
    })); // setState is async function.

    // Call function type prop with return values.
    this.props.clicked && this.props.clicked(this.props.value, this.state.isCheck);
  }

  render() {
    return (
        <TouchableOpacity onPress={this.checkClicked} style={this.props.style}>
          <View style={{
            height: this.props.size,
            width: this.props.size,
            borderWidth: 2,
            borderColor: '#000',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <View style={{
              height: this.props.size/2,
              width: this.props.size/2,
              backgroundColor: this.state.isCheck ? '#5f021f' : '#FFF',
            }}/>
          </View>
        </TouchableOpacity>
    )
  }
}

export default CheckBox;

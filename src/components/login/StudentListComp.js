import React from 'react';
import {Image, Text, TextInput, TouchableHighlight, View} from 'react-native';
import {Badge} from 'react-native-elements'
import Mail from '../../imagesicon/message_14.png';
import StyleSheetFactory from '../../styles/Ar_loginStyles.js';
import Icon from 'react-native-vector-icons/Ionicons'

class StudentListComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isRTL: this.props.locale.isRTL};
        // I18n.locale = this.state.locale;
    }

    studentCall = (student) => {
        if (this.state.disableCall) return;
        this.setState({disableCall: true, text: ''});
        setTimeout(() => this.setState({disableCall: false}), 180 * 1e3);
        this.props.studentCall(student.school_id, student.id, this.state.text, student.st_name, student.username,);
    }

    render() {
        styles = StyleSheetFactory.getSheet(this.state.isRTL);
        const {navigate,} = this.props.navigation;

        return (
            <View style={{
                flex: 0,
                flexDirection: 'row',
                backgroundColor: '#f2f2f2',
                padding: 6,
                marginBottom: 5,
                justifyContent: 'space-between'
            }}>
                <View style={styles.ColSet10}>
                    <TextInput style={styles.secretCol}
                               value={this.state.text}
                               maxLength={1}
                               onChangeText={(text) => this.setState({text})}
                               placeholder="" underlineColorAndroid="transparent"/>
                </View>
                <View style={styles.ColSet60}>
                    <Text style={{
                        textAlign: 'left',
                        color: 'black',
                        paddingLeft: this.state.isRTL ? 0 : 3,
                        paddingRight: this.state.isRTL ? 3 : 0,
                        padding: 6
                    }}>
                        {this.props.student.st_name}</Text>
                </View>
                <TouchableHighlight
                    onPress={() => this.studentCall(this.props.student)}>
                    <View
                        style={[{
                            color: 'black',
                            backgroundColor: !this.state.disableCall ? 'green' : 'red',
                            padding: 5,
                            borderRadius: 2,
                            marginTop: 6
                        }, styles.iconSizecall]}>
                        {/*<Image style={styles.iconSizecall} source={Call}/>*/}
                        <Icon size={18} style={{alignSelf: 'center'}} color="#fff"
                              name="md-call"/>
                    </View>
                </TouchableHighlight>

                <TouchableHighlight onPress={() => this.props.navigation('MessageScreen', {
                    studentId: this.props.student.id,
                    schoolId: this.props.student.school_id,
                    class: this.props.student.classId,
                    div: this.props.student.div,
                    academic_no: this.props.student.st_acadamy_no,
                })}>
                    <View style={styles.ColSetMsg}>
                        <Image style={{height: 30, width: 30, marginTop: 6}} source={Mail}/>
                        <Badge
                            value={this.props.student.messageCount}
                            containerStyle={{width: 30, position: 'absolute', top: -4, left: -4}}
                        />
                    </View>
                </TouchableHighlight>
            </View>

        );
    }
}

export default StudentListComp

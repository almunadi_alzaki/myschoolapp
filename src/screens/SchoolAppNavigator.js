import React from "react";
import { Image, TouchableOpacity, View, I18nManager } from "react-native";
import LoginScreen from './LoginScreen.js';
import StaffScreen from './StaffScreen/StaffScreen.js';
import StaffLoginScreen from './StaffScreen/LoginScreen.js';
import MessageScreen from './MessageScreen.js';
import StaffMessageScreen from './StaffMessageScreen.js';
import MessageDetails from './MessageDetails.js';
import StaffMessageDetails from './StaffMessageDetails.js';
import StudentFilter from './StudentFilter.js';
import SendBehaviour from './SendBehaviour.js';
import ViewExecuse from './ViewExecuse.js';
import AddSubSchoolScreen from './AddSubSchoolScreen.js';
import ViewTodayBehaviours from './ViewTodayBehaviours.js';
import ConfirmExcuse from './ConfirmExcuse.js';
import LanguageScreen from './LanguageScreen.js';
import NotificationScreen from './NotificationScreen.js';
import SoundScreen from './SoundSetting.js';
import DeleteStudent from './DeleteStudent.js';
import TeacherScreen from './TeacherScreen/TeacherScreen.js';
import SideMenuI from '../images/menu-lines.png';
import RightMenuScreen from './RightMenuScreen.js';
import I18n from '../i18n.js';
import AddStaffScreen from './StaffScreen/AddStaffScreen.js';
// import RightSideMenu from './RightMenuScreen';
import LeftSideMenu from './LeftMenuScreen';
//import MainScreenNavigator from "../ChatScreen/index.js";
//import Profile from "../ProfileScreen/index.js";
//import SideBar from "../SideBar/SideBar.js";
import { createAppContainer, createDrawerNavigator, createStackNavigator, DrawerActions } from "react-navigation";
import TimeTable from "./TimeTable";
import AcademicCalendar from "./AcademicCalendar";
import GuestBookList from "./GuestBook/GuestBookList";
import GuestBookDetails from "./GuestBook/GuestBookDetails";
import EmployeePhones from "./EmployeePhones";
import AddGuestBook from "./GuestBook/AddGuestBook";
import FullTimeTable from "./FullTimeTable";
import TeacherVisitScreen from "./TeacherVisit";


export const LeftDrawerButton = (props) => {
    return (
        <View style={{ marginLeft: 6, marginRight: 5 }}>
            <TouchableOpacity
                onPress={() => props.navigation.toggleInnerDrawer()}>
                <Image
                    source={SideMenuI}
                />
            </TouchableOpacity>
        </View>
    );
};

export const RightDrawerButton = (props) => {
    return (
        <View style={{ marginRight: 6, marginLeft: 5 }}>
            <TouchableOpacity
                onPress={() => props.navigation.toggleOuterDrawer()}>
                <Image
                    source={SideMenuI}
                />
            </TouchableOpacity>
        </View>
    );
};


const AppNavigator = createStackNavigator(
    {
        Login: { screen: LoginScreen, navigationOptions: () => ({ headerTitle: I18n.t('School App'), }) },
        StaffLogin: { screen: StaffLoginScreen, navigationOptions: () => ({ headerTitle: I18n.t('StaffLogin'), }) },
        AddStaff: { screen: AddStaffScreen, navigationOptions: () => ({ headerTitle: I18n.t('AddStaff'), }) },
        Staff: { screen: StaffScreen },
        TimeTable: { screen: TimeTable, navigationOptions: () => ({ headerTitle: I18n.t('TimeTable'), }) },
        FullTimeTable: { screen: FullTimeTable, navigationOptions: () => ({ headerTitle: I18n.t('FullTimeTable'), }) },
        GuestBookList: { screen: GuestBookList, navigationOptions: () => ({ headerTitle: I18n.t('GuestBookList'), }) },
        GuestBookDetails: {
            screen: GuestBookDetails,
            navigationOptions: () => ({ headerTitle: I18n.t('GuestBookDetails'), })
        },
        AddGuestBook: { screen: AddGuestBook, navigationOptions: () => ({ headerTitle: I18n.t('AddGuestBook'), }) },
        EmployeePhones: { screen: EmployeePhones, navigationOptions: () => ({ headerTitle: I18n.t('EmployeePhones'), }) },
        AcademicCalendar: {
            screen: AcademicCalendar,
            navigationOptions: () => ({ headerTitle: I18n.t('AcademicCalendar'), })
        },
        Teacher: { screen: TeacherScreen },
        TeacherVisit: { screen: TeacherVisitScreen, navigationOptions: () => ({ headerTitle: I18n.t('TeacherVisit'), }) },
        StudentFilter: { screen: StudentFilter },
        SendBehaviour: { screen: SendBehaviour, },
        MessageScreen: { screen: MessageScreen, navigationOptions: () => ({ headerTitle: I18n.t('MessageScreen'), }) },
        StaffMessageScreen: {
            screen: StaffMessageScreen,
            navigationOptions: () => ({ headerTitle: I18n.t('StaffMessageScreen'), })
        },
        MessageDetails: { screen: MessageDetails, navigationOptions: () => ({ headerTitle: I18n.t('MessageDetails'), }) },
        StaffMessageDetails: {
            screen: StaffMessageDetails,
            navigationOptions: () => ({ headerTitle: I18n.t('StaffMessageDetails'), })
        },
        ViewExecuse: { screen: ViewExecuse, navigationOptions: () => ({ headerTitle: I18n.t('ViewExecuse'), }) },
        ViewTodayBehaviours: {
            screen: ViewTodayBehaviours,
            navigationOptions: () => ({ headerTitle: I18n.t('ViewTodayBehaviours'), })
        },
        ConfirmExcuse: { screen: ConfirmExcuse, navigationOptions: () => ({ headerTitle: I18n.t('ConfirmExcuse'), }) },
        AddSubSchoolScreen: {
            screen: AddSubSchoolScreen,
            navigationOptions: () => ({ headerTitle: I18n.t('AddSubSchoolScreen'), })
        },
    },
    {
        initialRouteName: "Login",
        defaultNavigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: '#5f021f',
                height: 40,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                textAlign: 'center',
                flex: 1
            },
            headerLeft: <LeftDrawerButton navigation={navigation} />,
            headerRight: <RightDrawerButton navigation={navigation} />
        }),
    },
);

const NotificationStack = createStackNavigator(
    {
        NotificationSetting: NotificationScreen,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            title: 'Notification Settings',
            headerStyle: {
                backgroundColor: '#5f021f',
                height: 40,
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                textAlign: 'center',
                flex: 1
            },
        }),

    }
);

const LanguageStack = createStackNavigator(
    {
        LanguageSetting: LanguageScreen,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            title: 'Language Settings',
            headerStyle: {
                backgroundColor: '#5f021f',
                height: 40
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                textAlign: 'center',
                flex: 1
            },
        }),

    }
);

const SoundStack = createStackNavigator(
    {
        SoundSetting: SoundScreen,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            title: 'Sound Settings',
            headerStyle: {
                backgroundColor: '#5f021f',
                height: 40
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                textAlign: 'center',
                flex: 1
            },
        }),

    }
);

const DeleteStudentStack = createStackNavigator(
    {
        DeleteStudent: DeleteStudent,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
            title: 'Delete Student ',
            headerStyle: {
                backgroundColor: '#5f021f',
                height: 40
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                textAlign: 'center',
                flex: 1
            },
        }),

    }
);


const InnerDrawer = createDrawerNavigator(
    {
        inbox: AppNavigator,
        NotificationStack,
        LanguageStack,
        SoundStack,
        DeleteStudentStack,
    },
    {
        // defaultNavigationOptions: () => ({header: null}),
        drawerPosition: I18nManager.isRTL ? 'right' : 'left',
        getCustomActionCreators: (route, stateKey) => {
            // console.log('inner: ' + stateKey);
            return {
                toggleInnerDrawer: () => DrawerActions.toggleDrawer({ key: stateKey }),
            };
        },
        contentComponent: props => <LeftSideMenu {...props} />,

    }
);
//
// const StackNav = createStackNavigator({
//     drawerLeft: {
//         screen: InnerDrawer,
//         navigationOptions: () => ({header: null})
//     },
// });

const OuterDrawer = createDrawerNavigator(
    {
        stackNav: InnerDrawer,
    },
    {
        drawerPosition: I18nManager.isRTL ? 'left' : 'right',
        getCustomActionCreators: (route, stateKey) => {
            return {
                toggleOuterDrawer: () => DrawerActions.toggleDrawer({ key: stateKey }),
            };
        },
        contentComponent: props => <RightMenuScreen  {...props} />,
    }
);

export default createAppContainer(OuterDrawer);

import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	Text,
	TouchableHighlight,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View
} from 'react-native';
import {messageDelete, messageDeleteByArray} from '../actions/messageActions';
import styles from '../styles/LoginStyles';
import MailGeneralRead from '../imagesicon/message_general_read.png';
import MailGeneralUnRead from '../imagesicon/message_general.png';
import MailPrivateRead from '../imagesicon/message_read_private.png';
import MailPrivateUnRead from '../imagesicon/message_private.png';
import MessageDeleteIcon from '../imagesicon/deleteIcon.png';
import {connect} from 'react-redux';
import realm from '../realm/schema';
import CheckBox from '../components/Checkbox'
import {trim} from "../utils";
import {Icon} from "react-native-elements";
import i18n from "../i18n";

class MessageScreen extends React.Component {
	constructor(props) {
		super(props);
		let isRTL = this.props.locale.locale == "ar" ? true : false;
		this.state = {
			isRTL: isRTL,
			checkSelected: [],
			//checkeditems: false,
			messages: []
		};
	}

	componentDidMount() {

		this.props.navigation.setParams({});
		let params = this.props.navigation.state.params;
		console.log(params);
		messages = realm.objects('Message')
			.filtered(`studentId = "${params.studentId}" OR (id BEGINSWITH "${params.schoolId}" AND send_by = 'school' )  OR (id BEGINSWITH "${params.schoolId}"  AND send_by = 'acadamic_no' AND st_acadamy_no = "${params.academic_no ? params.academic_no.substring(0, 2) : ''}") OR (id BEGINSWITH "${params.schoolId}" AND send_by = "class" AND class_id = "${params.class}") OR (id BEGINSWITH "${params.schoolId}" AND send_by = "class_and_div" AND class_id = "${params.class}" AND div = "${params.div}")`)
			.sorted('date', true);
		console.log(messages, realm.objects('Message').map(p => ({...p})));
		updateUI = () => {
			this.setState({
				isRTL: this.props.locale.isRTL,
				messages: messages,
			})
		}
		realm.addListener('change', updateUI);
		this.setState({
			isRTL: this.props.locale.isRTL,
			messages: (messages),
		})
	}

	static navigationOptions = ({navigation}) => {
		return Platform.OS === 'ios' && {
			headerLeft: (
				<View style={{flexDirection: 'row'}}>
					{/*<LeftDrawerButton navigation={navigation}/>*/}
					<TouchableOpacity
						style={{marginRight: 10, marginLeft: 10,}}
						onPress={() => navigation.pop()}
					>
						<Icon size={24} style={{alignSelf: 'center'}} color="#fff"
						      name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
					</TouchableOpacity>
				</View>
			)
		};
	};


	deleteMessage = () => {
		// console.log("Delete message ..",this.state.checkSelected);
		// alert(this.state.checkSelected); // logging
		//this.props.messageDeleteByArray()
		this.props.messageDeleteByArray(this.state.checkSelected)
	}
	toggleCheckBox = (id, isCheck) => {
		let {checkSelected} = this.state;
		if (isCheck) {
			checkSelected.push(id);
		} else { // remove element
			var index = checkSelected.indexOf(id);
			if (index > -1) {
				checkSelected.splice(index, 1);
			}
		}
		this.setState({checkSelected});
		//alert(this.state.checkSelected); // logging
	}

	render() {
		const checkboxs = this.state.messages.map((message, key) =>
			<CheckBox style={{marginTop: 50,}} key={message.id} value={message.id}
			          clicked={() => this.toggleCheckBox(message.id, isCheck)}/>
		)
		const {navigate} = this.props.navigation;
		let studentId = this.props.navigation.state.params.studentId;
		return (
			<View style={styles.container}>
				<View style={styles.containerCenter}>
					<View style={{
						flex: 0,
						margin: 8,
						marginRight: 20,
						alignSelf: this.state.isRTL ? 'flex-start' : 'flex-end'
					}}>
						<TouchableHighlight onPress={() => this.deleteMessage()}>
							<Image style={{height: 30, width: 30}} source={MessageDeleteIcon}/>
						</TouchableHighlight>
					</View>

					<ScrollView>
						{this.state.messages.map((message, key) => {
							return <View key={message.id}>
								<TouchableWithoutFeedback
									onPress={() => navigate('MessageDetails', {message: message, studentId})}>
									<View style={{
										flex: 0,
										flexDirection: this.state.isRTL ? 'row-reverse' : 'row',
										backgroundColor: '#f9f9f9',
										padding: 6,
										borderBottomWidth: 1,
										borderBottomColor: '#f0f0f0',
										justifyContent: 'space-between'
									}}>

										<View style={styles.ColSet20}>
											{message.messageType === "private" ?
												message.read ?
													<Image style={{height: 30, width: 40,}} source={MailPrivateRead}/>
													:
													<Image style={{height: 30, width: 40,}} source={MailPrivateUnRead}/>
												:
												message.read ?
													<Image style={{height: 30, width: 40,}} source={MailGeneralRead}/>
													:
													<Image style={{height: 30, width: 40,}} source={MailGeneralUnRead}/>
											}
										</View>

										<View style={styles.ColSet70}>
											<Text style={{
												color: 'black',
												paddingLeft: 10,
												textAlign: this.state.isRTL ? 'right' : 'left'
											}}>{message.schoolName}</Text>
											<Text style={{
												color: 'black',
												paddingLeft: 10,
												textAlign: this.state.isRTL ? 'right' : 'left'
											}}>{trim(message.message)}
												<Text style={{
													color: 'grey',
													fontSize: 10,
													textAlign: this.state.isRTL ? 'right' : 'left'
												}}>   {message.date}</Text></Text>
										</View>
										<View style={styles.ColSet10}>
											<CheckBox size={30} style={{}} key={message.id} value={message.id}
											          clicked={(id, isCheck) => this.toggleCheckBox(message.id, isCheck)}/>

										</View>
									</View>
								</TouchableWithoutFeedback>
							</View>
						})}
					</ScrollView>
				</View>
			</View>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		locale: state.locale,
		student: state.student
	}
}

const mapDispatchToProps = dispatch => ({
	messageDelete: (msg) => dispatch(messageDelete(msg)),
	messageDeleteByArray: (id) => dispatch(messageDeleteByArray(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(MessageScreen)

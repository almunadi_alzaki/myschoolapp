import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  CheckBox,
  TouchableHighlight,
  Platform,
  TouchableOpacity
} from 'react-native';
//import styles from '../styles/LoginStyles';
import { RadioButton } from 'react-native-paper';
import { connect } from 'react-redux';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import DeleteIcon from '../imagesicon/deleteIcon.png';
import { setLocale } from '../actions/actions';
import I18n from '../i18n.js';
import Button from "../components/Button";
import { Icon } from "react-native-elements";
import i18n from "../i18n";

class AddSubSchoolScreen extends React.Component {
  state = {
    checked: this.props.locale.locale,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {

  }
  static navigationOptions = ({ navigation }) => {
    return Platform.OS === 'ios' && {
      headerLeft: (
        <View style={{ flexDirection: 'row' }}>
          {/*<LeftDrawerButton navigation={navigation}/>*/}
          <TouchableOpacity
            style={{ marginRight: 10, marginLeft: 10, }}
            onPress={() => navigation.pop()}
          >
            <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
              name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
          </TouchableOpacity>
        </View>
      )
    };
  };


  render() {
    styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);


    // const { checked } = this.state;
    const checked = this.props.locale.locale
    return (
      <View style={styles.container} >


        <View style={styles.containerCenter}>
          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', padding: 12, borderBottomWidth: 1, borderBottomColor: '#f0f0f0', justifyContent: 'center' }}>
            <View>

              <TextInput style={styles.inputSchool}
                onChangeText={(schoolId) => this.setState({ schoolId })}
                placeholder={I18n.t('School ID')} />
            </View>
            <View>

              <Button style={{ height: 25 }} color="#5f021f" title={I18n.t('Add')}

                onPress={this.addStudent} />
            </View>

          </View>

          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', padding: 12, borderBottomWidth: 1, borderBottomColor: '#f0f0f0' }}>

            <View style={styles.ColSet90}>
              <View style={styles.textAlignDiv}>
                <Text style={{ color: 'black', fontSize: 18 }}>97Uu</Text>
              </View>
            </View>
            <View style={styles.ColSet10}>
              <TouchableHighlight onPress={() => this.deleteSchool()}>
                <Image style={{ height: 30, width: 30, }} source={DeleteIcon} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', padding: 12, borderBottomWidth: 1, borderBottomColor: '#f0f0f0' }}>

            <View style={styles.ColSet90}>
              <View style={styles.textAlignDiv}>
                <Text style={{ color: 'black', fontSize: 18 }}>80LC</Text>
              </View>
            </View>
            <View style={styles.ColSet10}>
              <TouchableHighlight onPress={() => this.deleteSchool()}>
                <Image style={{ height: 30, width: 30, }} source={DeleteIcon} />
              </TouchableHighlight>
            </View>
          </View>

          <View style={{ flex: 0, flexDirection: this.state.isRTL ? 'row-reverse' : 'row', padding: 12, borderBottomWidth: 1, borderBottomColor: '#f0f0f0' }}>

            <View style={styles.ColSet90}>
              <View style={styles.textAlignDiv}>
                <Text style={{ color: 'black', fontSize: 18 }}>12UT</Text>
              </View>
            </View>
            <View style={styles.ColSet10}>
              <TouchableHighlight onPress={() => this.deleteSchool()}>
                <Image style={{ height: 30, width: 30, }} source={DeleteIcon} />
              </TouchableHighlight>
            </View>
          </View>

        </View>

      </View>

    );
  }
}
const mapStateToProps = (state) => {
  const { login } = state
  return {
    locale: state.locale,
    isRTL: state.isRTL,

  }
}

const mapDispatchToProps = dispatch => ({
  setLocale: (locale) => dispatch(setLocale(locale))
})


export default connect(mapStateToProps, mapDispatchToProps)(AddSubSchoolScreen)

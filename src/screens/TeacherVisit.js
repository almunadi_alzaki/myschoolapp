import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Platform } from 'react-native';
import i18n, { strings } from "../i18n";
import { connect } from "react-redux";


class TeacherVisit extends React.Component {

    state = {
        table: [
            { rows: 'test', data: 'w', performance: 'l' },
            { rows: 'test1', data: 'w', performance: 'l' },
            { rows: 'test2', data: 'w', performance: 'l' },
            { rows: 'test3', data: 'w', performance: 'l' },
            { rows: 'test4', data: 'w', performance: 'l' },
        ]
    };

    constructor(props) {
        super(props);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
    }

    static navigationOptions = ({ navigation }) => {
        // return {
        //     headerTitle: 'Teacher Visit'
        // }
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    }

    componentDidMount() {

    }

    render() {
        return (
            <View>
                <View style={styles.headerContainer}>
                    <View style={styles.header}>
                        <View style={styles.headerData}>
                            <Text>test</Text>
                        </View>
                        <View style={styles.headerData}>
                            <Text>2</Text>
                        </View>
                        <View style={styles.headerData}>
                            <Text>3</Text>
                        </View>
                    </View>
                    <View style={styles.taskView}>
                        <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                            <Text>performance</Text>
                        </View>
                        <View style={styles.taskHeader}>
                            <View style={[styles.taskData, { borderLeftWidth: 0 }]}>
                                <Text>1</Text>
                            </View>
                            <View style={styles.taskData}>
                                <Text>2</Text>
                            </View>
                            <View style={[styles.taskData, { borderRightWidth: 0 }]}>
                                <Text>3</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.visits}>
                        <TouchableOpacity style={styles.visitButton}><Text>1</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.visitButton}><Text>2</Text></TouchableOpacity>
                        <TouchableOpacity style={styles.visitButton}><Text>3</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={styles.headerContainer}>
                    <View style={styles.header}>
                        <View style={styles.headerData}>
                            <Text>1</Text>
                        </View>
                        <View style={styles.headerData}>
                            <Text>2</Text>
                        </View>
                        <View style={styles.headerData}>
                            <Text>3</Text>
                        </View>
                    </View>
                    <View style={styles.table}>
                        <View style={styles.tableHeader}>
                            <View style={[styles.tableData, { borderLeftWidth: 1 }]}>
                                <Text>1</Text>
                            </View>
                            <View style={styles.tableData}>
                                <Text>2</Text>
                            </View>
                            <View style={[styles.tableData, { borderRightWidth: 2 }]}>
                                <Text>3</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ width: '40%', alignItems: 'center' }}>
                        {/* <Text>{p.rows}</Text> */}
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    headerContainer: {
        borderWidth: 1,
        flexDirection: 'row'
    },
    header: {
        width: '30%',
        flexDirection: 'row'
    },
    headerData: {
        width: '33.33%',
        borderRightWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    taskView: {
        flex: 3,
        borderWidth: 1
    },
    taskHeader: {
        width: '100%',
        borderTopWidth: 1,
        flexDirection: 'row'
    },
    taskData: {
        width: '33.33%',
        borderRightWidth: 1,
        alignItems: 'center'
    },
    visits: {
        flexDirection: 'row',
        width: '40%',
        justifyContent: 'space-around',
        alignItems: 'center',

    },
    visitButton: {
        width: 30,
        height: 30,
        borderWidth: 1,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 2
    },
    table: {
        flex: 3,
    },
    tableHeader: {
        width: '100%',
        flexDirection: 'row'
    },
    tableData: {
        width: '33.33%',
        borderRightWidth: 1,
        borderTopWidth: 0,
        alignItems: 'center'
    },
    textView: {
        width: 25,
        height: 70,
        margin: 1
    },
    textRotat: {
        display: 'flex',
        transform: [{ rotate: '90deg' }, { translateX: 20 }, { translateY: 23 }],
        paddingLeft: 10,
        fontSize: 16,
        alignItems: 'center',
        width: 70,
        height: 25,
        color: '#5f021f',
        fontWeight: 'bold',
        backgroundColor: '#ebebeb',
    },

    textRotatAr: {
        display: 'flex',
        transform: [{ rotate: '270deg' }, { translateX: -20 }, { translateY: 23 }],
        paddingLeft: 10,
        fontSize: 16,
        alignItems: 'center',
        textAlign: 'center',
        width: 70,
        height: 25,
        color: '#5f021f',
        fontWeight: 'bold',
        backgroundColor: '#ebebeb',

    },
});
const mapStateToProps = state => {
    return {
        staffs: state.staff.staffs,
        locale: state.locale,
    }
}

const mapDispatchToProps = dispatch => ({});


export default connect(mapStateToProps, mapDispatchToProps)(TeacherVisit);

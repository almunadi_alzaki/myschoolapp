import React from 'react';
import {
    FlatList,
    Image,
    Linking,
    Platform,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from "react-redux";
import I18n from "../i18n";

import callIcon from '../imagesicon/loginscreen_12.png';
import {getLocalTeachers, getTeachers} from "../actions/general.action";
import { Icon } from "react-native-elements";

class EmployeePhones extends React.Component {

    constructor(props) {
        super(props);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
    }

    state = {};
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };


    componentDidMount() {

        this.props.getTeachers(this.props.staff.schoolId);
    }

    callNumber = phone => {
        console.log('callNumber ----> ', `${'00' + phone}`);
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
            phoneNumber = `telprompt:${'00' + phone}`;
        } else {
            phoneNumber = `tel:${'00' + phone}`;
        }
        Linking.canOpenURL(phoneNumber)
            .then(supported => {
                if (!supported) {
                    Alert.alert('Phone number is not available');
                } else {
                    return Linking.openURL(phoneNumber);
                }
            })
            .catch(err => console.log(err));
    };

    render() {
        return (
            <React.Fragment>
                <FlatList data={this.props.teachers} renderItem={({ item }) => (
                    <View style={styles.item}>
                        <TouchableHighlight onPress={() => {
                            this.callNumber(item.mobileNo)
                        }}>
                            <View style={{
                                justifyContent: 'flex-start',
                                flexDirection: 'row',
                                color: 'black',
                                marginTop: 5
                            }}>
                                <Image style={{ height: 22, marginTop: 1, width: 25, marginRight: 10 }}
                                    source={callIcon} />
                                <Text style={styles.employeeFont}>{item.teachername}</Text>
                            </View>
                        </TouchableHighlight>
                    </View>
                )} />
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    //console.log("Employess phone..",state);
    return {
        locale: state.locale,
        teachers: state.general.teachers,
        staff: state.staff
    }
}

const mapDispatchToProps = dispatch => ({
    getTeachers: (schoolId) => dispatch(getLocalTeachers(schoolId)),

});


export default connect(mapStateToProps, mapDispatchToProps)(EmployeePhones)
const styles = StyleSheet.create({
    row: { justifyContent: 'space-between', flexDirection: 'row', marginTop: 5 },
    noRecords: { justifyContent: 'center', alignItems: 'center', marginTop: 10 },
    employeeFont: { fontSize: 14, fontWeight: 'bold', color: 'black', textAlign: 'right', paddingRight: 5 },
    item: { padding: 10, elevation: 1, margin: 5, backgroundColor: '#fff' },
})

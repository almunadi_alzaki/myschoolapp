import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
//import styles from '../styles/LoginStyles';
import Person from '../imagesicon/home3_08.png';
import Apply from '../imagesicon/home3_09.png';
import Confirm from '../imagesicon/home3_10.png';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import { fetchExecuseValues } from '../actions/staffactions';
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
import i18n, { strings } from "../i18n";
import Icon from "react-native-vector-icons/Ionicons";


class ViewExcuse extends React.Component {

    constructor(props) {
        super(props);
        //console.log("dsdsdsdsd",I18nManager.isRTL);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
    }

    componentDidMount() {

        this.props.navigation.setParams({});
        this.props.fetchExecuseValues(this.props.staff.schoolId);

    }
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "md-arrow-forward" : "md-arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };

    addUser = () => {
        this.setState(prevState => {
            return {
                text: '',
                users: [...prevState.users, prevState.text]
            }
        })
    }

    render() {

        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);

        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.containerCenter}>
                    <View style={{
                        flex: 0,
                        flexDirection: 'row',
                        borderBottomWidth: 1,
                        borderBottomColor: '#f0f0f0',
                    }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 2 }}>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Class')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Division')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Status')}</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                        {this.props.staff.viewexecuses.length > 0 ? this.props.staff.viewexecuses.map((execuse, index) => (
                            <View style={{
                                flex: 0,
                                flexDirection: 'row',
                                borderBottomWidth: 1,
                                borderBottomColor: '#f0f0f0'
                            }}>
                                <View style={{ flex: 3 }}>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30, }} source={Person} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{(execuse.student_name)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30, }} source={Apply} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{(execuse.senter_teachername)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30, }} source={Confirm} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{(execuse.confirm_teachername)}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.boxCenterDiv}>
                                    <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <View style={style.icon}>
                                            <Text style={styles.BorderBox}>{execuse.class_shortcut}</Text>
                                        </View>
                                        <View style={style.icon}>
                                            <Text style={styles.BorderBox}>{execuse.div}</Text>
                                        </View>
                                        <View style={style.icon}>
                                            {execuse.accept_or_reject == 1 ?
                                                <View style={[style.icon, { backgroundColor: 'green' }]}>
                                                    <Icon size={22} color="#fff" name="md-checkmark" />
                                                </View>
                                                :
                                                execuse.accept_or_reject == 2 ?
                                                    <View style={[style.icon, { backgroundColor: 'red' }]}>
                                                        <Icon size={22} style={{ alignSelf: 'center' }} color="#fff"
                                                            name="md-close" />
                                                    </View>
                                                    :
                                                    <View style={[style.icon, { backgroundColor: 'yellow' }]}>
                                                    </View>
                                            }
                                        </View>
                                    </View>
                                </View>
                            </View>

                        ))
                            :
                            <View
                                style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center' }}>
                                <View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
                                    <Text style={{
                                        display: 'flex',
                                        alignItems: 'flex-end'
                                    }}>{strings('No View Execuses')}</Text>
                                </View>
                            </View>

                        }
                    </ScrollView>

                </View>

            </View>
        );
    }
}


const style = StyleSheet.create({
    icon: {
        borderWidth: 1,
        borderColor: 'grey',
        height: 25,
        width: 25,
        marginLeft: 1,
        marginRight: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
const mapDispatchToProps = dispatch => ({
    fetchExecuseValues: (schoolId) => dispatch(fetchExecuseValues(schoolId))

})
const mapStateToProps = (state) => {
    return {
        locale: state.locale,
        staff: state.staff,

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewExcuse)



import React from 'react';
import { Dimensions, Image, Platform, ScrollView, TouchableOpacity, View } from 'react-native';
import I18n from "../i18n";
import { connect } from "react-redux";
import { getAcedemicCalendar } from "../actions/general.action";
import { Icon } from "react-native-elements";
import ImageViewer from 'react-native-image-pan-zoom';

const win = Dimensions.get('window');

class AcademicCalendar extends React.Component {
  state = {};

  static navigationOptions = ({ navigation }) => {
    return Platform.OS === 'ios' && {
      headerLeft: (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ marginRight: 10, marginLeft: 10, }}
            onPress={() => navigation.pop()}
          >
            <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
              name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
          </TouchableOpacity>
        </View>
      )
    };
  };

  componentDidMount() {
    this.props.getCalendar();
  }


  render() {
    return (
      <View style={{ flex: 1, }}>
        <ImageViewer
          cropWidth={win.width}
          cropHeight={win.height}
          imageWidth={win.width}
          imageHeight={win.height} >
          <Image style={{ width: win.width, height: win.height }}
            source={{ uri: this.props.calendar }} />
        </ImageViewer>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return { calendar: state.general.calendar }
}

const mapDispatchToProps = dispatch => ({
  getCalendar: () => dispatch(getAcedemicCalendar()),

});


export default connect(mapStateToProps, mapDispatchToProps)(AcademicCalendar)

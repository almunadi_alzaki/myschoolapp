import React from 'react';
import { Alert, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import styles, { colors } from '../styles/LoginStyles';
import i18n, { strings } from "../i18n";
import { connect } from "react-redux";
import { getTeachers, getTimeTable, getTimeTableByRoom, getTimeTableByTeacher } from "../actions/general.action";
import { fetchFilterClassesValues, fetchFilterDivsValues, getAllDivsLocal, getRoomList } from "../actions/staffactions";
import Picker from 'react-native-picker-select';
import { Button, Icon } from "react-native-elements";

// import Icon from 'react-native-vector-icons/Ionicons'

class FullTimeTable extends React.Component {
    state = {
        schools: [],
        selectedTeacher: '', selectedClass: '', selectedDiv: ''
    }
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };

    componentDidMount() {

    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.staffs.length && nextProps.pageScreen === 'staff') {
            if (prevState.staff) {
                return null;
            }
            const staff = nextProps.staffs[0];
            if (staff.levelid != 17) {
                nextProps.getDivs(staff.schoolId);
                nextProps.getClasses(staff.schoolId);
                nextProps.getTeachers(staff.schoolId);
            } else {
                nextProps.getTeachers(staff.schoolId);
                nextProps.getRoomList(staff.schoolId);
            }
            return { staff }
        }
        return null
    }

    schoolChangeHandler = (value) => {
        const student = this.state.students.find(s => s.id === value);
        if (student) {
            this.props.getTimeTable({ schoolId: student.school_id, class: student.class, div: student.div });
        }
    };

    divChangeHandler = (selectedDiv) => {
        this.setState(s => ({ selectedDiv, selectedTeacher: selectedDiv ? '' : s.selectedTeacher }))
    };
    classChangeHandler = (selectedClass) => {
        this.setState(s => ({ selectedClass, selectedTeacher: selectedClass ? '' : s.selectedTeacher }))
        this.props.getAllDivsLocal(selectedClass);
    };
    roomChangeHandler = (selectedRoom) => {
        this.setState(s => ({ selectedRoom }))
    };
    teacherChangeHandler = (selectedTeacher) => {
        this.setState(s => ({
            selectedTeacher,
            selectedClass: selectedTeacher ? '' : s.selectedClass,
            selectedDiv: selectedTeacher ? '' : s.selectedDiv
        }))
    };
    searchPressHandler = () => {
        const { staff, selectedTeacher, selectedClass, selectedRoom, selectedDiv } = this.state;
        if (staff.levelid == 17) {
            if (selectedTeacher) {
                this.props.getTimeTableByRoom({
                    schoolId: staff.schoolId,
                    // roomno: selectedRoom,
                    teacher_id: selectedTeacher
                });
            }
            return;
        }
        if (selectedTeacher) {
            this.props.getTimeTableByTeacher({ schoolId: staff.schoolId, teacherId: selectedTeacher });
        } else {
            if (!selectedClass || !selectedDiv) {
                Alert.alert('', strings('selectClassDiv'))
            }
            this.props.getTimeTable({ schoolId: staff.schoolId, class: selectedClass, div: selectedDiv });
        }
    }

    render() {
        const { timeTable, staffs, divs, rooms, classes, teachers } = this.props;
        const { staff, selectedTeacher, selectedClass, selectedRoom, selectedDiv } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {staff && staff.levelid != 17 && <View style={{ flexDirection: 'row', margin: 10 }}>
                    {classes
                        && <Picker onValueChange={this.classChangeHandler}
                            value={selectedClass}
                            useNativeAndroidPickerStyle={false}
                            placeholder={{ label: strings('Class'), value: '' }}
                            style={pickerStyle}
                            Icon={() => <Icon containerStyle={{ left: -1, top: 12 }}
                                name={'arrow-drop-down'} />}
                            items={classes.map((p, i) => ({ label: p.class_shortcut, value: p.classId }))} />}
                    {divs
                        && <Picker onValueChange={this.divChangeHandler}
                            useNativeAndroidPickerStyle={false}
                            value={selectedDiv}
                            placeholder={{ label: strings('Division'), value: '' }}
                            style={pickerStyle}
                            Icon={() => <Icon containerStyle={{ left: -1, top: 12 }} name={'arrow-drop-down'} />}
                            items={divs.map((p, i) => ({ label: p.div, value: p.div }))} />}
                    {teachers
                        && <Picker onValueChange={this.teacherChangeHandler}
                            value={selectedTeacher}
                            useNativeAndroidPickerStyle={false}
                            Icon={() => <Icon containerStyle={{ left: -1, top: 12 }} name={'arrow-drop-down'} />}
                            placeholder={{ label: strings('Teacher'), value: '' }}
                            style={pickerStyle}
                            items={teachers.map((p, i) => ({ label: p.teachername, value: p.teacherId }))} />}
                    <Button title={'Ok'} containerStyle={{ margin: 5 }} onPress={this.searchPressHandler} />

                </View>}
                {staff && staff.levelid == 17 && <View style={{ flexDirection: 'row', margin: 10 }}>
                    {/*{rooms*/}
                    {/*&& <Picker onValueChange={this.roomChangeHandler}*/}
                    {/*           value={selectedRoom}*/}
                    {/*           useNativeAndroidPickerStyle={false}*/}
                    {/*           placeholder={{label: strings('room'), value: ''}}*/}
                    {/*           style={pickerStyle}*/}
                    {/*           Icon={() => <Icon containerStyle={{left: -1, top: 12}} name={'arrow-drop-down'}/>}*/}
                    {/*           items={rooms.map((p, i) => ({label: p.room_name, value: p.room_shortcut}))}/>}*/}
                    {teachers
                        && <Picker onValueChange={this.teacherChangeHandler}
                            value={selectedTeacher}
                            useNativeAndroidPickerStyle={false}
                            Icon={() => <Icon containerStyle={{ left: -1, top: 12 }} name={'arrow-drop-down'} />}
                            placeholder={{ label: strings('Teacher'), value: '' }}
                            style={pickerStyle}
                            items={teachers.map((p, i) => ({ label: p.teachername, value: p.teacherId }))} />}
                    <Button title={'Ok'} containerStyle={{ margin: 5 }} onPress={this.searchPressHandler} />

                </View>}

                <View style={{ flex: 0, flexDirection: 'row', borderWidth: 1, borderBottomColor: 'black', }}>
                    <View style={styles.tableGrid10}>
                        <View style={styles.boxCenterDiv}>
                            <Text style={{ color: 'black', fontSize: 12 }}></Text>
                        </View>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('sun')}</Text>

                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('mon')}</Text>

                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('tue')}</Text>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('wed')}</Text>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('thu')}</Text>
                    </View>
                </View>
                <ScrollView style={{ flex: 1 }}>
                    {timeTable && timeTable.map((t, index) => (
                        <View key={index}
                            style={{ flex: 0, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                            <View style={styles.tableGrid10}>
                                <View style={styles.boxCenterDiv}>
                                    <Text style={{ color: 'black', fontSize: 12 }}>{index + 1}</Text>
                                </View>
                            </View>
                            {t.map((d, i) => (
                                <View key={i} style={i % 2 == 0 ? styles.tableGrid18Yellow : styles.tableGrid18Greys}>
                                    {staff.levelid != 17 ?
                                        <React.Fragment>
                                            <Text style={styles.tableText}>{d.classdiv || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sub_shortcut || '-'}</Text>
                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            <Text style={styles.tableText}>{d.roomno || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sub_shortcut || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sec_arabic || '-'}</Text>
                                        </React.Fragment>
                                    }

                                </View>
                            ))}
                        </View>
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    timeTable: state.general.timeTable,
    pageScreen: state.general.pageScreen,
    teachers: state.general.teachers,
    students: state.student.students,
    staffs: state.staff.staffs,
    divs: state.staff.divs,
    classes: state.staff.classes,
    rooms: state.staff.rooms,
});

const mapDispatchToProps = dispatch => ({
    getTimeTable: (data) => dispatch(getTimeTable(data)),
    getTimeTableByTeacher: (data) => dispatch(getTimeTableByTeacher(data)),
    getTimeTableByRoom: (data) => dispatch(getTimeTableByRoom(data)),
    getTeachers: (schoolId) => dispatch(getTeachers(schoolId)),
    getClasses: (schoolId) => dispatch(fetchFilterClassesValues(schoolId)),
    getAllDivsLocal: (classId) => dispatch(getAllDivsLocal(classId)),
    getRoomList: (schoolId) => dispatch(getRoomList(schoolId)),
    getDivs: (schoolId) => dispatch(fetchFilterDivsValues(schoolId))
});
const pickerStyle = StyleSheet.create({
    inputIOS: { color: colors.primary, textAlign: 'center', margin: 5, padding: 10, borderWidth: 0.5, width: 80 },
    inputAndroid: {
        color: colors.primary,
        textAlign: 'center',
        marginLeft: 15,
        margin: 5,
        padding: 5,
        borderWidth: 0.5,
        width: 80
    },
}
)
export default connect(mapStateToProps, mapDispatchToProps)(FullTimeTable)

import React from 'react';
import {Image, Platform, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
//import styles from '../styles/LoginStyles';
import Delete from '../imagesicon/deleteMenuIcon.png';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import {connect} from 'react-redux';
import {deleteStudent, getStudentLocal} from '../actions/actions';
import {Icon} from "react-native-elements";
import i18n from "../i18n";

class DeleteStudent extends React.Component {

    constructor(props) {

        super(props);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = {isRTL: isRTL};


    }

    componentDidMount() {

        // this.props.getStudentLocal()
    }

    static navigationOptions = ({navigation}) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{flexDirection: 'row'}}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{marginRight: 10, marginLeft: 10,}}
                        onPress={() => navigation.navigate('Login')}
                    >
                        <Icon size={24} style={{alignSelf: 'center'}} color="#fff"
                              name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
                    </TouchableOpacity>
                </View>
            )
        };
    };


    deleteStudent = (student) => {
        this.props.deleteStudent(student,
            this.props.student.students,
            this.props.locale.deviceToken)
    }

    render() {
        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
        const {navigate} = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.containerCenter}>

                    {this.props.student.students.map((student, key) => {
                        if (student != null) {
                            return <View key={key} style={{
                                flex: 0, flexDirection: 'row',
                                padding: 12, borderBottomWidth: 1, borderBottomColor: '#f0f0f0',
                                shadowColor: "#000000",
                                shadowOpacity: 0.1,
                                shadowRadius: 2,
                                shadowOffset: {
                                    height: 1,
                                    width: 1
                                }
                            }}>
                                <View style={styles.ColSet80}>
                                    <View style={styles.textAlignDiv}>
                                        <Text style={{fontSize: 18}}>{student.st_name}</Text>
                                    </View>
                                </View>
                                <View style={styles.ColSet20}>
                                    <View style={styles.FlexCenter}>
                                        <TouchableHighlight onPress={() => this.deleteStudent(student)}>
                                            <Image style={{height: 30, width: 30,}} source={Delete}/>
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        }
                    })}


                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        locale: state.locale,
        isRTL: state.isRTL,
        student: state.student

    }
}

const mapDispatchToProps = dispatch => ({
    deleteStudent: (student, students, token) => dispatch(deleteStudent(student, students, token)),
    setLocale: (locale) => dispatch(setLocale(locale)),
    getStudentLocal: () => dispatch(getStudentLocal())
})


export default connect(mapStateToProps, mapDispatchToProps)(DeleteStudent)

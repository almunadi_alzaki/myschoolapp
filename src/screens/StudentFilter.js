import React from 'react';
import { Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import LeftArrow from '../imagesicon/left-arrow-line.png';
import RightArrow from '../imagesicon/right-arrow-line.png';
import I18n from '../i18n.js';
import { connect } from 'react-redux';
import Picker from 'react-native-picker-select';
import {
	getAllAcadamicStudentsLocal,
	getAllClassesLocal,
	getAllDivsLocal,
	getAllStudentsLocal
} from '../actions/staffactions';
import CheckBox from '../components/Checkbox'
import { parseArabic, trim } from "../utils";
import { Icon } from "react-native-elements";
import { colors } from "../styles/LoginStyles";

import { sentryMessage } from '../utils';

class StudentFilter extends React.Component {
	constructor(props) {
		super(props);

		let isRTL = this.props.locale.locale == "ar" ? true : false;
		this.state = {
			isRTL: isRTL,
			checked: true,
			classId: 'all',
			div: 'all',
			checkSelected: [],
			searchclass: '',
			searchdiv: '',
			searchname: '',
			searchAcadamicNo: '',
			counter: 0,
			divs: []
		};
	}

	componentDidMount() {

		const menu = this.props.navigation.state.params.menu;
		this.props.navigation.setParams({ title: this.props.locale.isRTL ? menu.menu_name_ar : menu.menu_name_en });
		if (this.props.staff.classes.length >= 0) {
			// this.props.fetchFilterStudentsValues(this.props.staff.staffs.find(p => p.schoolId === staff.shoolId));
			const staff = this.props.staff.staffs.find(p => p.schoolId === this.props.staff.schoolId);
			// this.props.fetchFilterStudentsValues(this.props.staff.schoolId);
			if (staff.levelid != 17) {
				//   this.props.fetchFilterClassesValues(this.props.staff.schoolId);
				this.props.getAllClassesLocal();
				//   this.props.fetchFilterDivsValues(this.props.staff.schoolId);
			}
		}
		this.props.getAllStudentsLocal(this.state.searchclass, this.state.searchdiv, this.state.searchname);
	}

	static navigationOptions = ({ navigation }) => {
		return {
			headerTitle: navigation.getParam('title'),

		};
	};

	toggleCheckBox = (id, isCheck) => {
		let { checkSelected } = this.state;
		if (!checkSelected.includes(id)) {
			checkSelected.push(id);
			this.setState({ counter: this.state.counter + 1 });
		} else { // remove element
			var index = checkSelected.indexOf(id);
			if (index > -1) {
				checkSelected.splice(index, 1);
			}
			this.setState({ counter: this.state.counter - 1 });
		}
		this.setState({ checkSelected });
	}

	pickerChange(value, index) {
		if (value)
			this.setState({
				currentLabel: value.class_ar,
				classId: value,
				div: '', searchdiv: '',
			})
		// const div = this.props.staff.divs.filter(a => a.classId === value);
		this.setState({ searchclass: value, })
		this.props.getAllDivsLocal(value);
		this.props.getAllStudentsLocal(value, this.state.searchdiv, this.state.searchname);
	}

	pickerDivChange(div, index) {
		if (div)
			this.setState({
				currentLabel: div,
				div: div,
				searchdiv: div
			})
		this.props.getAllStudentsLocal(this.state.searchclass, div, this.state.searchname);
	}

	filterbyName(studentname) {
		//console.log("By Name....",studentname);

		this.setState({ searchname: studentname })
		if (this.props.staff.staffs[0].levelid != 17) {
			this.props.getAllStudentsLocal(this.state.searchclass, this.state.searchdiv, studentname);
		} else {
			this.props.getAllAcadamicStudentsLocal(this.state.searchAcadamicNo, studentname);
		}

	}

	filterbyAcadamicNo(acadamicNo) {
		//console.log("By Name....",studentname);
		this.setState({ searchAcadamicNo: acadamicNo })
		this.props.getAllAcadamicStudentsLocal(parseArabic(acadamicNo), this.state.searchname);

	}

	getNormalSchoolSearchfield() {
		return (
			<View style={{
				flexDirection: 'row',
				borderColor: 'gray',
				justifyContent: 'space-around',
				marginTop: 10
			}}>
				<View style={{ borderBottomWidth: 1, height: 38, width: '40%' }}>
					<Picker
						onValueChange={(itemValue, itemIndex) => this.pickerChange(itemValue, itemIndex)}
						value={this.state.classId}
						placeholder={{
							label: 'الجميع',
							value: 'all'
						}}
						key={this.props.staff.classes.length}
						useNativeAndroidPickerStyle={false}
						Icon={() => <Icon containerStyle={{ left: -1, top: 5 }} name={'arrow-drop-down'} />}
						style={pickerStyle}
						items={this.props.staff.classes.map((p, i) => ({
							label: p.class_ar,
							value: p.classId
						}))}
					/>
				</View>
				<View style={{ borderBottomWidth: 1, height: 38, width: '40%' }}>
					<Picker onValueChange={(itemValue, itemIndex) => this.pickerDivChange(itemValue, itemIndex)}
						value={this.state.div}
						key={this.props.staff.divs.length}
						useNativeAndroidPickerStyle={false}
						Icon={() => <Icon containerStyle={{ left: -1, top: 5 }} name={'arrow-drop-down'} />}
						placeholder={{
							label: 'الجميع',
							value: 'all'
						}}
						style={pickerStyle}
						items={(this.props.staff.divs.map((p, i) => ({
							label: p.div,
							value: p.div
						})))} />
				</View>
			</View>
		);
	}

	getUniversitySchoolSearchfield() {
		return (
			<View style={{
				flex: 0,
				flexDirection: 'row',
				borderColor: 'gray',
				justifyContent: 'center'
			}}>
				<View style={{ borderBottomWidth: 1, height: 38, width: 250 }}>
					<TextInput style={{ height: 40, paddingTop: 10, textAlign: "right" }}
						onChangeText={(text) => this.filterbyAcadamicNo(text)}
						placeholder={I18n.t('Academic Number')} secureTextEntry={false} />
				</View>
			</View>
		);
	}

	render() {
		const { navigate } = this.props.navigation;
		menuId = this.props.navigation.state.params.menu.menu_id;
		const staff = this.props.staff.staffs.find(p => p.schoolId === this.props.staff.schoolId);
		styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
		sentryMessage('divs', { div: this.props.staff.divs.map(a => a.div) });
		return (
			<View style={styles.container}>

				<View style={styles.containerCenter}>

					<View style={styles.filterHeader}>
						<TouchableOpacity onPress={() => this.props.navigation.goBack(null)}
							style={styles.filterHeaderTop1}>
							<View>
								<Image style={styles.filtericonSize}
									source={!this.props.locale.isRTL ? LeftArrow : RightArrow} />
							</View>
							<View>
								<Text style={styles.filterHeadText}
									onPress={() => this.props.navigation.goBack(null)}>{I18n.t('Back')}</Text>
							</View>
						</TouchableOpacity>
						<View style={styles.filterHeaderTop2}>
							<Text style={styles.filterHeadText}>{I18n.t('Count')}:<Text
								id="selcount">{this.state.counter}</Text></Text>

						</View>
						<TouchableOpacity onPress={this.onNext} style={styles.filterHeaderTop3}>
							<View>
								<Text style={styles.filterHeadText}>{I18n.t('Next')}</Text>
							</View>
							<View>
								<Image style={styles.filtericonSize}
									source={!this.props.locale.isRTL ? RightArrow : LeftArrow} />
							</View>
						</TouchableOpacity>
					</View>

					{this.props.staff.staffs.length && staff.levelid != 17 ? this.getNormalSchoolSearchfield() : this.getUniversitySchoolSearchfield()}
					<View style={{
						flex: 0,
						flexDirection: 'row',
						borderColor: 'gray',
						justifyContent: 'center',
						marginBottom: 10
					}}>
						<View style={{ borderBottomWidth: 1, height: 40, width: 250 }}>
							<TextInput style={{ height: 40, paddingTop: 10, }}
								onChangeText={(text) => this.filterbyName(text)}
								placeholder={I18n.t('Student Name')} secureTextEntry={false} />
						</View>
					</View>

					<ScrollView>
						{this.props.staff.students.length > 0 ? this.props.staff.students.map((student1, index) => (
							<TouchableOpacity onPress={() => this.toggleCheckBox(student1.id, null)} style={{
								flex: 0,
								justifyContent: 'space-between',
								flexDirection: 'row',
								borderColor: 'gray',
								width: '100%',
								marginLeft: 10
							}}>
								<View style={styles.ColSet10}>
									<CheckBox style={{
										marginTop: 5,
										marginRight: 10,
										alignItems: 'flex-start'
									}} key={student1.id} value={this.state.checkSelected.includes(student1.id)}
										clicked={(id, isCheck) => this.toggleCheckBox(student1.id, isCheck)} />
								</View>
								<View style={styles.ColSet90}>
									<Text style={{
										display: 'flex',
										marginTop: 5,
										marginRight: 5,
										color: 'black',
										fontSize: 14,
										fontWeight: 'bold',
										textAlign: 'left',
										alignItems: 'flex-start'
									}}>{trim(student1.st_name, 30)}</Text>

								</View>
							</TouchableOpacity>
						))
							:
							<View style={{
								flex: 0,
								flexDirection: 'row',
								borderColor: 'gray',
								justifyContent: 'center'
							}}>
								<View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
									<Text style={{ display: 'flex', alignItems: 'flex-start' }}>No Students</Text>
								</View>
							</View>

						}
					</ScrollView>
				</View>

			</View>
		);
	}

	onNext = () => {
		if (this.state.checkSelected.length)
			this.props.navigation.navigate('SendBehaviour', {
				menu: this.props.navigation.state.params.menu,
				sclass: this.state.searchclass,
				behaviuorMenu: menuId,
				sdiv: this.state.searchdiv,
				selectedstudent: this.state.checkSelected
			})
	}
}

const mapDispatchToProps = dispatch => ({
	// fetchFilterClassesValues: (schoolId) => dispatch(fetchFilterClassesValues(schoolId)),
	// fetchFilterDivsValues: (schoolId) => dispatch(fetchFilterDivsValues(schoolId)),
	getAllClassesLocal: () => dispatch(getAllClassesLocal()),
	// fetchFilterStudentsValues: (schoolId, classId) => dispatch(fetchFilterStudentsValues(schoolId)),
	getAllDivsLocal: (classId) => dispatch(getAllDivsLocal(classId)),
	getAllStudentsLocal: (searchclass, searchdiv, searchname, searchAcadamicNo, levelid) => dispatch(getAllStudentsLocal(searchclass, searchdiv, searchname, searchAcadamicNo, levelid)),
	getAllAcadamicStudentsLocal: (searchAcadamicNo, searchname) => dispatch(getAllAcadamicStudentsLocal(searchAcadamicNo, searchname))
})
const mapStateToProps = (state) => {

	return {
		locale: state.locale,
		staff: state.staff,

	}
}

const pickerStyle = StyleSheet.create({
	inputIOS: { color: colors.primary, textAlign: 'center', margin: 5, padding: 10, },
	inputAndroid: {
		color: colors.primary,
		textAlign: 'center',
		marginLeft: 15,
		padding: 5,
	},
}
)
export default connect(mapStateToProps, mapDispatchToProps)(StudentFilter)

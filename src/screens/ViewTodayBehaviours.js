import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Person from '../imagesicon/home3_08.png';
import confirmMsgIcon from '../imagesicon/msg_icon.png';
import confirmStaffIcon from '../imagesicon/confirmStaff.png';
import execuseStatusIcon from '../imagesicon/execuse-status.png';
import I18n from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import { fetchTodayBehaviourValues } from '../actions/staffactions';
import { connect } from 'react-redux';
import Icon from "react-native-vector-icons/Ionicons";
import i18n from "../i18n";


class ViewTodayBehaviours extends React.Component {

    constructor(props) {
        super(props);
        //console.log("dsdsdsdsd",I18nManager.isRTL);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
        // I18n.locale = this.state.locale;
    }

    componentDidMount() {

        this.props.navigation.setParams({});
        this.props.fetchTodayBehaviourValues(this.props.staff.schoolId);
    }
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "md-arrow-forward" : "md-arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };

    addUser = () => {
        this.setState(prevState => {
            return {
                text: '',
                users: [...prevState.users, prevState.text]
            }
        })
    }

    render() {
        if (this.state.isRTL) {
            styles = StyleSheetFactory.getSheet(this.state.isRTL);
        }
        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);

        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <View style={styles.containerCenter}>
                    <View style={{
                        flex: 0,
                        flexDirection: 'row',
                        borderBottomWidth: 1,
                        borderBottomColor: '#f0f0f0',
                    }}>

                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{I18n.t('Class')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{I18n.t('Division')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{I18n.t('Status')}</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                        {this.props.staff.viewbehaviours.length > 0 ? this.props.staff.viewbehaviours.map((behaviour, index) => (
                            <View style={{
                                flex: 0,
                                flexDirection: 'row',
                                borderBottomWidth: 1,
                                borderBottomColor: '#f0f0f0'
                            }}>
                                <View style={{ flex: 3 }}>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={[styles.ColSet10, { marginRight: 10 }]}>
                                            <Image style={{ height: 30, width: 30, }} source={Person} />
                                        </View>
                                        <View style={styles.ColSet90}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{behaviour.student_name}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={[styles.ColSet10, { marginRight: 10 }]}>
                                            <Image style={{ height: 30, width: 30, }} source={execuseStatusIcon} />
                                        </View>
                                        <View style={styles.ColSet90}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{behaviour.functionality_ar}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={[styles.ColSet10, { marginRight: 10 }]}>
                                            <Image style={{ height: 30, width: 30, }} source={confirmStaffIcon} />
                                        </View>
                                        <View style={styles.ColSet90}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{(behaviour.senter_teachername)}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', padding: 2, alignItems: 'center' }}>
                                        <View style={[styles.ColSet10, { marginRight: 10 }]}>
                                            <Image style={{ height: 30, width: 30, }} source={confirmMsgIcon} />
                                        </View>
                                        <View style={styles.ColSet90}>
                                            <Text numberOfLines={1}
                                                style={[styles.fontSizeText, { marginLeft: 10 }]}>{(behaviour.topic)}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.boxCenterDiv}>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                        <View style={style.icon}>
                                            <Text style={styles.BorderBox}>{behaviour.class_shortcut}</Text>
                                        </View>
                                        <View style={style.icon}>
                                            <Text style={styles.BorderBox}>{behaviour.div}</Text>
                                        </View>
                                        <View style={style.icon}>
                                            {behaviour.accept_or_reject == 1 ?
                                                <View style={[style.icon, { backgroundColor: 'green' }]}>
                                                    <Icon size={22} color="#fff" name="md-checkmark" />
                                                </View>
                                                :
                                                behaviour.accept_or_reject == 2 ?
                                                    <Icon size={22} style={{ alignSelf: 'center' }} color="#fff"
                                                        name="md-close" />
                                                    :
                                                    <View style={[style.icon, { backgroundColor: 'yellow' }]}>
                                                    </View>
                                            }
                                        </View>
                                    </View>
                                </View>
                            </View>
                        ))
                            :
                            <View
                                style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center' }}>
                                <View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
                                    <Text style={{
                                        display: 'flex',
                                        alignItems: 'flex-end'
                                    }}>{I18n.t('No Today Behaviours')}</Text>
                                </View>
                            </View>

                        }
                    </ScrollView>
                </View>

            </View>
        );
    }
}


const style = StyleSheet.create({
    icon: {
        borderWidth: 1,
        borderColor: 'grey',
        height: 25,
        width: 25,
        marginLeft: 1,
        marginRight: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
const mapDispatchToProps = dispatch => ({
    fetchTodayBehaviourValues: (schoolId) => dispatch(fetchTodayBehaviourValues(schoolId))

})
const mapStateToProps = (state) => {
    return {
        locale: state.locale,
        staff: state.staff,

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewTodayBehaviours)



import React from 'react';
import { Platform, StyleSheet, TextInput, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import { fetchFilterSubjectsValues, getAllSubjectsLocal, sendBehaviour } from '../actions/staffactions';
import { connect } from 'react-redux';
import I18n, { strings } from "../i18n";
import Button from "../components/Button";
import { Icon } from "react-native-elements";
import Picker from 'react-native-picker-select';
import { colors } from "../styles/LoginStyles";


class SendBehaviour extends React.Component {
  constructor(props) {
    super(props);
    //console.log("dsdsdsdsd",I18nManager.isRTL);
    let isRTL = this.props.locale.locale == "ar" ? true : false;
    this.state = {
      isRTL: isRTL,
      checked: true,
      subId: '',
      period: '',
      permission: 1,
    };
  }

  componentDidMount() {

    this.props.getAllSubjectsLocal();
    const menu = this.props.navigation.state.params.menu;
    this.props.navigation.setParams({ title: this.props.locale.isRTL ? menu.menu_name_ar : menu.menu_name_en });
    // this.props.fetchFilterSubjectsValues(this.props.staff.schoolId);

  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: navigation.getParam('title'),
      headerLeft: Platform.OS === 'ios' ? (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ marginRight: 10, marginLeft: 10, }}
            onPress={() => navigation.pop()}
          >
            <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
              name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
          </TouchableOpacity>
        </View>
      ) : undefined
    };
  };

  addUser = () => {
    this.setState(prevState => {
      return {
        text: '',
        users: [...prevState.users, prevState.text]
      }
    })
  }

  pickerSubChange(value, index) {
    const sub = this.props.staff.subjects.find((v, i) => v.subId === value);
    if (sub)
      this.setState({
        currentLabel: sub.sub_name_ar,
        subId: sub.subId,
      })
  }

  sendBehaviour(behaviourMenuId, selstudents) {
    if (this.state.sent) return;
    this.setState({ sent: true });
    period = this.state.period;
    subId = this.state.subId;
    if (subId == undefined) subId = 0;
    message = this.state.messagetext;
    const teacherId = this.props.staff.staffs.find(a => a.schoolId === this.props.staff.schoolId).teacherId;
    var parmsArray = [this.props.staff.schoolId, behaviourMenuId, selstudents, period, subId, message, teacherId];
    if (selstudents.length <= 0) {
      alert(strings("chooseStudents"));
      return false;
    } else if (message == '') {
      alert(strings("fillDescription"));
      return false;
    } else {
      this.props.sendBehaviour(parmsArray, this.props.navigation);
    }
  }

  render() {
    behaviuorMenu = this.props.navigation.state.params.behaviuorMenu;
    selstudents = this.props.navigation.state.params.selectedstudent;
    const count = 1;
    const { navigate } = this.props.navigation;
    styles = StyleSheetFactory.getSheet(this.state.isRTL);
    return (
      <View style={styles.container}>

        <View style={styles.containerCenter}>
          <View style={{
            flex: 0,
            flexDirection: 'row',
            borderColor: 'gray',
            justifyContent: 'center'
          }}>
            <View style={{ borderBottomWidth: 1, height: 45, width: 250 }}>
              <Picker onValueChange={(itemValue, itemIndex) => this.pickerSubChange(itemValue, itemIndex)}
                value={this.state.subId}
                useNativeAndroidPickerStyle={false}
                Icon={() => <Icon containerStyle={{ left: -1, top: 12 }} name={'arrow-drop-down'} />}
                style={pickerStyle}
                items={[{
                  label: I18n.t('subject'),
                  value: ''
                }].concat(this.props.staff.subjects.map((p, i) => ({
                  label: p.sub_name_ar,
                  value: p.subId
                })))} />
            </View>
          </View>

          <View style={{
            flex: 0,
            flexDirection: 'row',
            borderColor: 'gray',
            justifyContent: 'center',
          }}>
            <View style={{ borderBottomWidth: 1, height: 45, width: 250 }}>
              <Picker onValueChange={(itemValue, itemIndex) => this.setState({ period: itemValue })}
                value={this.state.period}
                useNativeAndroidPickerStyle={false}
                Icon={() => <Icon containerStyle={{ left: -1, top: 12 }} name={'arrow-drop-down'} />}
                style={pickerStyle}
                items={[{ label: I18n.t('period'), value: '' },
                { label: '1', value: '1' },
                { label: '2', value: '2' },
                { label: '3', value: '3' },
                { label: '4', value: '4' },
                { label: '5', value: '5' },
                { label: '6', value: '6' },
                { label: '7', value: '7' },
                { label: '8', value: '8' },
                { label: '9', value: '9' }]} />
            </View>
          </View>

          <View style={{
            flex: 0,
            flexDirection: 'row',
            borderColor: 'gray',
            justifyContent: 'center'
          }}>
            <View style={{ borderWidth: 1, width: 250 }}>
              <TextInput style={{ paddingTop: 10, borderTopWidth: 0 }}
                multiline={true}
                maxLength={20}
                numberOfLines={4}
                onChangeText={(messagetext) => this.setState({ messagetext })}
                placeholder={I18n.t('BehaviourMessage')} />
            </View>
          </View>

          <View style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center', marginTop: 5 }}>
            <View>
              <Button color="#5f021f" loading={this.props.staff.isLoading}
                title={this.props.staff.isLoading ? I18n.t('Loading Text') : I18n.t('Send')}
                onPress={() => this.sendBehaviour(behaviuorMenu, selstudents)} />
            </View>
          </View>
        </View>

      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  // fetchFilterSubjectsValues: (schoolId) => dispatch(fetchFilterSubjectsValues(schoolId)),
  getAllSubjectsLocal: () => dispatch(getAllSubjectsLocal()),
  sendBehaviour: (paramsArray, n) => dispatch(sendBehaviour(paramsArray, n)),
})
const mapStateToProps = (state) => {
  return {
    locale: state.locale,
    staff: state.staff,
  }
}

const pickerStyle = StyleSheet.create({
  inputIOS: { color: colors.primary, textAlign: 'center', margin: 5, padding: 10, fontWeight: 'bold' },
  inputAndroid: {
    color: colors.primary,
    textAlign: 'center',
    marginLeft: 15,
    margin: 5,
    padding: 5,
    fontWeight: 'bold'
  },
}
)
export default connect(mapStateToProps, mapDispatchToProps)(SendBehaviour)


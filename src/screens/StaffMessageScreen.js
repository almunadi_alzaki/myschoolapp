import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from 'react-native';
//import { messageDelete } from '../actions/messageActions';
//import { messageDeleteByArray } from '../actions/messageActions';
import { staffMessageDeleteByArray } from '../actions/messageActions';
import styles from '../styles/LoginStyles';
import MailGeneralRead from '../imagesicon/message_general_read.png';
import MailGeneralUnRead from '../imagesicon/message_general.png';
import MailPrivateRead from '../imagesicon/message_read_private.png';
import MailPrivateUnRead from '../imagesicon/message_private.png';

import I18n from '../i18n.js';
import MessageDeleteIcon from '../imagesicon/deleteIcon.png';
import { connect } from 'react-redux';
import realm from '../realm/schema';
import CheckBox from '../components/Checkbox'
import { Icon } from "react-native-elements";
import i18n from "../i18n";


//import { CheckBox } from 'react-native-elements'
class StaffMessageScreen extends React.Component {
    constructor(props) {
        super(props);
        //console.log("dsdsdsdsd",I18nManager.isRTL);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = {
            isRTL: isRTL,
            checkSelected: [],
            //checkeditems: false,
            messages: []
        };

        //I18n.locale = this.state.locale;
    }

    componentDidMount() {
        this.props.navigation.setParams({});
        let params = this.props.navigation.state.params
        // params.studentId = '22891'
        // messages = realm.objects('StaffMessages').filtered(`teacherId = "${params.teacherId}"`)
        // messages = realm.objects('Message').filtered(`studentId = "22891"`)
        const messages = realm.objects('StaffMessages').sorted('date', true);
        const updateUI = () => {
            const messages = realm.objects('StaffMessages').sorted('date', true);
            this.setState({
                isRTL: this.props.locale.isRTL,
                messages: messages.map(p => ({ ...p })),
            })
        }
        realm.addListener('change', updateUI);
        this.setState({
            isRTL: this.props.locale.isRTL,
            messages: messages.map(p => ({ ...p })),
        })
    }
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };



    deleteStaffMessage = () => {
        this.props.staffMessageDeleteByArray(this.state.checkSelected)
        this.setState({ checkSelected: [] });
    }
    toggleCheckBox = (id, isCheck) => {
        let { checkSelected } = this.state;
        console.log("is check...", isCheck);
        if (isCheck) {
            checkSelected.push(id);
        } else { // remove element
            var index = checkSelected.indexOf(id);
            if (index > -1) {
                checkSelected.splice(index, 1);
            }
        }
        this.setState({ checkSelected });

        //  alert(this.state.checkSelected); // logging
    }


    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.containerCenter}>
                    <View style={{
                        flex: 0,
                        margin: 8,
                        marginRight: 15,
                        alignSelf: this.state.isRTL ? 'flex-start' : 'flex-end'
                    }}>

                        <TouchableHighlight onPress={() => this.deleteStaffMessage()}>
                            <Image style={{ height: 30, width: 30, }} source={MessageDeleteIcon} />
                        </TouchableHighlight>


                    </View>


                    <ScrollView>
                        {this.state.messages.length > 0 ? this.state.messages.map((message, key) => {

                            // return message[0]
                            return <View key={message.id}>
                                <TouchableWithoutFeedback
                                    onPress={() => navigate('StaffMessageDetails', { message: message })}>
                                    <View style={{
                                        flex: 0,
                                        flexDirection: this.state.isRTL ? 'row-reverse' : 'row',
                                        backgroundColor: '#f9f9f9',
                                        padding: 6,
                                        borderBottomWidth: 1,
                                        borderBottomColor: '#f0f0f0',
                                        justifyContent: 'space-between'
                                    }}>


                                        <View style={styles.ColSet20}>
                                            {message.messageType == "private" ?
                                                message.read ?
                                                    <Image style={{ height: 30, width: 40, }} source={MailPrivateRead} />
                                                    :
                                                    <Image style={{ height: 30, width: 40, }} source={MailPrivateUnRead} />
                                                :
                                                message.read ?
                                                    <Image style={{ height: 30, width: 40, }} source={MailGeneralRead} />
                                                    :
                                                    <Image style={{ height: 30, width: 40, }} source={MailGeneralUnRead} />
                                            }
                                        </View>

                                        <View style={styles.ColSet70}>
                                            <Text style={{
                                                color: 'black',
                                                paddingLeft: 10,
                                                textAlign: this.state.isRTL ? 'right' : 'left'
                                            }}>{message.schoolName}</Text>
                                            <Text style={{
                                                color: 'black',
                                                paddingLeft: 10,
                                                textAlign: this.state.isRTL ? 'right' : 'left'
                                            }}>{message.message}
                                                <Text style={{
                                                    color: 'grey',
                                                    fontSize: 10,
                                                    textAlign: this.state.isRTL ? 'right' : 'left'
                                                }}>   {message.date}</Text></Text>
                                        </View>
                                        <View style={styles.ColSet10}>
                                            <CheckBox size={30} style={{}} key={message.id} value={message.id}
                                                clicked={(id, isCheck) => this.toggleCheckBox(message, isCheck)} />

                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        })
                            :
                            <View
                                style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center' }}>
                                <View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
                                    <Text style={{
                                        display: 'flex',
                                        alignItems: 'flex-end'
                                    }}>{I18n.t('No message found')}</Text>
                                </View>
                            </View>

                        }
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        locale: state.locale,
        student: state.student
    }
}

const mapDispatchToProps = dispatch => ({
    staffMessageDeleteByArray: (str) => dispatch(staffMessageDeleteByArray(str))
})

export default connect(mapStateToProps, mapDispatchToProps)(StaffMessageScreen)

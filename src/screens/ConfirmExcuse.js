import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
//import styles from '../styles/LoginStyles';
import Person from '../imagesicon/home3_08.png';
import confirmStaffIcon from '../imagesicon/confirmStaff.png';
import confirmMsgIcon from '../imagesicon/msg_icon.png';
import { strings } from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import { changeExecuseStatus, fetchExecuseValues } from '../actions/staffactions';
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons'
import i18n from "../i18n";

class ConfirmExcuse extends React.Component {

    constructor(props) {
        super(props);
        //console.log("dsdsdsdsd",I18nManager.isRTL);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = { isRTL: isRTL };
        // I18n.locale = this.state.locale;
    }

    componentDidMount() {

        this.props.navigation.setParams({});
        this.props.fetchExecuseValues(this.props.staff.schoolId);
    }

    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={i18n.locale === 'ar' ? "md-arrow-forward" : "md-arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };

    addUser = () => {
        this.setState(prevState => {
            return {
                text: '',
                users: [...prevState.users, prevState.text]
            }
        })
    }

    changeExecuseStatus(behaviourId, statusval) {
        const { staff } = this.props;
        const selectedStaff = staff.staffs.find(a => a.schoolId === staff.schoolId);
        this.props.changeExecuseStatus(selectedStaff.schoolId, selectedStaff.teacherId, behaviourId, statusval);
    }

    render() {

        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>

                <View style={styles.containerCenter}>
                    <View style={{
                        flex: 0,
                        flexDirection: 'row',
                        borderBottomWidth: 1,
                        borderBottomColor: '#f0f0f0',
                    }}>
                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 0 }}>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Accept')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Class')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Division')}</Text>
                            </View>
                            <View style={styles.textView}>
                                <Text
                                    style={!this.props.locale.isRTL ? styles.textRotat : styles.textRotatAr}>{strings('Reject')}</Text>
                            </View>
                        </View>
                    </View>
                    <ScrollView>
                        {this.props.staff.viewexecuses.length > 0 ? this.props.staff.viewexecuses.map((execuse, index) => (
                            <View style={{
                                flex: 0,
                                flexDirection: 'row',
                                borderBottomWidth: 1,
                            }}>
                                <View style={{ flex: 2 }}>
                                    <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', padding: 2 }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30 }} source={Person} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{execuse.student_name}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', padding: 2 }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30, }} source={confirmStaffIcon} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1}
                                                style={styles.fontSizeText}>{execuse.senter_teachername}</Text>
                                        </View>
                                    </View>
                                    <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', padding: 2 }}>
                                        <View style={styles.ColSet20}>
                                            <Image style={{ height: 30, width: 30 }}
                                                source={confirmMsgIcon} />
                                        </View>
                                        <View style={styles.ColSet80}>
                                            <Text numberOfLines={1} style={styles.fontSizeText}>{(execuse.topic)}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.boxCenterDiv}>
                                    <View style={{
                                        flex: 0,
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'space-between'
                                    }}>
                                        <View>
                                            <TouchableOpacity activeOpacity={.5}
                                                onPress={() => this.changeExecuseStatus(execuse.behaviourId, 1)}
                                                style={[style.icon, { backgroundColor: 'green' }]}>
                                                {execuse.accept_or_reject == 1 &&
                                                    <Icon size={22} color="#fff" name="md-checkmark" />}
                                            </TouchableOpacity>
                                        </View>
                                        <View>
                                            <View style={style.icon}>
                                                <Text>{execuse.class_shortcut}</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <View style={style.icon}>
                                                <Text>{execuse.div}</Text>
                                            </View>
                                        </View>
                                        <View>
                                            <TouchableOpacity activeOpacity={.5}
                                                onPress={() => this.changeExecuseStatus(execuse.behaviourId, 2)}
                                                style={[style.icon, { backgroundColor: 'red' }]}>
                                                {execuse.accept_or_reject == 2 &&
                                                    <Icon size={22} style={{ alignSelf: 'center' }} color="#fff"
                                                        name="md-close" />}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        ))
                            :
                            <View
                                style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center' }}>
                                <View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
                                    <Text style={{
                                        display: 'flex',
                                        alignItems: 'flex-end',
                                        color: 'black',
                                        fontSize: 14,
                                        fontWeight: 'bold'
                                    }}>{strings('No View Execuses')}</Text>
                                </View>
                            </View>

                        }
                    </ScrollView>

                </View>

            </View>
        );
    }
}

const style = StyleSheet.create({
    icon: {
        borderWidth: 1,
        borderColor: 'grey',
        height: 25,
        width: 25,
        marginTop: 1,
        marginLeft: 1,
        marginRight: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
const mapDispatchToProps = dispatch => ({
    fetchExecuseValues: (schoolId) => dispatch(fetchExecuseValues(schoolId)),
    changeExecuseStatus: (schoolId, teacherId, behaviourId, statusval) => dispatch(changeExecuseStatus(schoolId, teacherId, behaviourId, statusval))
})
const mapStateToProps = (state) => {
    return {
        locale: state.locale,
        staff: state.staff,

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmExcuse)




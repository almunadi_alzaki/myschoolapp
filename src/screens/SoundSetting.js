import React from 'react';
import { Dimensions, NativeModules, ScrollView, StyleSheet, Text, TouchableOpacity, View, Platform } from 'react-native';
import { connect } from 'react-redux';
import I18n from '../i18n.js';
import { setNotificationSound } from '../actions/settings.action';
import { colors } from "../styles/LoginStyles";
import {Button, Icon} from "react-native-elements";
import Sound from "react-native-sound";
import * as firebase from "react-native-firebase";
import i18n from "../i18n";


//import styles from '../styles/LoginStyles';

class SoundSetting extends React.Component {
  state = {
    ringtones: [
      { name: 'no_sound' },
      { name: 'default' },
      { name: 'coconuts' },
      { name: 'duet' },
      { name: 'end_note' },
      { name: 'gentle_gong' },
      { name: 'mallet' },
      { name: 'orders_up' },
      { name: 'ping' },
      { name: 'pipes' },
      { name: 'popcorn' },
      { name: 'shopkeeper' },
      { name: 'sticks_and_stones' },
      { name: 'tuneup' },
      { name: 'tweeter' },
      { name: 'twinkle' },
    ]
  }
  static navigationOptions = ({navigation}) => {
    return Platform.OS === 'ios' && {
      headerLeft: (
          <View style={{flexDirection: 'row'}}>
            {/*<LeftDrawerButton navigation={navigation}/>*/}
            <TouchableOpacity
                style={{marginRight: 10, marginLeft: 10,}}
                onPress={() => navigation.pop()}
            >
              <Icon size={24} style={{alignSelf: 'center'}} color="#fff"
                    name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
            </TouchableOpacity>
          </View>
      )
    };
  };

  componentDidMount() {

    this.setState({ ringtone: this.props.sound_code })
  }

  setNotificationSound = () => {
    if (!this.state.ringtone) {
      return;
    }
    let channel = null;
    switch (this.props.popup_settings) {
      case 'always': {
        channel = new firebase.notifications.Android.Channel('always-' + this.state.ringtone, 'Always-' + this.state.ringtone, firebase.notifications.Android.Importance.Max)
          .setDescription('Always')
          .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
          .enableVibration(true);
        break;
      }
      case 'open': {
        channel = new firebase.notifications.Android.Channel('open-' + this.state.ringtone, 'Open-' + this.state.ringtone, firebase.notifications.Android.Importance.High)
          .setDescription('never')
          .setLockScreenVisibility(firebase.notifications.Android.Visibility.Private)
          .enableVibration(true);
        break;
      }
      case 'closed': {
        channel = new firebase.notifications.Android.Channel('closed-' + this.state.ringtone, 'Closed-' + this.state.ringtone, firebase.notifications.Android.Importance.High)
          .setDescription('closed')
          .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
          .enableVibration(true);
        break;
      }
      case 'never': {
        channel = new firebase.notifications.Android.Channel('never-' + this.state.ringtone, 'Never-' + this.state.ringtone, firebase.notifications.Android.Importance.Low)
          .setDescription('never')
          .setLockScreenVisibility(firebase.notifications.Android.Visibility.Private)
          .enableVibration(true);
        break;
      }
    }
    channel.setSound(this.state.ringtone);
    firebase.notifications().android.createChannels([channel]);
    this.props.setNotificationSound(this.state.ringtone);
  }

  play = (ringtone) => {
    this.setState({ ringtone: ringtone.name });
    if (Platform.OS === 'android') {
      NativeModules.RingtoneManager.stop()
        .then(() => {
          if (ringtone.name === 'default') {
            NativeModules.RingtoneManager.playDefault();
          } else {
            Sound.setCategory('Playback');
            if (this.whoosh && this.whoosh.isPlaying()) {
              this.whoosh.stop(() => {
                this.whoosh.release();
                this.playSound(ringtone);
              });
            } else {
              this.playSound(ringtone);
            }
          }
        });
    } else {
      this.playSound(ringtone);
    }
  }

  playSound = (ringtone) => {
    this.whoosh = new Sound(ringtone.name + '.mp3', Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // Play the sound with an onEnd callback
      this.whoosh.play((success) => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
        this.whoosh.release();
      });
    });
  }

  nameToLabel(name) {
    name = name.toString()[0].toUpperCase() + name.toString().slice(1);
    name = name.replace('_', ' ');
    return name;
  }

  render() {
    // I18n.locale = this.props.locale.locale;
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.state.ringtones.map(r => (
            <TouchableOpacity key={r.name} onPress={() => this.play(r)}
              style={styles.listItem}>
              <Text>
                {this.nameToLabel(r.name)}
              </Text>
              <View style={[styles.radio, this.state.ringtone === r.name && styles.radioSelected]}>
                {this.state.ringtone === r.name && <View style={styles.radioSelectedInner} />}
              </View>
            </TouchableOpacity>
          ))}
        </ScrollView>
        <View style={{ flexDirection: 'row', marginTop: 'auto' }}>
          <Button title={I18n.t('SAVE')} onPress={this.setNotificationSound} containerStyle={{ margin: '2%', width: '46%', }} />
          <Button title={I18n.t('Close')} onPress={() => this.props.navigation.goBack(null)} containerStyle={{ margin: '2%', width: '46%' }} type={"outline"} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  radio: { height: 20, width: 20, borderWidth: 0.5, borderColor: colors.grey, borderRadius: 10 },
  radioSelected: { borderColor: colors.primary, borderWidth: 1 },
  radioSelectedInner: { flex: 1, margin: 2, borderRadius: 10, backgroundColor: colors.primary },
  listItem: {
    padding: 15, margin: 5, borderWidth: 0.5, borderColor: colors.grey,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  }
});

const mapStateToProps = (state) => {
  return {
    locale: state.locale,
    isRTL: state.isRTL,
    sound_code: state.settings.sound_code,
    popup_settings: state.settings.popup_settings,
  }
}

const mapDispatchToProps = dispatch => ({
  setNotificationSound: (notificationSound) => dispatch(setNotificationSound(notificationSound))
})


export default connect(mapStateToProps, mapDispatchToProps)(SoundSetting)

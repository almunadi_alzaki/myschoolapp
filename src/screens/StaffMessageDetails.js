import React from 'react';
import { Linking, Platform, Text, TouchableOpacity, View } from 'react-native';
import styles from '../styles/LoginStyles';
import { staffmessageRead } from '../actions/messageActions';
import { connect } from 'react-redux';
import realm from "../realm/schema";
import { Icon } from "react-native-elements";
import i18n from "../i18n";


class StaffMessageDetails extends React.Component {
  constructor(props) {
    super(props);

    let isRTL = this.props.locale.locale == "ar" ? true : false;
    this.state = {
      isRTL: isRTL

    };

    //  I18n.locale = this.state.locale;
  }

  componentDidMount() {

    this.props.navigation.setParams({});
    console.log("in did mount message details")
    let msg = this.props.navigation.state.params
    const message = realm.objectForPrimaryKey('StaffMessages', msg.message.id);
    if (message) {
      realm.write(() => {
        message.read = true;
      })
    }
    this.props.staffmessageRead(msg.message)
    this.setState({
      // isRTL: this.props.locale.isRTL,
    })
  }
  static navigationOptions = ({ navigation }) => {
    return Platform.OS === 'ios' && {
      headerLeft: (
        <View style={{ flexDirection: 'row' }}>
          {/*<LeftDrawerButton navigation={navigation}/>*/}
          <TouchableOpacity
            style={{ marginRight: 10, marginLeft: 10, }}
            onPress={() => navigation.pop()}
          >
            <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
              name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
          </TouchableOpacity>
        </View>
      )
    };
  };


  render() {
    const message = this.props.navigation.state.params.message
    const image = message.message.toString().match(/^nenewe.com(.*?).*$/gim);
    return (
      <View style={styles.container}>


        <View style={styles.containerCenter}>
          {/* { this.props.navigation.state.params.student } */}
          <View style={{ backgroundColor: '#f9f9f9', padding: 6, marginBottom: 5, borderRadius: 10 }}>

            <View>
              <Text style={{ color: 'black', fontSize: 22 }}>{message.schoolName}</Text>
              <Text style={{
                color: 'black',
                fontSize: 16,
                textAlign: this.state.isRTL ? 'right' : 'left'
              }}>{message.message.replace(image, '')}</Text>
              {image && <TouchableOpacity onPress={() => Linking.openURL('http://' + image)}>
                <Text style={{ color: 'blue', textDecorationLine: 'under-line' }}>{image}</Text>
              </TouchableOpacity>}
            </View>
          </View>

          <View style={{ backgroundColor: '#f9f9f9', padding: 6, marginBottom: 5, borderRadius: 10 }}>

            <Text style={{ color: 'black', fontSize: 18 }}>{message.date}</Text>

          </View>
        </View>

      </View>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("Staff msg details..", state)
  return {
    locale: state.locale,
    student: state.student,
    staff: []

  }
}

const mapDispatchToProps = dispatch => ({
  staffmessageRead: (msg) => dispatch(staffmessageRead(msg))
})


export default connect(mapStateToProps, mapDispatchToProps)(StaffMessageDetails)

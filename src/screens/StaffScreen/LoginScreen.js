import React from 'react';
import { Image, Keyboard, Text, TextInput, TouchableHighlight, View} from 'react-native';
import Logo from '../../images/staff-icon.png';
import Parent from '../../imagesicon/home2_36.png';
import Teacher from '../../imagesicon/home2_38.png';
import Staff from '../../imagesicon/staff_32.png';
import Degree from '../../imagesicon/loginscreen_03.png';
import Lock from '../../imagesicon/loginscreen_06.png';
import I18n from '../../i18n.js';
import StyleSheetFactory from '../../styles/Ar_loginStyles.js';
import  Button  from "../../components/Button";
import {connect} from 'react-redux';
import {addStaffToApp, getStaffLocal} from '../../actions/staffactions';
import {getUnreadStaffMessages} from '../../actions/messageActions'


class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        console.log(" in const login screen+ " + this.props.locale.locale)
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = {isRTL: isRTL, schoolId: '', password: ''};
    }

    componentDidMount() {

        this.props.navigation.setParams({});
        this.props.getStaffLocal()
        this.props.getUnreadStaffMessages()
        console.log(" in did mount login screen+ " + this.props.locale.locale)
        this.setState({isRTL: this.props.locale.isRTL})
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (!this.props.staff.staffs.length && nextProps.staff.staffs.length) {
            this.setState({schoolId: '', password: ''});
            this.props.navigation.navigate('Staff');
        }
    }


    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: I18n.t('School App'),
        };
    };

    addStaffToApp = () => {
        Keyboard.dismiss();
        this.props.addStaffToApp(this.state.schoolId,
            this.state.password, this.props.locale.deviceToken)
    }

    render() {
        if (this.props.staff.staffs.length == 0) {
            this.props.navigation.navigate('StaffLogin');
        }

        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
        const {navigate} = this.props.navigation;
        const {userPermission} = this.state;
        return (
            <View style={styles.container}>
                <View style={styles.containerCenter}>
                    <View style={styles.imageCenter}>
                        <Image style={styles.imageLogo} source={Logo}/>
                    </View>
                    <View style={{
                        flex: 0,
                        flexDirection:  'row',
                        borderColor: 'gray',
                        borderWidth: 1,
                        width: '90%',
                        marginLeft: '5%',
                        marginRight: '5%'
                    }}>
                        <View style={styles.LogininputIcon}>
                            <Image style={styles.iconSize} source={Degree}/>
                        </View>
                        <View>
                            <TextInput style={styles.inputSet}
                                       onChangeText={(schoolId) => this.setState({schoolId})}
                                       value={this.state.schoolId}
                                       placeholder={I18n.t('School ID')}/>
                        </View>
                    </View>
                    <View style={{
                        flex: 0,
                        flexDirection:  'row',
                        borderColor: 'gray',
                        borderWidth: 1,
                        marginBottom: 10,
                        width: '90%',
                        marginLeft: '5%',
                        marginRight: '5%'
                    }}>
                        <View style={styles.LogininputIcon}>
                            <Image style={styles.iconSize} source={Lock}/>
                        </View>
                        <View>
                            <TextInput style={styles.inputSet}
                                       onChangeText={(password) => this.setState({password})}
                                       value={this.state.password}
                                       placeholder={I18n.t('Password')}/>
                        </View>
                    </View>
                    <View style={{margin: 10}}>
                        <Button color="#5f021f" title={I18n.t('Login')}
                                onPress={this.addStaffToApp}/>
                    </View>
                </View>

			<View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-end', marginBottom: 5}}>
                <View style={styles.FooterBottom}>
                    <View style={[{flex: 1}, styles.ColSetHeighlight]}>
                        <View style={styles.FlexCenter}>
                            <Image style={styles.FooterImage} source={Parent} />
                            <TouchableHighlight onPress={() => navigate('Login')}>
                                <Text style={{
                                    textAlign: 'center',
                                    color: 'white',
                                }}>{I18n.t('Parent')}</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>

                <View style={styles.FooterBottom}>
                    <View style={{flex: 1}}>
                        <View style={styles.FlexCenter}>
                            <TouchableHighlight onPress={() => navigate('StaffLogin')}>
                                <Image style={styles.FooterImageStaff} source={Staff} />
                            </TouchableHighlight>
                            <TouchableHighlight onPress={() => navigate('Staff')}>
                                <Text style={{textAlign: 'center', color: 'white'}}>{I18n.t('Staff')}</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>

                <View style={styles.FooterBottom}>
                    {this.props.staff.staffs.length > 0 && <View style={{flex: 1}}>
                        <View style={styles.FlexCenter}>
                            <TouchableHighlight onPress={() => navigate('Teacher')}>
                                <Image
                                    style={styles.FooterImageStaff}
                                    source={Teacher}
                                />
                            </TouchableHighlight>
                            <TouchableHighlight onPress={() => navigate('Teacher')}>
                                <Text style={{textAlign: 'center', color: 'white'}}
                                        onPress={() => navigate('Teacher')}>{I18n.t('Teacher')}</Text>
                            </TouchableHighlight>
                        </View>
                    </View>}
                </View>
                    </View>
                </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addStaffToApp: (schoolId, password, token) => dispatch(addStaffToApp(schoolId, password, token)),
    getStaffLocal: () => dispatch(getStaffLocal()),
    getUnreadStaffMessages: () => dispatch(getUnreadStaffMessages())

})


const mapStateToProps = (state) => {

    const {login} = state
    // return { login }
    return {
        locale: state.locale,
        staff: state.staff
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen)

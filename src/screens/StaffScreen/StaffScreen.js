import React from 'react';
import { Image, ScrollView, Text, TouchableHighlight, TouchableOpacity, View, } from 'react-native';
import Mail from '../../images/mail-white.png';
import Parent from '../../images/Parent-icon.png';
import Staff from '../../imagesicon/home2_38.png';
import Teacher from '../../imagesicon/confirmStaff.png';
import RightArrow from '../../imagesicon/right-arrow.png';
import LeftArrow from '../../imagesicon/left-arrow.png';
import StyleSheetFactory from '../../styles/Ar_loginStyles.js';
import { getStaffMenus, logoutStaff, updateApp, } from '../../actions/staffactions';
import I18n from '../../i18n.js';
import { connect } from 'react-redux';
import { NavigationEvents } from "react-navigation";
import { getLoggedInUser, getRightMenu } from "../../actions/general.action";
import { LeftDrawerButton } from "../SchoolAppNavigator";
import { setPageScreen } from "../../actions/actions";
import ModalDialog from "../../components/ModalDialog";
import { strings } from "../../i18n";


class StaffScreen extends React.Component {
	constructor(props) {
		super(props);
		let isRTL = this.props.locale.locale == "ar" ? true : false;
		this.state = { isRTL: isRTL };
	}

	componentDidMount() {

		const { staff } = this.props;
		this.props.setPageScreen('staff');
		if (staff.staffs.length) {
			const { teacherId, schoolId } = staff.staffs.find(f => f.schoolId === staff.schoolId);
			this.props.navigation.setParams({
				teacher: staff.staffs.find(f => f.schoolId === staff.schoolId),
			});
			this.props.getLoggedInUser({ teacherId, schoolId });
			this.props.getStaffMenus(schoolId, teacherId);
		}
	}

	componentWillReceiveProps(nextProps, nextContext) {
		if (!this.props.staff.staffs.length && nextProps.staff.staffs.length) {
			const { teacherId, schoolId } = nextProps.staff.staffs.find(f => f.schoolId === nextProps.staff.schoolId);
			this.props.getStaffMenus(schoolId, teacherId);
			this.props.getLoggedInUser({ teacherId, schoolId });
			nextProps.navigation.setParams({
				teacher: nextProps.staff.staffs.find(f => f.schoolId === nextProps.staff.schoolId),
			});
		}
		if (nextProps.staff.staffs.length && this.props.staff.schoolId !== nextProps.staff.schoolId) {
			const { teacherId, teacherName, messageCount, schoolId } = nextProps.staff.staffs.find(f => f.schoolId === nextProps.staff.schoolId);
			this.props.getStaffMenus(schoolId, teacherId);
			this.props.getLoggedInUser({ teacherId, schoolId });
			nextProps.navigation.setParams({
				teacher: nextProps.staff.staffs.find(f => f.schoolId === nextProps.staff.schoolId),
			});
		}
	}

	logOutApp = () => {
		const teacherId = this.props.staff.staffs.find(f => f.schoolId === this.props.staff.schoolId).teacherId;
		const deviceToken = this.props.locale.deviceToken;
		this.props.navigation.navigate('StaffLogin');
		this.props.logoutStaff(teacherId, deviceToken);
	}

	static navigationOptions = ({ navigation }) => {
		const teacher = navigation.getParam('teacher') || {};
		console.log(teacher, 'msg')
		return {
			headerTitle: teacher.teacherName,
			headerLeft: (
				<View style={{ flexDirection: 'row' }}>
					<LeftDrawerButton navigation={navigation} />
					<TouchableOpacity
						style={{ marginRight: 10, marginLeft: 10, }}
						onPress={() => navigation.navigate('StaffMessageScreen', { teacherId: teacher.teacherId })}>
						<Image style={{ height: 30, width: 30, alignSelf: 'flex-start' }}
							source={Mail} />
						<View style={{
							height: 18,
							width: 18,
							borderRadius: 10,
							borderWidth: 0.5,
							borderColor: '#fff',
							backgroundColor: '#5f021f',
							position: 'absolute',
							top: 0,
							right: 18,
							justifyContent: 'center',
							alignItems: 'center'
						}}>
							<Text style={{ color: 'white' }}>{teacher.messageCount}</Text>
						</View>
					</TouchableOpacity>
				</View>
			)
		};
	};

	gotoStaffMenu = (menu) => {
		console.log("Menu Id..", menu);
		switch (menu.menu_id) {
			case "3":
				this.props.navigation.navigate('StudentFilter', { menu });
				break;
			case "4":
				this.props.navigation.navigate('StudentFilter', { menu });
				break;
			case "11":
				this.props.navigation.navigate('StudentFilter', { menu });
				break;
			case "14":
				this.props.navigation.navigate('StudentFilter', { menu });
				break;
			case "16":
				this.props.navigation.navigate('StudentFilter', { menu });
				break;
			case "13":
				this.props.navigation.navigate('ConfirmExcuse', { menu });
				break;
			case "12":
				this.props.navigation.navigate('ViewExecuse', { menu });
				break;
			case "15":
				this.props.navigation.navigate('ViewTodayBehaviours', { menu });
				break;
			case "17":
				this.props.navigation.navigate('GuestBookList', { menu });
				break;
		}

	}
	getRightMenu = () => {
		if (!this.props.staff.staffs.length) {
			return;
		}
		const staff = this.props.staff.staffs[0];
		this.props.getRightMenu({ teacherId: staff.teacherId, schoolId: staff.schoolId, page_type: 'staff' });
	}

	render() {
		const { navigate } = this.props.navigation;
		styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
		const { appPermission, staff } = this.props;
		let loggedTo = 0;
		// if (appPermission && staff.staffs.length) {
		// 	const permission = appPermission.find(p => p.teacherId === staff.staffs.find(f => f.schoolId === staff.schoolId).teacherId);
		// 	if (permission && permission) {
		// 		loggedTo = permission.logged_to;
		// 	}
		// }
		return (
			<View style={styles.container}>
				<ModalDialog show={loggedTo === 10}
					onAction={() => this.props.updateApp()}
					buttonTitle={strings('Close')} text={strings('updateApp')} />
				<ModalDialog show={loggedTo === 4}
					onAction={this.logOutApp}
					buttonTitle={strings('Close')} text={strings('blocked')} />
				<NavigationEvents
					onWillFocus={this.getRightMenu}
				/>
				<View style={styles.containerCenter}>
					<View style={{
						flex: 0,
						flexDirection: 'row'
					}}>
						<View style={styles.topStaffRow}>
							<View style={styles.imageStaffAdmin}>
								<Image style={styles.imageLogo} source={Staff} />
							</View>
						</View>
					</View>
					<ScrollView contentContainerStyle={{ paddingBottom: 100 }}>
						{this.props.staff.menupermissions.length > 0 ? this.props.staff.menupermissions
							.filter(p => p.menu_for === 's').map((menu, index) => (
								<TouchableOpacity onPress={() => this.gotoStaffMenu(menu)}>
									<View style={styles.staffMenusList}>
										<View style={styles.LogininputIcon}>
											<Image style={styles.iconSize} source={{ uri: menu.icon_label }} />
										</View>
										<View>
											<Text
												style={styles.staffMenuName}>{!this.props.locale.isRTL ? menu.menu_name_en : menu.menu_name_ar}</Text>
										</View>
										<View style={styles.staffMenuRightArrow}>
											<Image style={styles.rightArrowIconSize}
												source={!this.props.locale.isRTL ? RightArrow : LeftArrow} />
										</View>

									</View>
								</TouchableOpacity>
							))
							:
							<View
								style={{ flex: 0, flexDirection: 'row', borderColor: 'gray', justifyContent: 'center' }}>
								<View style={{ borderBottomWidth: 1, height: 30, width: 250 }}>
									<Text style={{
										display: 'flex',
										alignItems: 'flex-end'
									}}>{I18n.t('No Permission')}</Text>
								</View>
							</View>
						}
					</ScrollView>
				</View>

				<View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', marginBottom: 5 }}>
					<View style={styles.FooterBottom}>
						<View style={{ flex: 1, flexDirection: 'row' }}>
							<View style={{ flex: 1 }}>
								<View style={styles.FlexCenter}>
									<Image style={styles.FooterImage} source={Parent} />
									<TouchableHighlight onPress={() => navigate('Login')}>
										<Text style={styles.footerTabMenu}>{I18n.t('Parent')}</Text>
									</TouchableHighlight>
								</View>
							</View>
						</View>
					</View>

					<View style={styles.FooterBottom}>
						<View style={[{ flex: 1 }, styles.ColSetHeighlight]}>
							<View style={styles.FlexCenter}>
								<TouchableHighlight onPress={() => navigate('Staff')}>
									<Image style={styles.FooterImageStaff} source={Staff} />
								</TouchableHighlight>
								<TouchableHighlight>
									<Text style={styles.footerTabMenu}>{I18n.t('Staff')}</Text>
								</TouchableHighlight>
							</View>
						</View>
					</View>

					<View style={styles.FooterBottom}>
						{this.props.staff.staffs.length > 0 && <View style={{ flex: 1 }}>
							<View style={styles.FlexCenter}>
								<TouchableHighlight onPress={() => navigate('Teacher')}>
									<Image style={styles.FooterImageStaff} source={Teacher} />
								</TouchableHighlight>
								<TouchableHighlight onPress={() => navigate('Teacher')}>
									<Text style={styles.footerTabMenu}
										onPress={() => navigate('Teacher')}>{I18n.t('Teacher')}</Text>
								</TouchableHighlight>
							</View>
						</View>}
					</View>
				</View>
			</View>

		);
	}
}

const mapDispatchToProps = dispatch => ({
	getStaffMenus: (schoolId, teacherId) => dispatch(getStaffMenus(schoolId, teacherId)),
	setPageScreen: (page) => dispatch(setPageScreen(page)),
	getRightMenu: (data) => dispatch(getRightMenu(data)),
	logoutStaff: (teacherId, deviceToken) => dispatch(logoutStaff(teacherId, deviceToken)),
	updateApp: (schoolId) => dispatch(updateApp(schoolId)),
	getLoggedInUser: (data) => dispatch(getLoggedInUser(data)),
	// onFetchClassesValues: (schoolId) => dispatch(fetchFilterClassesValues(schoolId)),
	// onFetchDivValues: (schoolId) => dispatch(fetchFilterDivsValues(schoolId)),
	// getAllClassesLocal: () => dispatch(getAllClassesLocal()),
	// getAllDivsLocal: (classId) => dispatch(getAllDivsLocal(classId)),
	// fetchFilterSubjectsValues: (schoolId) => dispatch(fetchFilterSubjectsValues(schoolId)),
	// getAllSubjectsLocal: () => dispatch(getAllSubjectsLocal()),
	// fetchFilterStudentsValues: (schoolId, classId) => dispatch(fetchFilterStudentsValues(schoolId)),
})

const mapStateToProps = (state) => {
	return {
		locale: state.locale,
		appPermission: state.general.appPermission,
		staff: state.staff
	}
}


export default connect(mapStateToProps, mapDispatchToProps)(StaffScreen)

import React from 'react';
import {
  Image,
  Keyboard,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  View,
  Dimensions,
} from 'react-native';
import Logo from '../images/newstudent.png';
import Parent from '../imagesicon/home2_36.png';
import Staff from '../imagesicon/home2_38.png';
import Teacher from '../imagesicon/confirmStaff.png';
import Degree from '../imagesicon/loginscreen_03.png';
import Lock from '../imagesicon/loginscreen_06.png';
import I18n from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import {connect} from 'react-redux';
import {
  addStudent,
  deleteStudent,
  getStudentLocal,
  setPageScreen,
  studentCall,
} from '../actions/actions';
import StudentListComp from '../components/login/StudentListComp';
import {addStaffToApp, getStaffLocal} from '../actions/staffactions';
import {NavigationEvents} from 'react-navigation';
import {
  getLoggedInUser,
  getRightMenu,
  getRightMenuSuccess,
} from '../actions/general.action';
import ModalDialog from '../components/ModalDialog';
import {strings} from '../i18n';
import Button from '../components/Button';
import Checkbox from '../components/Checkbox';
const {width} = Dimensions.get('window');
class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    let isRTL = this.props.locale.locale === 'ar' ? true : false;
    this.state = {
      isRTL: isRTL,
      schoolId: '',
      password: '',
      checkedStudent: false,
      checkedStaff: false,
    };
  }

  componentDidMount() {
    this.props.navigation.setParams({});
    this.props.getStudentLocal(this.props.staff.schoolId);
    this.props.getStaffLocal();
    this.setState({isRTL: this.props.locale.isRTL});
    this.props.setPageScreen('parent');
    this.setState({pageScreen: 'parent'});
  }

  componentWillReceiveProps(nextProps) {
    const {staff, student} = nextProps;
    if (student.students.length !== this.props.student.students.length)
      this.setState({schoolId: '', password: '', checkedStudent: false});

    if (student && student.students.length > 0 && !this.state.student) {
      const student = nextProps.student.students[0];
      const {school_id, id} = student;
      this.setState({student});
      nextProps.student.students.forEach(s =>
        this.props.getLoggedInUser({
          schoolId: s.school_id,
          studentId: s.id,
        }),
      );
      this.props.getRightMenu({school_id, page_type: 'parent'});
    }
    if (!this.props.staff.staffs.length && nextProps.staff.staffs.length) {
      this.setState({schoolId: '', password: '', checkedStaff: false});
    }
    if (staff && staff.staffs.length > 0) {
      this.setState({staff: true});
    } else {
      this.setState({staff: false});
    }
  }

  loginChangHandler = () => {
    if (this.state.checkedStudent) {
      Keyboard.dismiss();
      this.props.addStudent(
        this.state.schoolId,
        this.state.password,
        this.props.locale.deviceToken,
      );
    } else if (this.state.checkedStaff) {
      Keyboard.dismiss();
      this.props.addStaffToApp(
        this.state.schoolId,
        this.state.password,
        this.props.locale.deviceToken,
      );
      this.setState({checkedStaff: false});
    }
  };

  getRightMenus = () => {
    if (!this.props.student.students.length) {
      this.props.getRightMenuSuccess([]);
      return;
    }
    const student = this.props.student.students[0];
    const {school_id} = student;
    this.props.setPageScreen('parent');
    this.props.getRightMenu({school_id, page_type: 'parent'});
  };

  deleteStudent = student => {
    student.forEach(p =>
      this.props.deleteStudent(
        p,
        this.props.student.students,
        this.props.locale.deviceToken,
      ),
    );
  };

  studentToggleCheckBox = () => {
    this.setState({
      checkedStudent: !this.state.checkedStudent,
      checkedStaff: false,
    });
  };

  staffToggleCheckBox = () => {
    this.setState({
      checkedStaff: !this.state.checkedStaff,
      checkedStudent: false,
    });
  };

  render() {
    styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
    const {navigate} = this.props.navigation;
    let loggedTo = 0;
    const {appPermission} = this.props;
    const {checkedStudent, checkedStaff} = this.state;
    const students = this.props.student.students.filter(s =>
      appPermission.find(p => p.logged_to === 4 && p.studentId === s.id),
    );

    return (
      <View style={styles.container}>
        <ModalDialog
          show={loggedTo === 10}
          buttonTitle={strings('Close')}
          text={strings('updateApp')}
        />
        <ModalDialog
          show={students.length}
          onAction={() => this.deleteStudent(students)}
          buttonTitle={strings('Close')}
          text={strings('blocked')}
        />
        <NavigationEvents onWillFocus={this.getRightMenus} />
        <View>
          <View style={styles.imageCenter}>
            <Image style={styles.imageLogo} source={Logo} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              borderColor: 'gray',
              borderWidth: 1,
              width: width - 40,
              marginLeft: 20,
              marginRight: 20,
              borderRadius: 5,
            }}>
            <View style={styles.LogininputIcon}>
              <Image style={styles.iconSize} source={Degree} />
            </View>
            <TextInput
              style={[
                styles.inputSet,
                {textAlign: this.props.locale.isRTL ? 'right' : 'left'},
              ]}
              onChangeText={schoolId => this.setState({schoolId})}
              value={this.state.schoolId}
              placeholder={I18n.t('School ID')}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              borderColor: 'gray',
              borderWidth: 1,
              marginBottom: 10,
              width: width - 40,
              marginLeft: 20,
              marginRight: 20,
              marginTop: 10,
              borderRadius: 5,
            }}>
            <View style={styles.LogininputIcon}>
              <Image style={styles.iconSize} source={Lock} />
            </View>
            <TextInput
              style={[
                styles.inputSet,
                {textAlign: this.props.locale.isRTL ? 'right' : 'left'},
              ]}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
              placeholder={I18n.t('Password')}
            />
          </View>

          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Checkbox
                style={{marginLeft: 10}}
                value={this.state.checkedStaff}
                clicked={this.staffToggleCheckBox}
              />
              <Text style={{marginLeft: 5}}>{I18n.t('Staff/Teacher')}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Checkbox
                style={{marginLeft: 10}}
                value={this.state.checkedStudent}
                clicked={this.studentToggleCheckBox}
              />
              <Text style={{marginLeft: 5}}>{I18n.t('Student')}</Text>
            </View>
          </View>
          <View style={{margin: 10, alignItems: 'center'}}>
            <Button
              color="#5f021f"
              width={'50%'}
              disabled={!checkedStudent && !checkedStaff}
              title={I18n.t('Add Student')}
              onPress={
                (checkedStaff || checkedStudent) && this.loginChangHandler
              }
            />
          </View>

          <ScrollView
            contentContainerStyle={{
              flexGrow: 0,
              paddingBottom: 50,
              justifyContent: 'space-between',
            }}>
            {this.props.student.students.map((student, key) => {
              if (student != null) {
                return (
                  <StudentListComp
                    key={key}
                    locale={this.props.locale.locale}
                    studentCall={this.props.studentCall}
                    navigation={navigate}
                    student={student}
                  />
                );
              }
            })}
          </ScrollView>
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'flex-end',
            marginBottom: 5,
          }}>
          <View style={styles.FooterBottom}>
            <View style={[{flex: 1}, styles.ColSetHeighlight]}>
              <View style={styles.FlexCenter}>
                <Image style={styles.FooterImage} source={Parent} />
                <TouchableHighlight onPress={() => navigate('Login')}>
                  <Text style={styles.footerTabMenu}>{I18n.t('Parent')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>

          <View style={styles.FooterBottom}>
            <View style={{flex: 1}}>
              <View style={styles.FlexCenter}>
                <TouchableHighlight
                  onPress={() =>
                    this.props.staff.staffs.length > 0 &&
                    navigate(this.state.staff ? 'Staff' : 'StaffLogin')
                  }>
                  <Image style={styles.FooterImageStaff} source={Staff} />
                </TouchableHighlight>
                <TouchableHighlight
                  onPress={() =>
                    this.props.staff.staffs.length > 0 &&
                    navigate(this.state.staff ? 'Staff' : 'StaffLogin')
                  }>
                  <Text style={styles.footerTabMenu}>{I18n.t('Staff')}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>

          <View style={styles.FooterBottom}>
            {
              <View style={{flex: 1}}>
                <View style={styles.FlexCenter}>
                  <TouchableHighlight
                    onPress={() =>
                      this.props.staff.staffs.length > 0 && navigate('Teacher')
                    }>
                    <Image style={styles.FooterImageStaff} source={Teacher} />
                  </TouchableHighlight>
                  <TouchableHighlight
                    onPress={() =>
                      this.props.staff.staffs.length > 0 && navigate('Teacher')
                    }>
                    <Text
                      style={styles.footerTabMenu}
                      onPress={() => navigate('Teacher')}>
                      {I18n.t('Teacher')}
                    </Text>
                  </TouchableHighlight>
                </View>
              </View>
            }
          </View>
        </View>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addStaffToApp: (schoolId, password, token) =>
    dispatch(addStaffToApp(schoolId, password, token)),
  addStudent: (schoolId, password, token) =>
    dispatch(addStudent(schoolId, password, token)),
  getStudentLocal: schoolId => dispatch(getStudentLocal(schoolId)),
  getStaffLocal: () => dispatch(getStaffLocal()),
  setPageScreen: page => dispatch(setPageScreen(page)),
  getRightMenu: data => dispatch(getRightMenu(data)),
  getRightMenuSuccess: data => dispatch(getRightMenuSuccess(data)),
  deleteStudent: (student, students, token) =>
    dispatch(deleteStudent(student, students, token)),
  getLoggedInUser: data => dispatch(getLoggedInUser(data)),
  studentCall: (schoolId, id, secretId, st_name, username) =>
    dispatch(studentCall(schoolId, id, secretId, st_name, username)),
});

const mapStateToProps = state => {
  const {login} = state;
  // return { login }
  return {
    locale: state.locale,
    student: state.student,
    staff: state.staff,
    appPermission: state.general.appPermission,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);

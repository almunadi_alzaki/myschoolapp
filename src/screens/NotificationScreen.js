import React from 'react';
import {Platform, Text, TouchableOpacity, View} from 'react-native';
//import styles from '../styles/LoginStyles';
import { connect } from 'react-redux';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import { RadioButton } from 'react-native-paper';
import { setNotificationPopup } from '../actions/settings.action';
import I18n from '../i18n.js';
import {Icon} from "react-native-elements";
import i18n from "../i18n";


class NotificationScreen extends React.Component {

  static navigationOptions = ({navigation}) => {
    return Platform.OS === 'ios' && {
      headerLeft: (
          <View style={{flexDirection: 'row'}}>
            {/*<LeftDrawerButton navigation={navigation}/>*/}
            <TouchableOpacity
                style={{marginRight: 10, marginLeft: 10,}}
                onPress={() => navigation.pop()}
            >
              <Icon size={24} style={{alignSelf: 'center'}} color="#fff"
                    name={i18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
            </TouchableOpacity>
          </View>
      )
    };
  };

  componentDidMount(){

  }

  render() {
    this.state = {
      checked: 'always',
    };
    if (this.props.settings.popup_settings !== undefined) {
      this.state = {
        checked: this.props.settings.popup_settings,
      };
    } else {
      this.state = {
        checked: 'always',
      };
    }


    //console.log("settings popup....", this.props.locale.notificationPopup);

    styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
    const {checked} = this.state.checked;
    //console.log("Checked....",checked);
    return (
        <View style={styles.container}>

          <View style={styles.containerCenter}>
            <View style={{
              flex: 0,
              flexDirection: 'row',
              padding: 12,
              borderBottomWidth: 1,
              borderBottomColor: '#f0f0f0',
            }}>

              <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                  <Text style={{color: 'black', fontSize: 18}}>{I18n.t('Never Appear')}</Text>
                </View>
              </View>
              <View style={styles.ColSet10}>

                <RadioButton
                    value="never"
                    status={this.state.checked === 'never' ? 'checked' : 'unchecked'}
                    onPress={() => this.props.setNotificationPopup('never')}
                />

              </View>
            </View>

            <View style={{
              flex: 0,
              flexDirection: 'row',
              padding: 12,
              borderBottomWidth: 1,
              borderBottomColor: '#f0f0f0'
            }}>

              <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                  <Text style={{color: 'black', fontSize: 18}}>{I18n.t('When the screen is open')}</Text>
                </View>
              </View>
              <View style={styles.ColSet10}>

                <RadioButton
                    value="screenOpen"
                    status={this.state.checked === 'screenOpen' ? 'checked' : 'unchecked'}
                    onPress={() => this.props.setNotificationPopup('screenOpen')}
                />


              </View>
            </View>

            <View style={{
              flex: 0,
              flexDirection: 'row',
              padding: 12,
              borderBottomWidth: 1,
              borderBottomColor: '#f0f0f0',
            }}>

              <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                  <Text style={{color: 'black', fontSize: 18}}>{I18n.t('When the screen is closed')}</Text>
                </View>
              </View>
              <View style={styles.ColSet10}>

                <RadioButton
                    value="screenClose"
                    status={this.state.checked === 'screenClose' ? 'checked' : 'unchecked'}
                    onPress={() => this.props.setNotificationPopup('screenClose')}
                />

              </View>
            </View>

            <View style={{
              flex: 0,
              flexDirection: 'row',
              padding: 12,
              borderBottomWidth: 1,
              borderBottomColor: '#f0f0f0'
            }}>

              <View style={styles.ColSet90}>
                <View style={styles.textAlignDiv}>
                  <Text style={{color: 'black', fontSize: 18}}>{I18n.t('Always Appear')}</Text>
                </View>
              </View>
              <View style={styles.ColSet10}>

                <RadioButton
                    value="always"
                    status={this.state.checked === 'always' ? 'checked' : 'unchecked'}
                    onPress={() => this.props.setNotificationPopup('always')}
                />

              </View>
            </View>

          </View>

        </View>
    );
  }
}

const mapStateToProps = (state) => {

  return {
    locale: state.locale,
    isRTL: state.isRTL,
    settings: state.settings
  }
}

const mapDispatchToProps = dispatch => ({
  setNotificationPopup: (popupsettings) => dispatch(setNotificationPopup(popupsettings))
})


export default connect(mapStateToProps, mapDispatchToProps)(NotificationScreen)

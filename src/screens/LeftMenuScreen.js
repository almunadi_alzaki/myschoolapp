import React from 'react';
import { Image, ScrollView, Text, View } from 'react-native';
//import styles from '../styles/LoginStyles';
import LanguageIcon from '../imagesicon/setting_03.png';
import SoundIcon from '../imagesicon/setting_08.png';
import StaffIcon from '../images/staff-icon.png'
import SoundIconRight from '../imagesicon/right_sound.png';
import DeleteIcon from '../imagesicon/deleteMenuIcon.png';
import ShareIcon from '../imagesicon/setting_14.png';
import UpdateIcon from '../imagesicon/setting_17.png';
import LogoutIcon from '../imagesicon/setting_19.png';
import TeacherIcon from '../imagesicon/teacher.png';
import { getLocale } from '../actions/actions';
import { logoutStaff, updateApp } from '../actions/staffactions';
import I18n from '../i18n.js';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

class LeftMenuScreen extends React.Component {

    constructor(props) {
        super(props);
        console.log("Left menu screen open..", props);
        let isRTL = this.props.locale.locale === "ar" ? true : false;
        this.state = { isRTL: isRTL };

    }

    componentDidMount() {

        getLocale();
    }

    addUser = () => {
        this.setState(prevState => {
            return {
                text: '',
                users: [...prevState.users, prevState.text]
            }
        })
    }

    navigateToScreen = (route) => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
    }

    logOutApp(teacherId, deviceToken) {
        this.props.navigation.navigate('Login');
        this.props.logoutStaff(teacherId, deviceToken)

    }

    updateApp = () => {
        console.log("Updating app...");
        if (this.props.staff.staffs.length > 0 && this.props.pageScreen === 'staff') {
            // const schoolId = this.props.staff.staffs.map(a => a.schoolId);
            this.props.updateApp();
            this.props.navigation.navigate('Staff');
        } else if (this.props.student.students.length > 0) {
            this.props.updateApp();
            this.props.navigation.navigate('Login');
        }
        this.props.navigation.toggleInnerDrawer();
    }

    render() {
        styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
        const teacherId = this.props.staff.staffs.filter(a => a.schoolId === this.props.staff.schoolId).map(a => a.teacherId);
        const deviceToken = this.props.locale.deviceToken;
        return (
            <View style={styles.leftMenuContainer}>
                <ScrollView>
                    <View>
                        <Text style={styles.sectionHeadingStyle}>
                            {I18n.t('Settings')}
                        </Text>


                        <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon} source={LanguageIcon} />
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('LanguageSetting')}>
                                {I18n.t('Language Settings')}
                            </Text>
                        </View>

                        <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon} source={StaffIcon} />
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('AddStaff')}>
                                {I18n.t('AddStaff')}
                            </Text>
                        </View>

                        {/*<View style={styles.navSectionStyle}>*/}
                        {/*    <Image style={styles.navMenuIcon} source={TeacherIcon} />*/}
                        {/*    <Text style={styles.navItemStyle} onPress={this.navigateToScreen('TeacherVisit')}>*/}
                        {/*        {I18n.t('TeacherVisit')}*/}
                        {/*    </Text>*/}
                        {/*</View>*/}

                        <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon}
                                source={!this.props.locale.isRTL ? SoundIcon : SoundIconRight} />
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('SoundSetting')}>

                                {I18n.t('Sound Settings')}
                            </Text>
                        </View>
                        {this.props.pageScreen === 'parent' && <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon} source={DeleteIcon} />
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('DeleteStudent')}>

                                {I18n.t('Delete Student')}
                            </Text>
                        </View>}
                        <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon} source={ShareIcon} />
                            <Text style={styles.navItemStyle} onPress={this.navigateToScreen('Share App')}>
                                {I18n.t('Share App')}
                            </Text>
                        </View>
                        <View style={styles.navSectionStyle}>
                            <Image style={styles.navMenuIcon} source={UpdateIcon} />
                            <Text style={styles.navItemStyle} onPress={this.updateApp}>
                                {I18n.t('Update')}
                            </Text>
                        </View>
                        {this.props.staff.staffs.length > 0 ?
                            <View style={styles.navSectionStyle}>
                                <Image style={styles.navMenuIcon} source={LogoutIcon} />
                                <Text style={styles.navItemStyle}
                                    onPress={() => this.logOutApp(teacherId[0], deviceToken)}>
                                    {I18n.t('Logout')}
                                </Text>
                            </View>

                            :
                            <View style={styles.navSectionStyle}>
                                <Image style={styles.navMenuIcon} source={LogoutIcon} />
                                <Text style={styles.navItemStyle}>
                                    {I18n.t('Logout')}
                                </Text>
                            </View>
                        }
                    </View>

                </ScrollView>

            </View>
        );
    }
}

LeftMenuScreen.propTypes = {
    navigation: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        locale: state.locale,
        isRTL: state.isRTL,
        staff: state.staff,
        student: state.student,
        pageScreen: state.general.pageScreen,
    }
}

const mapDispatchToProps = dispatch => ({
    getLocale: () => dispatch(getLocale()),
    logoutStaff: (teacherId, deviceToken) => dispatch(logoutStaff(teacherId, deviceToken)),
    updateApp: (schoolId) => dispatch(updateApp(schoolId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenuScreen)

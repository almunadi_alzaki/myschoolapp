import React from 'react';
import {FlatList, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import I18n from "../../i18n";
import {FAB, RadioButton} from 'react-native-paper';
import {getGuestBooks} from "../../actions/guest-book.action";
import {DatePicker} from "../../components/Datepicker";
import {colors} from "../../styles/LoginStyles";
import {statusTypesEn, visitorTypes} from "./AddGuestBook";
import moment from "moment";
import {Icon} from "react-native-elements";


class GuestBookList extends React.Component {
    state = {
        statusId: 3,
        schoolId: '',
        startdate: moment().format('YYYY-MM-DD'),
        enddate: moment().format('YYYY-MM-DD'),
        statuschecked: 3,
        typechecked: 1,
    };
    static navigationOptions = ({navigation}) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{flexDirection: 'row'}}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{marginRight: 10, marginLeft: 10,}}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{alignSelf: 'center'}} color="#fff"
                              name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
                    </TouchableOpacity>
                </View>
            )
        };
    };


    static getDerivedStateFromProps(nextProps, prevState) {
        if (!prevState.schoolId && nextProps.pageScreen === 'parent' && nextProps.students.length) {
            const schoolId = nextProps.students[0].school_id;
            nextProps.getGuestBooks({...prevState, schoolId});
            return {schoolId}
        } else if (!prevState.schoolId && nextProps.staffs.length) {
            const schoolId = nextProps.staffs[0].schoolId;
            nextProps.getGuestBooks({...prevState, schoolId});
            return {schoolId}
        }
        return null;
    }

    navigateToAdd = () => {
        this.props.navigation.navigate('AddGuestBook');
    }
    navigateToDetails = (guestBook) => {
        this.props.navigation.navigate('GuestBookDetails', {guestBook});
    }
    changeHandler = key => (value) => {
        this.setState({[key]: value}, () => this.props.getGuestBooks(this.state));
    }

    visitorTypeOptions() {
        return visitorTypes.map((visitor_type, index) => {
            return (
                <View style={{flexDirection: 'row', flex: index === 0 ? 1.5 : 1}}>
                    <View style={{textAlign: this.props.locale === 'ar' ? 'left' : 'right', width: 30}}>
                        <RadioButton
                            value="eng"
                            status={this.state.typechecked === visitor_type.value ? 'checked' : 'unchecked'}
                            onPress={() => this.changeHandler('typechecked')(visitor_type.value)}
                        />

                    </View>

                    <View style={{height: 30,}}>

                        <Text style={{
                            color: 'black',
                            paddingRight: 5,
                            paddingLeft: 5,
                            fontSize: 14,
                            textAlign: this.props.locale === 'ar' ? 'left' : 'right',
                            marginTop: 5
                        }}>{this.props.locale === 'ar' ? visitor_type.label_ar : visitor_type.label} </Text>

                    </View>

                </View>
            );
        });


    }


    statusTypeOptions() {
        return statusTypesEn.map((status_type, index) => {
            return (
                <View style={{flexDirection: 'row', flex: index === 0 ? 1.5 : 1}}>
                    <View style={{textAlign: this.props.locale === 'ar' ? 'left' : 'right', width: 30}}>
                        <RadioButton
                            value="eng"
                            status={this.state.statuschecked === status_type.value ? 'checked' : 'unchecked'}
                            onPress={() => this.changeHandler('statuschecked')(status_type.value)}
                        />

                    </View>

                    <View style={{height: 30,}}>
                        <Text style={{
                            color: 'black',
                            paddingRight: 5,
                            paddingLeft: 5,
                            fontSize: 14,
                            textAlign: this.props.locale === 'ar' ? 'left' : 'right',
                            marginTop: 5
                        }}>{this.props.locale === 'ar' ? status_type.label_ar : status_type.label} </Text>

                    </View>

                </View>
            );
        });


    }

    render() {
        return (
            <React.Fragment>
                <View style={{width: '100%', backgroundColor: '#d8ffff'}}>
                    <View style={{flexDirection: 'row', justifyContent: 'center', width: '100%'}}>
                        <View>
                            <Text style={styles.smallLablel}>{I18n.t('From')}</Text>
                        </View>
                        <DatePicker
                            style={styles.input}
                            placeholder={"Start Date"}
                            value={this.state.startdate}
                            onChange={(_, date) => this.changeHandler('startdate')(date)}
                        />
                        <View style={{marginLeft: 30}}>
                            <Text style={styles.smallLablel}>{I18n.t('To')}</Text>
                        </View>
                        <DatePicker
                            style={styles.input}
                            placeholder={"End Date"}
                            value={this.state.enddate}
                            onChange={(_, date) => this.changeHandler('enddate')(date)}
                        />
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        {this.visitorTypeOptions()}

                    </View>

                    <View style={{flexDirection: 'row',}}>
                        {this.statusTypeOptions()}

                    </View>

                </View>
                <FlatList
                    ListEmptyComponent={() => <View style={styles.noRecords}>
                        <Text>No Records Found.</Text>
                    </View>}
                    data={this.props.visitors}
                    keyExtractor={(item) => item.visitorId.toString()}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={() => this.navigateToDetails(item)} style={styles.item}>
                            <View style={styles.row}>
                                <Text style={{fontSize: 16, fontWeight: '500'}}>{item.visitor_name}</Text>
                                <Text style={{fontSize: 14, fontWeight: '400'}}>{item.visitor_en}</Text>
                            </View>
                            <View style={styles.row}>
                                <Text style={{fontSize: 12, fontWeight: '400'}}>{item.visitor_date_en}</Text>
                                <Text style={{fontSize: 12, fontWeight: '400'}}>Status: <Text
                                    style={{fontWeight: '500'}}>{item.visitor_status_en}</Text></Text>
                            </View>
                        </TouchableOpacity>
                    )}/>
                <FAB
                    color={'#fff'}
                    icon="add"
                    label={'Add Visitor'}
                    style={styles.fab}
                    onPress={this.navigateToAdd}
                />
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        visitors: state.guestBooks.visitors,
        students: state.student.students,
        staffs: state.staff.staffs,
        locale: state.locale.locale,
        pageScreen: state.general.pageScreen
    }
}

const mapDispatchToProps = dispatch => ({
    getGuestBooks: (data) => dispatch(getGuestBooks(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(GuestBookList)
const styles = StyleSheet.create({
    row: {justifyContent: 'space-between', flexDirection: 'row', marginTop: 5},
    noRecords: {justifyContent: 'center', alignItems: 'center', marginTop: 10},
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
    },
    input: {
        margin: 5,
        minWidth: 100,
        height: 25,
        padding: 1, borderWidth: 1, borderColor: 'black', marginBottom: 1
    },
    item: {padding: 10, elevation: 1, margin: 5, backgroundColor: '#fff'},
    smallLablel: {marginTop: 10, color: 'black'}
})

const pickerStyle = StyleSheet.create({
        inputIOS: {
            color: colors.primary,
            margin: 3,

            minWidth: 100,
            padding: 3,
            borderColor: '#000',
            borderWidth: 0.5,
            marginBottom: 5
        },
        inputAndroid: {
            color: '#000',
            margin: 5,
            padding: 3,
            minWidth: 100,

            borderColor: '#000',
            borderWidth: 0.5,
            marginBottom: 3
        },
    }
)

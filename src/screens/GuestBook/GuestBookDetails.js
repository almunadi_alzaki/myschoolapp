import React from 'react';
import {Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import I18n from "../../i18n";
import { connect } from "react-redux";

import { colors } from "../../styles/LoginStyles";
import StyleSheetFactory from '../../styles/Ar_loginStyles.js';
import { statusTypes, visitorTypes } from "./AddGuestBook";
import {Icon} from "react-native-elements";

class GuestBookDetails extends React.Component {
	constructor(props) {
    super(props);
    console.log(" in const login screen+ " + this.props.locale.locale)
    let isRTL = this.props.locale.locale == "ar" ? true : false;
    this.state = { isRTL: isRTL };
  }
  state = {};
    static navigationOptions = ({navigation}) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{flexDirection: 'row'}}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{marginRight: 10, marginLeft: 10,}}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{alignSelf: 'center'}} color="#fff"
                              name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"}/>
                    </TouchableOpacity>
                </View>
            )
        };
    };


    render() {
	  console.log("State Language..",this.state.isRTL);
	styles = StyleSheetFactory.getSheet(this.state.isRTL);
    const {
      statusId, specialist, visitor_notes, reminder_date, visitor_type, visitor_date_en, visitor_name,
    } = this.props.navigation.getParam('guestBook', {});
    const status = statusTypes.find(p => p.value === statusId);
    const visitor = visitorTypes.find(p => p.value === visitor_type);
    return (
        <ScrollView style={styles.container_guest}>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Visitor Name')}: </Text>
            <Text style={styles.value}>{visitor_name}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Visitor Date')}: </Text>
            <Text style={styles.value}>{visitor_date_en}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Type')}: </Text>
            <Text style={styles.value}>{status.label}</Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Specialist')}: </Text>
            <Text style={styles.value}>{specialist}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Notes')}: </Text>
            <Text style={styles.value}>{visitor_notes}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Visitor Status')}: </Text>
            <Text style={styles.value}>{visitor.label}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>{I18n.t('Reminder Date')}: </Text>
            <Text style={styles.value}>{reminder_date}</Text>
          </View>
        </ScrollView>
    );
  }
}

const mapStateToProps = (state) => {  return {
    locale: state.locale

  }}

const mapDispatchToProps = dispatch => ({});


export default connect(mapStateToProps, mapDispatchToProps)(GuestBookDetails)
const styles1 = StyleSheet.create({
  container: {flex: 1, padding: 10},

});

import React from 'react';
import { Alert, Platform, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import I18n from "../../i18n";
import { connect } from "react-redux";
import MultiSelect from 'react-native-multiple-select';
import { RadioButton } from 'react-native-paper';
import { addGuestBook } from "../../actions/guest-book.action";
import { getTeachers } from "../../actions/general.action";
import { Button, CheckBox, Icon } from "react-native-elements";
import { colors } from "../../styles/LoginStyles";
import { setLocale } from '../../actions/actions';
import { DatePicker } from "../../components/Datepicker";
import moment from "moment";

export const visitorTypes = [{ label: 'Supervisior', value: 1, label_ar: 'مشرف/ة' }, {
    label: 'Parent ',
    value: 2,
    label_ar: 'ولي أمر'
}, {
    label: 'Others',
    value: 3,
    label_ar: 'أخرى'
}];
export const statusTypesEn = [{
    label: 'Under Processing',
    value: 3,
    label_ar: 'تحت الإجراء'
}, {
    label: 'Done',
    value: 2,
    label_ar: 'تم الإجراء',
}, { label: 'Nothing', value: 1, label_ar: 'لاشيء' }];

export const statusTypes = [{ label: 'لاشيء', value: 1 }, {
    label: 'تم الإجراء',
    value: 2
}, {
    label: 'تحت الإجراء',
    value: 3
}];

class AddGuestBook extends React.Component {
    constructor(props) {
        super(props);
        //console.log("dsdsdsdsd",I18nManager.isRTL);
        let isRTL = this.props.locale.locale == "ar" ? true : false;
        this.state = {
            isRTL: isRTL, add: false,
            remindMeApp: false,
            remindMeSMS: false,
            schoolId: '',
            specialist: '',
            visitornotes: '',
            reminderdate: moment().format('YYYY-MM-DD'),
            visitortype: 2,
            typechecked: 2,
            statusId: 3,
            statuschecked: 3,
            visitordate: moment().format('YYYY-MM-DD'),
            visitorname: '',
            teachernamecheck: ''
        };
        // I18n.locale = this.state.locale;
    }

    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };


    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.result === 'success' && prevState.add) {
            Alert.alert('Success', 'Success');
            nextProps.navigation.pop();
            return {
                add: false,
                remindMeApp: false,
                remindMeSMS: false,
                schoolId: '',
                specialist: '',
                visitornotes: '',
                reminderdate: moment().format('YYYY-MM-DD'),
                visitortype: 2,
                typechecked: 2,
                statusId: 3,
                statuschecked: 3,
                visitordate: moment().format('YYYY-MM-DD'),
                visitorname: '',
                teachernamecheck: ''
            }
        } else if (nextProps.result === 'error' && prevState.add) {
            Alert.alert('Error', 'Please check data');
            return { add: false };
        }
        if (!prevState.schoolId && nextProps.pageScreen === 'parent' && nextProps.students.length) {
            const schoolId = nextProps.students[0].school_id;
            nextProps.getTeachers(schoolId);
            return { schoolId }
        } else if (!prevState.schoolId && nextProps.staffs.length) {
            const schoolId = nextProps.staffs[0].schoolId;
            nextProps.getTeachers(schoolId);
            return { schoolId }
        }
        return null
    }

    addPressHandler = () => {
        const { statusId, schoolId, specialist, visitornotes, reminderdate, visitortype, visitordate, visitorname, teachernamecheck } = this.state;
        this.props.addGuestBook({
            statusId,
            schoolId,
            specialist,
            visitornotes,
            reminderdate,
            visitortype: visitortype || 0,
            visitordate,
            visitorname,
            teachernamecheck
        });
        this.setState({ add: true })
    }

    visitorTypeOptions() {
        return visitorTypes.map((visitor_type, key) => {
            return (
                <View style={{ flexDirection: 'row', borderWidth: 1 }}>
                    <View style={{ textAlign: this.state.isRTL ? 'left' : 'right', width: 30 }}>
                        <RadioButton
                            value="eng"
                            status={this.state.typechecked === visitor_type.value ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({
                                visitortype: visitor_type.value,
                                typechecked: visitor_type.value
                            })}

                        />

                    </View>

                    <View style={{ height: 30, }}>
                        <Text style={{
                            color: 'black',
                            paddingRight: 5,
                            paddingLeft: 5,
                            fontSize: 14,
                            textAlign: this.state.isRTL ? 'left' : 'right',
                            marginTop: 5
                        }}>{this.props.locale.locale === 'ar' ? visitor_type.label_ar : visitor_type.label} </Text>

                    </View>

                </View>
            );
        });


    }


    statusTypeOptions() {


        return statusTypesEn.map((status_type, key) => {
            return (
                <View style={{ flexDirection: 'row', }}>
                    <View style={{ textAlign: this.props.locale.locale === 'ar' ? 'left' : 'right', width: 30 }}>
                        <RadioButton
                            value="eng"
                            status={this.state.statuschecked === status_type.value ? 'checked' : 'unchecked'}
                            onPress={() => this.setState({
                                statusId: status_type.value,
                                statuschecked: status_type.value
                            })}
                        />

                    </View>

                    <View style={{ height: 30, }}>

                        <Text style={{
                            color: 'black',
                            paddingRight: 5,
                            paddingLeft: 5,
                            fontSize: 14,
                            textAlign: this.state.isRTL ? 'left' : 'right',
                            marginTop: 5
                        }}>{this.props.locale.locale === 'ar' ? status_type.label_ar : status_type.label} </Text>

                    </View>

                </View>
            );
        });


    }

    render() {
        const { statusId, remindMeApp, remindMeSMS, specialist, visitornotes, reminderdate, visitortype, visitordate, visitorname, teachernamecheck } = this.state;
        console.log(this.state);
        return (
            <ScrollView style={styles.container}>
                <DatePicker
                    style={styles.input}
                    placeholder={I18n.t('Visitor Date')}
                    value={visitordate}
                    onChange={(_, visitordate) => this.setState({ visitordate })}
                />
                <TextInput style={styles.input}
                    onChangeText={(visitorname) => this.setState({ visitorname })}
                    value={visitorname}
                    placeholder={I18n.t('Visitor Name')} />

                <View style={{
                    flexDirection: this.state.isRTL ? 'row' : 'row-reverse',
                }}>
                    {this.visitorTypeOptions()}
                </View>

                <TextInput style={styles.input}
                    onChangeText={(specialist) => this.setState({ specialist })}
                    value={specialist}
                    placeholder={I18n.t('Specialist')} />
                <TextInput style={styles.input}
                    onChangeText={(visitornotes) => this.setState({ visitornotes })}
                    value={visitornotes}
                    placeholder={I18n.t('Notes')} />
                <View style={{
                    flexDirection: this.state.isRTL ? 'row' : 'row-reverse',
                }}>
                    {this.statusTypeOptions()}
                </View>


                <CheckBox checked={remindMeApp} onPress={() => this.setState({ remindMeApp: !remindMeApp })}
                    title={I18n.t('Remind Me via App')} />
                <CheckBox checked={remindMeSMS} onPress={() => this.setState({ remindMeSMS: !remindMeSMS })}
                    title={I18n.t('Remind Me via SMS')} />

                <View pointerEvents={remindMeApp || remindMeSMS ? 'auto' : 'none'}>
                    <DatePicker
                        style={styles.input}
                        placeholder={I18n.t('Visitor Date')}
                        value={reminderdate}
                        onChange={(_, reminderdate) => this.setState({ reminderdate })}
                        disabled={true}
                    />

                    <MultiSelect
                        hideTags
                        items={this.props.teachers}
                        uniqueKey="teacherId"
                        styleInputGroup={{ backgroundColor: 'transparent' }}
                        fixedHeight={false}
                        styleDropdownMenuSubsection={{ paddingTop: 0, paddingBottom: 0, borderBottomWidth: 0 }}
                        styleDropdownMenu={[styles.input, { height: undefined }]}
                        onSelectedItemsChange={teachernamecheck => this.setState({ teachernamecheck })}
                        selectedItems={teachernamecheck}
                        selectText={I18n.t('Select teachers')}
                        searchInputPlaceholderText="Search Teachers..."
                        selectedItemTextColor="#CC484A"
                        selectedItemIconColor="#CC484A"
                        itemTextColor="#000"
                        displayKey="teachername"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#CCC"
                        submitButtonText="Submit"
                    />
                </View>
                <Button title={I18n.t('Add')} type={"solid"} onPress={() => this.addPressHandler()}
                    containerStyle={{ marginBottom: 20 }} />
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) => ({
    teachers: state.general.teachers,
    students: state.student.students,
    staffs: state.staff.staffs,
    locale: state.locale.locale,
    pageScreen: state.general.pageScreen,
    result: state.guestBooks.result,
    isRTL: state.isRTL,
})

const mapDispatchToProps = dispatch => ({
    addGuestBook: (data) => dispatch(addGuestBook(data)),
    getTeachers: (schoolId) => dispatch(getTeachers(schoolId)),
    setLocale: (locale) => dispatch(setLocale(locale))
});


export default connect(mapStateToProps, mapDispatchToProps)(AddGuestBook)
const styles = StyleSheet.create({
    container: { flex: 1, padding: 10, },
    input: { margin: 5, padding: 10, borderWidth: 0.5, borderColor: colors.primary, marginBottom: 10 },
})

export const pickerStyle = StyleSheet.create({
    inputIOS: {
        color: colors.primary,
        margin: 5,
        padding: 10,
        borderColor: colors.primary,
        borderWidth: 0.5,
        marginBottom: 10
    },
    inputAndroid: {
        color: colors.primary,
        margin: 5,
        padding: 10,
        borderColor: colors.primary,
        borderWidth: 0.5,
        marginBottom: 10
    },
}
)

import React from 'react';
import { Alert, ScrollView, StyleSheet, TextInput } from 'react-native';
import I18n from "../../i18n";
import { connect } from "react-redux";
import MultiSelect from 'react-native-multiple-select';
import { RadioButton } from 'react-native-paper';
import { addGuestBook } from "../../actions/guest-book.action";
import { getTeachers } from "../../actions/general.action";
import Picker from 'react-native-picker-select';
import { Button, CheckBox, Icon } from "react-native-elements";
import { colors } from "../../styles/LoginStyles";
import { Datepicker } from "../../components/Datepicker";

export const visitorTypes = [{label: 'Supervisior', value: 1,label_ar:'مشرف'}, {label: 'Parent ', value: 2,label_ar:'ولي أمر'}, {
  label: 'Others',
  value: 3,
  label_ar:'أخرى'
}];
export const statusTypesEn = [{label: 'Nothing', value: 1,label_ar:'لاشيء'}, {
  label: 'Done',
  value: 2,
  label_ar:'تم الإجراء',
}, {
  label: 'Under Processing',
  value: 3,
  label_ar:'تحت الإجراء'
}];

export const statusTypes = [{label: 'لاشيء', value: 1}, {
  label: 'تم الإجراء',
  value: 2
}, {
  label: 'تحت الإجراء',
  value: 3
}];

class AddGuestBook extends React.Component {
  constructor(props) {
    super(props);
    //console.log("dsdsdsdsd",I18nManager.isRTL);
    let isRTL = this.props.locale.locale == "ar" ? true : false;
    this.state = {isRTL: isRTL,typechecked: 1};
    I18n.locale = this.state.locale;
  }

  state = {};
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: I18n.t('AddGuestBook'),
    };
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.result === 'success' && prevState.add) {
      Alert.alert('Success', 'Success');
      nextProps.navigation.pop();
      return {
        add: false, statusId: '',
        remindMe: false,
        schoolId: '',
        specialist: '',
        visitornotes: '',
        reminderdate: '',
        visitortype: '',
        visitordate: '',
        visitorname: '',
        teachernamecheck: ''
      }
    }
    if (nextProps.students.length) {
      if (prevState.student) {
        return null;
      }
      const student = nextProps.students[0];
      const {school_id} = student;
      nextProps.getTeachers(school_id);
      return {student, schoolId: school_id}
    }
    return null
  }

  addPressHandler = () => {
    const {statusId, schoolId, specialist, visitornotes, reminderdate, visitortype, visitordate, visitorname, teachernamecheck} = this.state;
    this.props.addGuestBook({
      statusId,
      schoolId,
      specialist,
      visitornotes,
      reminderdate,
      visitortype,
      visitordate,
      visitorname,
      teachernamecheck
    });
    this.setState({add: true})
  }


  render() {
    const {statusId, remindMe, specialist, visitornotes, reminderdate, visitortype, visitordate, visitorname, teachernamecheck} = this.state;
    return (
        <ScrollView style={styles.container}>
		 <Datepicker
              style={styles.input}
              placeholder={"Visitor Date"}
              value={visitordate}
              onChange={(visitordate) => this.setState({visitordate})}
          />
          <TextInput style={styles.input}
                     onChangeText={(visitorname) => this.setState({visitorname})}
                     value={visitorname}
                     placeholder={'visitor name'}/>
         
          <Picker onValueChange={statusId => this.setState({statusId})}
                  value={statusId}
                  useNativeAndroidPickerStyle={false}
                  placeholder={{label: 'Type', value: ''}}
                  style={pickerStyle}
                  Icon={() => <Icon containerStyle={{left: '-15%', top: '65%'}} name={'arrow-drop-down'}/>}
                  items={statusTypes}/>

          <TextInput style={styles.input}
                     onChangeText={(specialist) => this.setState({specialist})}
                     value={specialist}
                     placeholder={'Specialist'}/>
          <TextInput style={styles.input}
                     onChangeText={(visitornotes) => this.setState({visitornotes})}
                     value={visitornotes}
                     placeholder={'Notes'}/>
          <Picker onValueChange={visitortype => this.setState({visitortype})}
                  value={visitortype}
                  useNativeAndroidPickerStyle={false}
                  placeholder={{label: 'Visitor Type', value: ''}}
                  style={pickerStyle}
                  Icon={() => <Icon containerStyle={{left: '-15%', top: '65%'}} name={'arrow-drop-down'}/>}
                  items={visitorTypes}/>

          <CheckBox checked={remindMe} onPress={() => this.setState({remindMe: !remindMe})} title={'Remind Me'}/>
          <Datepicker
              style={styles.input}
              placeholder={"Reminder Date"}
              value={reminderdate}
              onChange={(reminderdate) => this.setState({reminderdate})}
          />
          <MultiSelect
              hideTags
              items={this.props.teachers}
              uniqueKey="teacherId"
              styleInputGroup={{backgroundColor: 'transparent'}}
              fixedHeight={false}
              styleDropdownMenuSubsection={{paddingTop: 0, paddingBottom: 0, borderBottomWidth: 0}}
              styleDropdownMenu={[styles.input, {height: undefined}]}
              onSelectedItemsChange={teachernamecheck => this.setState({teachernamecheck})}
              selectedItems={teachernamecheck}
              selectText="Select Teachers"
              searchInputPlaceholderText="Search Teachers..."
              selectedItemTextColor="#CCC"
              selectedItemIconColor="#CCC"
              itemTextColor="#000"
              displayKey="teachername"
              searchInputStyle={{color: '#CCC'}}
              submitButtonColor="#CCC"
              submitButtonText="Submit"
          />
          <Button title={'Add'} type={"solid"} onPress={this.addPressHandler} containerStyle={{marginBottom: 20}}/>
        </ScrollView>
    );
  }
}

const mapStateToProps = (state) => ({
  locale: state.locale,
  teachers: state.general.teachers,
  students: state.student.students,
  result: state.guestBooks.result,
})

const mapDispatchToProps = dispatch => ({
  addGuestBook: (data) => dispatch(addGuestBook(data)),
  getTeachers: (schoolId) => dispatch(getTeachers(schoolId)),
});


export default connect(mapStateToProps, mapDispatchToProps)(AddGuestBook)
const styles = StyleSheet.create({
  container: {flex: 1, padding: 10,},
  input: {margin: 5, padding: 10, borderWidth: 0.5, borderColor: colors.primary, marginBottom: 10},
})

export const pickerStyle = StyleSheet.create({
      inputIOS: {
        color: colors.primary,
        margin: 5,
        padding: 10,
        borderColor: colors.primary,
        borderWidth: 0.5,
        marginBottom: 10
      },
      inputAndroid: {
        color: colors.primary,
        margin: 5,
        padding: 10,
        borderColor: colors.primary,
        borderWidth: 0.5,
        marginBottom: 10
      },
    }
)

import React from 'react';
import {Image, Linking, ScrollView, Text, View} from 'react-native';
import StyleSheetFactory from '../styles/Ar_loginStyles.js';
import PropTypes from 'prop-types';
import {NavigationActions} from 'react-navigation';
import {getLocale} from '../actions/actions';
import {connect} from 'react-redux';
import {getRightMenu} from "../actions/general.action";
import I18n from "../i18n";


const MENU_ITEMS = {
	'Teacher\'s Timetable': {icon: require('../imagesicon/timetable.png'), route: 'TimeTable'},
	'Academic Calendar': {icon: require('../imagesicon/acadamic-calendar.png'), route: 'AcademicCalendar'},
	'Employee Phones': {icon: require('../imagesicon/employee-phone.png'), route: 'EmployeePhones'},
	'Event Calendar': {icon: require('../imagesicon/eventcalendar.png'), route: 'event'},
	'Full Timetable': {icon: require('../imagesicon/timetable.png'), route: 'FullTimeTable'},
	'Time Table': {icon: require('../imagesicon/timetable.png'), route: 'TimeTable'},
	'Guest Visitor': {icon: require('../imagesicon/guest-book.png'), route: 'GuestBookList'},
	// 'Teacher Visit': {icon: require('../imagesicon/teacher.png'), route: 'TeacherVisit'},
}
const guestBookMenuItem = {
	menuId: 'guestbook',
	menu_name_en: 'Guest Visitor',
	menu_name_ar: 'سجل الزوار'
};


const fullTimeTableMenuItem = {
	menuId: 'fullTimeTable',
	menu_name_en: 'Full TimeTable',
	menu_name_ar: 'هواتف الموظفين'
};

class RightMenuScreen extends React.Component {
	constructor(props) {
		super(props);
		let isRTL = this.props.locale.locale == "ar" ? true : false;
		this.state = {isRTL: isRTL};
	}

	componentDidMount() {
		getLocale();
	}

	navigateToScreen = (route) => () => {
		const navigateAction = NavigationActions.navigate({
			routeName: route
		});
		this.props.navigation.navigate(route);
	}

	renderMenuItem = (menuItem) => {
		const menu = MENU_ITEMS[menuItem.menu_name_en] || {};
		return (
			<View style={styles.navSectionStyle} key={menuItem.menuId}>
				<Image style={styles.navMenuIcon}
				       source={!menuItem.page_link ? menu.icon : {uri: 'http://nenewe.com' + menuItem.icon_link}}/>
				<Text style={styles.navItemStyle}
				      onPress={menuItem.page_link ? () => this.openLink(menuItem.page_link) : this.navigateToScreen(menu.route)}>
					{this.props.locale.locale === 'en' ? menuItem.menu_name_en : menuItem.menu_name_ar}
				</Text>
			</View>
		)
	}

	openLink = (url) => {
		Linking.canOpenURL(url)
			.then((supported) => {
				if (supported) {
					return Linking.openURL(url);
				}
			})
			.catch((err) => console.error('An error occurred', err));
	}

	render() {
		styles = StyleSheetFactory.getSheet(this.props.locale.isRTL);
		return (
			<View style={styles.leftMenuContainer}>
				<ScrollView>
					<View>
						<Text style={styles.sectionHeadingStyle}>
							{I18n.t('Settings')}
						</Text>
						{this.props.rightMenus.map(this.renderMenuItem)}
					</View>
				</ScrollView>
			</View>
		);
	}
}

RightMenuScreen.propTypes = {
	navigation: PropTypes.object
};

const mapStateToProps = (state) => {
	return {
		locale: state.locale,
		isRTL: state.isRTL,
		students: state.student.students,
		staffs: state.staff.staffs,
		rightMenus: state.general.rightMenus,
		pageScreen: state.general.pageScreen,
	}
}

const mapDispatchToProps = dispatch => ({
	getLocale: () => dispatch(getLocale()),
	getRightMenu: (data) => dispatch(getRightMenu(data))
})

export default connect(mapStateToProps,
	mapDispatchToProps)(RightMenuScreen)

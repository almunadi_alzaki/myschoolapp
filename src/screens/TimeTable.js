import React from 'react';
import { Alert, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import styles, { colors } from '../styles/LoginStyles';
import I18n, { strings } from "../i18n";
import { connect } from "react-redux";
import { getTeachers, getTimeTable, getTimeTableByTeacher } from "../actions/general.action";
import { fetchFilterClassesValues, fetchFilterDivsValues, getAllDivsLocal } from "../actions/staffactions";
import Picker from 'react-native-picker-select';
import { Icon } from "react-native-elements";


class TimeTable extends React.Component {
    state = {
        schools: [],
        selectedTeacher: '', selectedClass: '', selectedDiv: ''
    }
    static navigationOptions = ({ navigation }) => {
        return Platform.OS === 'ios' && {
            headerLeft: (
                <View style={{ flexDirection: 'row' }}>
                    {/*<LeftDrawerButton navigation={navigation}/>*/}
                    <TouchableOpacity
                        style={{ marginRight: 10, marginLeft: 10, }}
                        onPress={() => navigation.pop()}
                    >
                        <Icon size={24} style={{ alignSelf: 'center' }} color="#fff"
                            name={I18n.locale === 'ar' ? "arrow-forward" : "arrow-back"} />
                    </TouchableOpacity>
                </View>
            )
        };
    };

    componentDidMount() {

    }


    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.staffs.length && nextProps.pageScreen === 'staff') {
            if (prevState.staff) {
                return null;
            }
            // const staff = nextProps.staffs[0];
            const staff = nextProps.staffs.find(f => f.schoolId === nextProps.staff);
            nextProps.getTimeTableByTeacher({ schoolId: staff.schoolId, teacherId: staff.teacherId });
            return { staff }
        } else if (nextProps.students.length) {
            if (prevState.student) {
                return null;
            }
            const student = nextProps.students[0];
            const { school_id, div } = student;
            nextProps.getTimeTable({ schoolId: school_id, class: student.class, div });
            const schools = [...new Set(nextProps.students.map(p => p.school_id))];
            return { student, selected: student.id, students: nextProps.students, schools }
        }
        return null
    }

    schoolChangeHandler = (value) => {
        const student = this.state.students.find(s => s.id === value);
        this.setState({ selected: student.id });
        if (student) {
            this.props.getTimeTable({ schoolId: student.school_id, class: student.class, div: student.div });
        }
    };

    divChangeHandler = (selectedDiv) => {
        this.setState(s => ({ selectedDiv, selectedTeacher: selectedDiv ? '' : s.selectedTeacher }))
    };
    classChangeHandler = (selectedClass) => {
        this.setState(s => ({ selectedClass, selectedTeacher: selectedClass ? '' : s.selectedTeacher }))
        //console.log("selected class..",selectedClass.classId);
        this.props.getAllDivsLocal(selectedClass);
    };
    teacherChangeHandler = (selectedTeacher) => {
        this.setState(s => ({
            selectedTeacher,
            selectedClass: selectedTeacher ? '' : s.selectedClass,
            selectedDiv: selectedTeacher ? '' : s.selectedDiv
        }))
    };
    searchPressHandler = () => {
        const { staff, selectedTeacher, selectedClass, selectedDiv } = this.state;
        if (selectedTeacher) {
            this.props.getTimeTableByTeacher({ schoolId: staff.schoolId, teacherId: selectedTeacher });
        } else {
            if (!selectedClass || !selectedDiv) {
                Alert.alert('Error', 'Please select class and div.')
            }
            this.props.getTimeTable({ schoolId: staff.schoolId, class: selectedClass, div: selectedDiv });
        }
    }

    render() {
        const { timeTable, staffs, divs, classes, teachers } = this.props;
        const { students, student, selected, staff, selectedTeacher, selectedClass, selectedDiv } = this.state;
        return (
            <View style={{ flex: 1 }}>
                {student && <Picker onValueChange={this.schoolChangeHandler}
                    style={{ inputIOS: { color: colors.primary }, inputAndroid: { color: colors.primary } }}
                    value={selected}
                    items={students.map((p, i) => ({
                        label: `(${p.class_shortcut}/${p.div}) ${p.st_name}`,
                        value: p.id
                    }))} />}
                <View style={{ flex: 0, flexDirection: 'row', borderWidth: 1, borderBottomColor: 'black', }}>
                    <View style={styles.tableGrid10}>
                        <View style={styles.boxCenterDiv}>
                            <Text style={{ color: 'black', fontSize: 12 }}></Text>
                        </View>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('sun')}</Text>

                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('mon')}</Text>

                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('tue')}</Text>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('wed')}</Text>
                    </View>
                    <View style={styles.tableGrid18}>
                        <Text style={styles.gridTitle}>{strings('thu')}</Text>
                    </View>
                </View>
                <ScrollView style={{ flex: 1 }}>
                    {timeTable && timeTable.map((t, index) => (
                        <View key={index}
                            style={{ flex: 0, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'black' }}>
                            <View style={styles.tableGrid10}>
                                <View style={styles.boxCenterDiv}>
                                    <Text style={{ color: 'black', fontSize: 12 }}>{index + 1}</Text>
                                </View>
                            </View>
                            {t.map((d, i) => (
                                <View key={i}
                                    style={i % 2 == 0 ? styles.tableGrid18Yellow : styles.tableGrid18Greys}>
                                    {staff ? staff.levelid != 17 ?
                                        <React.Fragment>
                                            <Text style={styles.tableText}>{d.classdiv || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sub_shortcut || '-'}</Text>
                                        </React.Fragment>
                                        :
                                        <React.Fragment>
                                            <Text style={styles.tableText}>{d.room_no || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sub_shortcut || '-'}</Text>
                                            <Text style={styles.tableText}>{d.sec_arabic || '-'}</Text>
                                        </React.Fragment> :
                                        <Text style={styles.tableText}>{d.sub_shortcut || '-'}</Text>}
                                </View>
                            ))}
                        </View>
                    ))}
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({

    timeTable: state.general.timeTable,
    pageScreen: state.general.pageScreen,
    teachers: state.general.teachers,
    students: state.student.students,
    staffs: state.staff.staffs,
    staff: state.staff.schoolId,
    divs: state.staff.divs,
    classes: state.staff.classes,
});

const mapDispatchToProps = dispatch => ({
    getTimeTable: (data) => dispatch(getTimeTable(data)),
    getTimeTableByTeacher: (data) => dispatch(getTimeTableByTeacher(data)),
    getTeachers: (schoolId) => dispatch(getTeachers(schoolId)),
    getClasses: (schoolId) => dispatch(fetchFilterClassesValues(schoolId)),
    getAllDivsLocal: (classId) => dispatch(getAllDivsLocal(classId)),
    getDivs: (schoolId) => dispatch(fetchFilterDivsValues(schoolId))
});
const pickerStyle = StyleSheet.create({
    inputIOS: { color: colors.primary, margin: 5, padding: 5, borderWidth: 0.5, width: 80 },
    inputAndroid: { color: colors.primary, margin: 5, padding: 5, borderWidth: 0.5, width: 80 },
}
)
export default connect(mapStateToProps, mapDispatchToProps)(TimeTable)

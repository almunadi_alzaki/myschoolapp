import {StyleSheet, Dimensions, I18nManager} from 'react-native';
const {width} = Dimensions.get('window');
export default class StyleSheetFactory {
  static getSheet(isRTL) {
    return StyleSheet.create({
      container: {
        backgroundColor: '#fff',
        position: 'relative',
        width: '100%',
        height: '100%',
      },
      container_guest: {flex: 1, padding: 10},
      headerTops: {
        width: '100%',
        backgroundColor: '#5f021f',
        // top:20,
        color: 'white',
        position: 'absolute',
        textAlign: 'center',
        padding: 10,
        zIndex: 99999,
      },
      imageCenter: {
        width: '100%',
        textAlign: 'center',
        padding: 10,
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
      },

      FlexCenter: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
      },

      LogininputIcon: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
        width: 48,
      },

      FooterImage: {
        height: 20,
        width: 20,
      },
      FooterImageStaff: {
        height: 20,
        width: 20,
      },
      FooterBottom: {
        // width: "30%",
        // bottom: 0,
        // position: 'absolute',
        // textAlign: 'center',
        color: 'white',
        backgroundColor: '#5f021f',
        width: '33%',
        height: 50,
        flex: 1,
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 10,
      },
      ColSet50: {
        width: '50%',
        height: 50,
        backgroundColor: '#5f021f',
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
      },
      containerCenter: {
        backgroundColor: '#fff',
        // textAlign: 'center',
        // position: 'absolute',
        // top: 5,
        // bottom: 60,
        // width: "100%",
        // height: "100%",
      },
      imageLogo: {
        height: 60,
        width: 60,
      },
      inputSet: {
        height: 40,
        width: width - 90,
        padding: 10,
        fontSize: 14,
      },
      buttonRed: {
        backgroundColor: 'red',
        width: '100%',
      },
      input: {
        backgroundColor: 'white',
        width: '90%',
        padding: 10,
      },
      text: {
        padding: 10,
        fontSize: 20,
      },
      ColSet5: {
        width: '5%',
      },
      ColSet10: {
        width: '10%',
      },
      ColSet15: {
        width: '15%',
        alignItems: 'flex-start',
      },
      ColSet20: {
        width: '20%',
      },
      ColSet30: {
        width: '30%',
      },
      ColSet33: {
        width: '33%',
      },
      ColSet40: {
        width: '40%',
      },
      ColSet50: {
        width: '50%',
      },
      ColSet55: {
        width: '55%',
      },
      ColSet60: {
        width: '60%',
      },
      ColSet70: {
        width: '70%',
      },
      ColSet80: {
        width: '80%',
      },
      ColSet85: {
        width: '85%',
      },
      ColSet90: {
        width: '90%',
      },

      ColSetMsg: {
        flex: 0,

        alignItems: 'flex-start',
      },

      ColSetHeighlight: {
        backgroundColor: 'darkorange',
        borderRadius: 10,
      },

      iconSize: {
        height: 30,
        width: 30,
      },

      iconSizeLock: {
        height: 10,
        width: 10,
      },

      iconSizecall: {
        height: 28,
        width: 28,
      },
      iconSizemail: {
        height: 28,
        width: 28,
      },
      CallBg: {
        backgroundColor: 'green',
        width: '15%',
      },

      leftMenuContainer: {
        //marginTop:10
        flex: 1,
        flexDirection: 'row',
      },

      sectionHeadingStyle: {
        height: 40,
        backgroundColor: '#5f021f',
        //marginTop:15,
        color: '#ffff',
        lineHeight: 40,
        fontSize: 18,
        paddingLeft: 5,
        paddingRight: 5,
      },
      navSectionStyle: {
        flex: 1,
        flexDirection: 'row',
        height: 50,
        textAlign: 'right',
        paddingLeft: 10,
        paddingRight: 10,
      },
      navMenuIcon: {
        paddingTop: 10,
        marginTop: 15,
        marginLeft: 5,
        width: 30,
        height: 30,
      },
      navItemStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 12,
        lineHeight: 35,
        color: 'black',
        fontWeight: 'bold',
        fontSize: 14,
      },
      footerMenuBtn: {
        marginLeft: '1%',
      },
      fontSizeText: {
        color: 'black',
        fontSize: 16,
        textAlign: 'left',
        fontWeight: 'bold',
      },
      textView: {width: 25, height: 70, margin: 1},
      textRotat: {
        display: 'flex',
        transform: [{rotate: '90deg'}, {translateX: 20}, {translateY: 23}],
        paddingLeft: 10,
        fontSize: 16,
        alignItems: 'center',
        width: 70,
        height: 25,
        color: '#5f021f',
        fontWeight: 'bold',
        backgroundColor: '#ebebeb',
      },

      textRotatAr: {
        display: 'flex',
        transform: [{rotate: '270deg'}, {translateX: -20}, {translateY: 23}],
        paddingLeft: 10,
        fontSize: 16,
        alignItems: 'center',
        textAlign: 'center',
        width: 70,
        height: 25,
        color: '#5f021f',
        fontWeight: 'bold',
        backgroundColor: '#ebebeb',
      },

      boxCenterDiv: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
      },
      textAlignDiv: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
      },

      BorderBox: {
        borderColor: 'gray',
        borderWidth: 1,
        textAlign: 'center',
        height: 25,
        width: 25,
        backgroundColor: '#ebebeb',
      },

      tableGrid18: {
        width: '18.8%',
        textAlign: 'center',
        borderColor: 'gray',
        borderRightWidth: 1,
      },

      tableGrid18Yellow: {
        width: '18.8%',
        textAlign: 'center',
        borderColor: 'gray',
        borderRightWidth: 1,
        backgroundColor: '#fff1ce',
        paddingBottom: 4,
        paddingTop: 4,
      },

      tableGrid18Greys: {
        width: '18.8%',
        textAlign: 'center',
        borderColor: 'gray',
        borderRightWidth: 1,
        backgroundColor: '#fefffc',
        paddingBottom: 4,
        paddingTop: 4,
      },

      tableGrid10: {
        width: '6%',
        textAlign: 'center',
        borderColor: 'gray',
        borderRightWidth: 1,
        backgroundColor: '#e1e1e1',
      },

      tableText: {
        color: 'black',
        fontSize: 13,
        textAlign: 'center',
      },
      gridTitle: {
        color: 'black',
        fontSize: 18,
        textAlign: 'center',
        paddingBottom: 6,
        paddingTop: 6,
        backgroundColor: '#e1e1e1',
      },

      bgnumber: {
        backgroundColor: '#911610',
        color: 'white',
        paddingLeft: 6,
        paddingRight: 6,
        borderRadius: 20,
      },
      secretCol: {
        height: 30,
        width: 30,
        borderColor: 'gray',
        borderWidth: 1,
        textAlign: 'center',
        padding: 0,
      },

      staffMenuRightArrow: {
        marginLeft: 'auto',
        width: 20,
        height: 20,
      },
      rightArrowIconSize: {
        width: 18,
        height: 18,
      },
      staffMenuName: {
        textAlign: 'center',
        color: 'black',
        paddingTop: 3,
        fontSize: 16,
        fontWeight: 'bold',
      },
      filterHeader: {
        flex: 0,
        flexDirection: 'row',
        width: '100%',
        height: 30,
        marginBottom: 10,
        //backgroundColor:'#5f021f'
      },
      filterHeaderTop1: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        width: '32%',
        height: 40,
        backgroundColor: '#000080',
        marginLeft: 3,
        justifyContent: 'center',
      },
      filterHeaderTop2: {
        width: '32%',
        height: 40,
        backgroundColor: '#000080',
        marginLeft: 5,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
      },
      filterHeaderTop3: {
        flex: 1,
        flexDirection: 'row',
        width: '32%',
        height: 40,
        justifyContent: 'center',
        backgroundColor: '#000080',
        marginLeft: 5,
        marginRight: 2,
        alignItems: 'center',
      },
      filterHeadText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 14,
        fontWeight: 'bold',
      },
      filtericonSize: {
        margin: 5,
      },
      label: {fontSize: 16, fontWeight: '500', width: '50%', padding: 5},
      value: {fontSize: 16, width: '50%'},
      row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        borderBottomColor: '#eee',
      },
      input: {
        margin: 5,
        padding: 10,
        borderWidth: 0.5,
        borderColor: '#5f021f',
        marginBottom: 10,
      },
      footerTabMenu: {
        textAlign: 'center',
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
      },
      topStaffRow: {
        flexDirection: 'row',
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        width: '100%',
        height: 90,
        borderBottomWidth: 0.5,
        borderBottomColor: '#696969',
        marginBottom: 5,
      },
      staffMsgicon: {width: 30},
      imageStaffAdmin: {
        //width: "95%",
        textAlign: 'center',
        padding: 10,
        width: 80,
        height: 80,
        borderRadius: 40,

        borderWidth: 2,
        alignItems: 'center',
        //  display:'flex',
        // justifyContent:'center',
        // flex:1,
        //  borderWidth:1,
        borderColor: 'navy',
      },
      inputSchool: {
        padding: 3,
        width: 200,
        borderWidth: 0.5,
        borderColor: '#696969',
        height: 35,
      },
      staffMenusList: {
        height: 40,
        flex: 0,
        flexDirection: 'row',
        borderColor: '#696969',
        borderBottomWidth: 0.5,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 5,
        marginRight: 5,
      },
      menuIconSize: {
        height: 30,
        width: 30,
        borderWidth: 1,
        borderColor: '#000',
        borderRadius: 15,
        padding: 10,
        backgroundColor: '#fff',
      },
      userContainer: {
        paddingTop: 100,
        paddingLeft: 20,
      },
      userText: {
        width: '50%',
        color: 'white',
        backgroundColor: '#5f021f',
        fontSize: 22,
        marginLeft: 10,
      },
      selectSchool: {
        color: '#5f021f',
        fontSize: 22,
        fontWeight: 'bold',
        marginLeft: 10,
      },
    });
  }
}

import realm from '../realm/schema';
import {UpdateMode} from 'realm';

export const saveStaffContacts = (contacts, schoolId) => {
    let q = `schoolId='${schoolId}'`;
    const existing = realm.objects('StaffContacts')
        .filtered(q);
    realm.write(() => {
        realm.delete(existing);
        contacts.forEach((t) => {
            realm.create('StaffContacts', {
                teachername:t.teachername,
                schoolId,
                mobileNo:t.mobileNo
            }, UpdateMode.Never);
        });
    })

}
export const getSavedStaffContacts = (schoolId) => {
    let q = `schoolId='${schoolId}'`;
    return realm.objects('StaffContacts')
        .filtered(q);
}

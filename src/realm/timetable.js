import realm from '../realm/schema';
import { UpdateMode } from 'realm';
import { groupBy } from "../utils";
import { AsyncStorage, PermissionsAndroid } from "react-native";
import RNFS from 'react-native-fs';
import { asyncStorage } from 'reactotron-react-native';

export const saveTimeTable = (timetable, schoolId, teacherId) => {
    let q = `schoolId='${schoolId}'`;
    if (teacherId) {
        q += ` AND teacherId = "${teacherId}"`
    }
    const existing = realm.objects('TimeTable')
        .filtered(q);
    realm.write(() => {
        realm.delete(existing);
        timetable.forEach((t, row) => t.forEach(({ sub_shortcut }, day) => {
            realm.create('TimeTable', {
                sub_shortcut,
                class: classId,
                div,
                schoolId,
                teacherId,
                day,
                row
            }, UpdateMode.Never);
        }))
    })

}
export const getSavedTimeTable = (schoolId, teacherId) => {
    let q = `schoolId='${schoolId}'`;
    if (teacherId) {
        q += ` AND teacherId = "${teacherId}"`
    }
    const existing = realm.objects('TimeTable')
        .filtered(q);
    return Object.values(groupBy(existing.sorted(['row', 'day']), 'row'));
}


export const saveAcademicCalendar = (calendar) => {
    const name = 'academic-calendar.jpg';
    const path = `${RNFS.DocumentDirectoryPath}/${name}`;
    return RNFS.downloadFile({ fromUrl: calendar, toFile: path }).promise
        .then(response => {
            AsyncStorage.setItem('AcademicCalendar', 'file://' + path)
            return 'file://' + path;
        });
}
export const getSavedAcademicCalendar = () => {
    return AsyncStorage.getItem('AcademicCalendar')
}

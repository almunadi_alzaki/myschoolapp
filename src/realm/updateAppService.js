import realm from '../realm/schema';
//let realm = global.realm
let updateAppService = {
  saveClass: function (classes, schoolId) {
    console.log("Classes in service..", classes);
    classes.map((item, key) => {
      realm.write(() => {
        realm.create('Classes',
          {
            classId: item.classId,

            class_en: item.class_en,
            schoolId: schoolId,
            class_ar: item.class_ar,
            levelid: item.levelId,
            class_shortcut: item.class_shortcut,
            class_order: item.class_order,
            hifl_or_not: item.hifl_or_not
          });
      });
    });
  },
  saveDiv: function (divs, schoolId) {
    console.log("Divs in service..", divs);
    divs.map((item, key) => {
      realm.write(() => {
        realm.create('Divs',
          {
            classId: item.class,
            schoolId: schoolId,
            div: item.div
          });
      });


    });
  },


  saveAcadamicStudents: function (students) {
    students.map((item, key) => {
      realm.write(() => {
        realm.create('AcadamicStudents',
          {
            id: item.id,

            schoolId: item.school_id,
            st_name: item.st_name,
            classid: item.class,
            st_acadamy_no: item.st_acadamy_no,
            parent_phone: item.parent_phone,
            id_no: item.id_no,
            st_password: item.st_password,
            app_usage: item.app_usage,
            div: item.div

          });
      });


    });
  },

  saveMenuPermission: function (menupermissions) {
    menupermissions.map((permission, key) => {
      realm.write(() => {
        realm.create('StaffMenuPermission',
          {
            teacherId: permission.teacherId,
            mobileNo: permission.mobileNo,
            menu_id: permission.menu_id,
            full_or_own: permission.full_or_own,
            menustatus: permission.status,
            schoolId: permission.schoolId,
            menu_name_en: permission.menu_name_en,
            parent_id: permission.parent_id,
            menu_name_ar: permission.menu_name_ar,
            menu_link: permission.menu_link,
            menu_for: permission.menu_for,
            icon_label: permission.icon_label

          });
      });


    });
  },
  update: function (classes) {
    try {
      let msgUpdateId = obj.id;
      console.log("Update msg by Id....", msgUpdateId);
      let msgUpdate = realm.objects('Message').filtered(`id == "${msgUpdateId}"`);
      realm.write(() => {
        // realm.deleteAll()
        msgUpdate[0].read = true;
        //realm.create('StaffMessages',obj,true)
      })
    } catch (err) {
      console.log(err)
    }
  },



  delete: function (msg) {
    try {
      // var filtered = sample.filtered([2, 4, 7, 10].map((id) => 'id == ' + id).join(' OR '));
      let msgDelteId = msg.id;
      let msgDelte = realm.objects('Message').filtered(`id == "${msgDelteId}"`);
      realm.write(() => {
        // realm.deleteAll()
        realm.delete(msgDelte)
      })
    } catch (err) {
      console.log(err)
    }
  },
  deleteById: function (msg) {
    try {
      // var filtered = sample.filtered([2, 4, 7, 10].map((id) => 'id == ' + id).join(' OR '));

      let msgDelteId = msg.id;
      let msgDelte = realm.objects('Message').filtered(`id == "${msgDelteId}"`);
      //console.log("Delete general student message ",msgDelte);
      realm.write(() => {
        // realm.delete( realm.objects('Message'));
        realm.delete(msgDelte);
        console.log("Messages---------", realm.objects('Message').length);
      })
    } catch (err) {
      console.log(err)
    }
  },
};

module.exports = {
  updateAppService
};

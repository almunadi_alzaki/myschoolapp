import realm from '../realm/schema';
import { UpdateMode } from 'realm';

export const saveRightMenus = (rightMenus, schoolId, pageType, teacherId, page_link) => {
    let q = `schoolId='${schoolId}' AND page_type = '${pageType}'`;
    if (teacherId) {
        q += ` AND teacherId = '${teacherId}'`;
    }
    const existing = realm.objects('RightMenus')
        .filtered(q);
    realm.write(() => {
        realm.delete(existing);
        rightMenus.filter(p => p.menu_id && p.menu_name_en && p.menu_name_ar ).forEach((t) => {
            realm.create('RightMenus', {
                menuId: t.menu_id,
                menu_name_en: t.menu_name_en,
                menu_name_ar: t.menu_name_ar,
                page_type: pageType,
                schoolId,
                page_link,
            }, UpdateMode.Never);
        })
    })

}
export const getSavedRightMenus = (schoolId, pageType, teacherId) => {
    let q = `schoolId='${schoolId}' AND page_type = '${pageType}'`;
    if (teacherId) {
        q += ` AND teacherId = '${teacherId}'`;
    }
    return realm.objects('RightMenus')
        .filtered(q);
}

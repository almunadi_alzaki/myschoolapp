import Realm from 'realm';

class Student extends Realm.Object {
}

Student.schema = {
	name: 'Student',
	properties: {
		st_name: 'string',
		id: 'string',
		class: "string",
		div: "string",
		st_acadamy_no: "string",
		parent_phone: "string",
		id_no: "string",
		st_password: "string",
		school_id: "string",
		app_usage: "string",
		username: "string",
		classId: {type: "string", optional: true, default: null},
		class_en: {type: "string", optional: true, default: null},
		class_ar: {type: "string", optional: true, default: null},
		levelId: {type: "string", optional: true, default: null},
		class_shortcut: {type: "string", optional: true, default: null},
		class_order: {type: "string", optional: true, default: null}
	}
};

class Staff extends Realm.Object {
}

Staff.schema = {
	name: 'Staff',
	primaryKey: 'teacherId',
	properties: {
		teacherId: 'int',
		teacherName: {type: "string", optional: true, default: null},
		schoolName: {type: "string", optional: true, default: null},
		schoolId: {type: "string", optional: true, default: null},
		mobileNo: {type: "string", optional: true, default: null},
		t_password: {type: "string", optional: true, default: null},
		levelid: {type: "string", optional: true, default: null},
		logged_to: 'int',
		teacherType: 'int',
		permission: 'int',

	}
};

class Classes extends Realm.Object {
}

Classes.schema = {
	name: 'Classes',
	primaryKey: 'classId',
	properties: {
		classId: {type: "string", optional: true, default: null},
		schoolId: {type: "string", optional: true, default: null},
		class_en: {type: "string", optional: true, default: null},
		class_ar: {type: "string", optional: true, default: null},
		levelid: {type: "string", optional: true, default: null},
		class_shortcut: {type: "string", optional: true, default: null},
		class_order: {type: "string", optional: true, default: null},
		hifl_or_not: {type: "string", optional: true, default: null}

	}
};

class Divs extends Realm.Object {
}

Divs.schema = {
	name: 'Divs',
	properties: {
		classId: {type: "string", optional: true, default: null},
		schoolId: {type: "string", optional: true, default: null},
		div: {type: "string", optional: true, default: null}

	}
};

class AcadamicStudents extends Realm.Object {
}

AcadamicStudents.schema = {
	name: 'AcadamicStudents',
	primaryKey: 'id',
	properties: {
		id: {type: "string", optional: true, default: null},
			schoolId: {type: "string", optional: true, default: null},
		st_name: {type: "string", optional: true, default: null},
		classid: {type: "string", optional: true, default: null},
		div: {type: "string", optional: true, default: null},
		st_acadamy_no: {type: "string", optional: true, default: null},
		parent_phone: {type: "string", optional: true, default: null},
		id_no: {type: "string", optional: true, default: null},
		st_password: {type: "string", optional: true, default: null},
		app_usage: {type: "string", optional: true, default: null}
	}
};

class Subjects extends Realm.Object {
}

Subjects.schema = {
	name: 'Subjects',
	primaryKey: 'subId',
	properties: {
		subId: 'int',
		sub_name_en: {type: "string", optional: true, default: null},
		sub_name_ar: {type: "string", optional: true, default: null},
		sub_shortcut: {type: "string", optional: true, default: null},
		hifl_or_not: 'int',
		levelid: 'int',
		ordering: 'int',
		schoolId: {type: "string", optional: true, default: null}
	}
};

class Message extends Realm.Object {
}

Message.schema = {
	name: 'Message',
	primaryKey: 'id',
	properties: {
		id: 'string',
		message: {type: "string", optional: true, default: null},
		send_by: {type: "string", optional: true, default: null},
		class_id: {type: "string", optional: true, default: null},
		div: {type: "string", optional: true, default: null},
		description: {type: "string", optional: true, default: null},
		messageType: {type: "string", optional: true, default: null},
		read: {type: "bool", default: false},
		studentId: {type: "string", optional: true, default: null},
		st_acadamy_no: {type: "string", optional: true, default: null},
		schoolName: {type: "string", optional: true, default: null},
		date: {type: "string", optional: true, default: null},
	}
};

class StaffMessages extends Realm.Object {
}

StaffMessages.schema = {
	name: 'StaffMessages',
	primaryKey: 'id',
	properties: {
		id: 'string',
		message: {type: "string", optional: true, default: null},
		description: {type: "string", optional: true, default: null},
		messageType: {type: "string", optional: true, default: null},
		read: {type: "bool", default: false},
		teacherId: 'int?',
		schoolName: {type: "string", optional: true, default: null},
		date: {type: "string", optional: true, default: null},
	}
};

class StaffMenuPermission extends Realm.Object {
}

StaffMenuPermission.schema = {
	name: 'StaffMenuPermission',

	properties: {
		teacherId: 'string',
		mobileNo: {type: "string", optional: true, default: null},
		menu_id: 'string',
		full_or_own: {type: "string", optional: true, default: null},
		menustatus: 'string',
		schoolId: 'string',
		menu_name_en: {type: "string", optional: true, default: null},
		parent_id: 'string',
		menu_name_ar: {type: "string", optional: true, default: null},
		menu_link: {type: "string", optional: true, default: null},
		menu_for: {type: "string", optional: true, default: null},
		icon_label: {type: "string", optional: true, default: null},

	}
};


class Settings extends Realm.Object {
}

Settings.schema = {
	name: 'Settings',
	primaryKey: 'id',
	properties: {
		id: 'string',
		locale: 'string',
		sound_code: 'string',
		popup_settings: 'string',
		schoolId: 'string',
		visibility: 'int'
	}
};


class TimeTable extends Realm.Object {
}

TimeTable.schema = {
	name: 'TimeTable',
	properties: {
		day: 'int',
		row: 'int',
		sub_shortcut: 'string?',
		schoolId: 'string',
		class: 'string?',
		div: 'string?',
		teacherId: 'string?'
	}
};


class RightMenus extends Realm.Object {
}

RightMenus.schema = {
	name: 'RightMenus',
	properties: {
		menuId: 'string',
		menu_name_en: 'string',
		menu_name_ar: 'string',
		page_type: 'string',
		page_link: 'string?',
		schoolId: 'string',
		teacherId: 'string?',
	}
};


class StaffContacts extends Realm.Object {
}

StaffContacts.schema = {
	name: 'StaffContacts',
	properties: {
		schoolId: 'string',
		teachername: 'string',
		mobileNo: 'string',
	}
};

export default new Realm({
	schema: [Student, Settings, TimeTable, StaffContacts, RightMenus,
		Message, Staff, Classes, Divs, AcadamicStudents, Subjects, StaffMessages, StaffMenuPermission],
	schemaVersion: 41
});

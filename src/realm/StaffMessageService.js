// import realm from '../realm/schema';
// import realm from '../actions/actions'
import realm from '../realm/schema';
//let realm = global.realm
let StaffMessageService = {
  save: function (obj) {
    try {
      realm.write(() => {
        // realm.deleteAll()
        realm.create('StaffMessages', obj)
      })
    } catch (err) {
      console.log(err.message)
    }
  },


  update: function (obj) {
    try {
      let msgUpdateId = obj.id;
      console.log("Update msg by Id....", msgUpdateId);
      let msgUpdate = realm.objects('StaffMessages').filtered(`id == "${msgUpdateId}"`);
      realm.write(() => {
        // realm.deleteAll()
        msgUpdate[0].read = true;
        //realm.create('StaffMessages',obj,true)
      })
    } catch (err) {
      console.log(err)
    }
  },

  delete: function (msg) {
    try {
      // var filtered = sample.filtered([2, 4, 7, 10].map((id) => 'id == ' + id).join(' OR '));
      let msgDelteId = msg.id;
      let msgDelte = realm.objects('StaffMessages').filtered(`id == "${msgDelteId}"`);
      realm.write(() => {
        // realm.deleteAll()
        realm.delete(msgDelte)
      })
    } catch (err) {
      console.log(err)
    }
  },
  deleteById: function (msg) {
    try {
      // var filtered = sample.filtered([2, 4, 7, 10].map((id) => 'id == ' + id).join(' OR '));
      let msgDelteId = msg.id;
      console.log("Delete msg by Id....", msgDelteId);
      let msgDelte = realm.objects('StaffMessages').filtered(`id == "${msgDelteId}"`);

      realm.write(() => {

        // realm.delete( realm.objects('Message'));
        console.log("delete seclection object....", msgDelte);
        realm.delete(msgDelte);
        console.log("Messages---------", realm.objects('StaffMessages').length);
        console.log("After delete messages..", realm.objects('StaffMessages'));


      })
    } catch (err) {
      console.log(err)
    }
  },
};

module.exports = {
  StaffMessageService
};

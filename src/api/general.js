export const toFormData = (data) => {
    const form = new FormData();
    Object.keys(data).forEach(key => {
        if (Array.isArray(data[key]))
            return form.append(key,JSON.stringify(data[key]));
        return data[key] && form.append(key, data[key])
    });
    return form;
}

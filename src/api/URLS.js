export const BASE_URL_HOME = 'https://www.nenewe.com/';
export const BASE_URL = BASE_URL_HOME + 'school/neneweapis/';
export const token_secure = "$2y$10$83U0d5VXD6hEPGDvyEHYZO99SXhWQloSVcP/zl2Ja6caOtOYzMeCZ";
export const URLS = {
    LOGGED_IN_USER: BASE_URL + 'logged-mobile-user/',
    FULL_TIMETABLE: BASE_URL + 'full-school-time-table/',
    GET_RIGHT_MENU: BASE_URL + 'right-app-menus/',
    GET_TEACHERS: BASE_URL + 'teachers-with-mobile/',
    GET_TIME_TABLE: BASE_URL + 'time-table-by-class/',
    GET_TIME_TABLE_BY_TEACHER: BASE_URL + 'time-table-by-teachers/',
    GET_TIME_TABLE_BY_ROOM: BASE_URL + 'timetablebyroom/',
    GET_ACADEMIC_CALENDAR: BASE_URL + 'academic-calendar/',
    ADD_TOKEN: BASE_URL_HOME + 'add-tokens/',
    SOUND_SETTINGS: BASE_URL + 'update-sound-settings/',
    GUEST_BOOKS: {
        GET: BASE_URL + 'guestbooks/',
        ADD: BASE_URL + 'addguestbooks/',
        UPDATE: BASE_URL + 'update-guest-book-status/'
    }
}

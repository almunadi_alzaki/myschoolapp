/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React from 'react';
import ReactNative, { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { setToken } from './src/actions/actions'
import configureStore from './store';
import * as firebase from "react-native-firebase";
import I18n from 'react-native-i18n';

const currentLocale = I18n.currentLocale();
// Is it a RTL language?

export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;
console.log(isRTL, 'index')
// Allow RTL alignment in RTL languages
ReactNative.I18nManager.allowRTL(isRTL);
ReactNative.I18nManager.forceRTL(isRTL);

const store = configureStore();
const configureNotifications = async () => {
    let always = new firebase.notifications.Android.Channel('always-default', 'Always-default', firebase.notifications.Android.Importance.Max)
        .setDescription('Always')
        .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
        .enableVibration(true);

    const open = new firebase.notifications.Android.Channel('open-default', 'Open-default', firebase.notifications.Android.Importance.High)
        .setDescription('never')
        .setLockScreenVisibility(firebase.notifications.Android.Visibility.Private)
        .enableVibration(true);
    const closed = new firebase.notifications.Android.Channel('closed-default', 'Closed-default', firebase.notifications.Android.Importance.High)
        .setDescription('closed')
        .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public)
        .enableVibration(true);
    const never = new firebase.notifications.Android.Channel('never-default', 'Never-default', firebase.notifications.Android.Importance.Low)
        .setDescription('never')
        .setLockScreenVisibility(firebase.notifications.Android.Visibility.Private)
        .enableVibration(true);

    firebase.notifications().android.createChannels([always, open, closed, never]);
    firebase.messaging().getToken()
        .then(fcmToken => {
            if (fcmToken) {
                store.dispatch(setToken(fcmToken));
            }
        });
    firebase.messaging().onTokenRefresh(fcmToken => {
        if (fcmToken) {
            store.dispatch(setToken(fcmToken));
        }
    });
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        // user has permissions
    } else {
        try {
            await firebase.messaging().requestPermission();
            // User has authorized
        } catch (error) {
            // User has rejected permissions
        }
    }
}
configureNotifications();
// store.dispatch(getSettings())
// registerForPushNotificationsAsync();

const SchoolAppRedux = () => (
    <Provider store={store}>
        <App />
    </Provider>
)
AppRegistry.registerComponent(appName, () => SchoolAppRedux);

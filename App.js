import React from 'react';
import SchoolAppNavigator from './src/screens/SchoolAppNavigator.js';
import { connect } from 'react-redux';
import { getSettings } from './src/actions/actions'
import { getUnreadMessages, getUnreadStaffMessages } from './src/actions/messageActions';
import { ThemeProvider } from "react-native-elements";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import Axios from "axios";
import {ActivityIndicator, Alert, AppState, StyleSheet, View} from "react-native";
import * as firebase from "react-native-firebase";
import { strings } from "./src/i18n";
import { sentryMessage } from './src/utils';

const theme = {
	colors: {
		primary: '#5f021f',
	}
}
const paperTheme = {
	...DefaultTheme,
	roundness: 2,
	colors: {
		...DefaultTheme.colors,
		primary: '#5f021f',
		accent: '#911610',
	}
};

class App extends React.Component {
	state = {
		appState: AppState.currentState,
	};

	componentDidMount() {
		AppState.addEventListener('change', this._handleAppStateChange);
		this.onStart();
	}

	componentWillUnmount() {
		AppState.removeEventListener('change', this._handleAppStateChange);
		this.onEnd();
	}

	_handleAppStateChange = (nextAppState) => {
		if (
			this.state.appState.match(/inactive|background/) &&
			nextAppState === 'active'
		) {
			this.onStart();
			console.log('App has come to the foreground!');
		} else {
			this.onEnd();
		}
		this.setState({ appState: nextAppState });
	};
	onStart = () => {
		this.props.getSettings();
		this.configureNotifications();
		// Axios.defaults.timeout = 1e3;
		Axios.interceptors.request.use((config) => {
			this.setState({ isLoading: true });
			return config;
		}, (error) => {
			this.setState({ isLoading: false });
			return Promise.reject(error);
		});

		Axios.interceptors.response.use((response) => {
			this.setState({ isLoading: false });
			return response;
		}, (error) => {
			sentryMessage('internet Connection', { response: error.response });
			if (!error.response && !this.state.alert) {
				this.setState({ alert: true }, () => Alert.alert(strings('internetConnection'), '', [{
					text: 'OK',
					onPress: () => this.setState({ alert: false })
				}]))
			}
			this.setState({ isLoading: false });
			return Promise.reject(error);
		});
		this.props.sendMessage()
	}

	onEnd = () => {
		this.notificationOpenedListener();
		this.notificationListener();
	}

	configureNotifications = () => {
		this.notificationListener = firebase.notifications()
			.onNotification((notification) => {
				if (this.props.popup_settings === 'always') {
					const n = new firebase.notifications.Notification()
						.setNotificationId('notificationId')
						.setTitle(notification._title)
						.setBody(notification._body);
					n
						.android.setChannelId('always')
						.android.setSmallIcon('ic_launcher');

					firebase.notifications().displayNotification(n)
				}
				this.props.sendMessage();
			});
		this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
			// Get the action triggered by the notification being opened
			const action = notificationOpen.action;
			// Get information about the notification that was opened
			const notification = notificationOpen.notification;
			this.props.sendMessage()
		});
		firebase.notifications().getInitialNotification()
			.then((notificationOpen) => {
				if (notificationOpen) {
					const action = notificationOpen.action;
					const notification = notificationOpen.notification;
					this.props.sendMessage()
				}
			});
	};

	render() {
		const isLoading = Object.values(this.props.updateStatus).some(p => !!p);
		return (
			<ThemeProvider theme={theme}>
				<PaperProvider theme={paperTheme}>
					<SchoolAppNavigator/>
					{isLoading && <View style={styles.loader}>
						<ActivityIndicator size='large'/>
					</View>}
				</PaperProvider>
			</ThemeProvider>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	getSettings: () => dispatch(getSettings()),
	sendMessage: () => {
		// dispatch(getSettings());
		dispatch(getUnreadMessages());
		dispatch(getUnreadStaffMessages());
	},
})

const mapStateToProps = (state) => {
	return {
		locale: state.locale,
		isRTL: state.isRTL,
		updateStatus: state.general.updateStatus,
		popup_settings: state.settings.popup_settings,
	}
}
const styles = StyleSheet.create({
	loader: {
		position: 'absolute',
		left: 0,
		right: 0,
		top: 0,
		bottom: 0,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#F5FCFF88'
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(App);


import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import locale from './src/reducers/locale';
import student from './src/reducers/student';
import staff from './src/reducers/staff';
import general from './src/reducers/general';
import guestBooks from './src/reducers/guest-books';
import thunkMiddleware from 'redux-thunk'
import Reactotron from "reactotron-react-native";
import { reactotronRedux } from "reactotron-redux";
import settings from "./src/reducers/settings";

const rootReducer = combineReducers({
  locale,
  student,
  staff,
  general,
  guestBooks,
  settings,
});
const rt = Reactotron
    .configure({
      name: "School App",
      // host:'192.168.43.64'
    })
    .useReactNative({
      networking: { // optionally, you can turn it off with false.
        ignoreUrls: /symbolicate/
      }
    })
    .use(reactotronRedux())
    .connect();
const log = console.log;
console.log = function () {
  Reactotron.log(...arguments);
  log(...arguments);
}
const configureStore = () => {
  return createStore(rootReducer,
      compose(applyMiddleware(thunkMiddleware), rt.createEnhancer()));
}

export default configureStore;
